package com.atlassian.plugin.cache.filecache.impl;

import com.atlassian.plugin.cache.filecache.Cache;

import java.io.OutputStream;

import static com.atlassian.plugin.cache.filecache.impl.FileCacheImpl.ensureNotNull;

/**
 * Always streams each resource from the given input
 * @since v2.13
 */
public class PassThroughCache implements Cache
{
    @Override
    public void cache(String bucket, String key, OutputStream out, StreamProvider provider)
    {
        provider.write(out);
    }

    @Override
    public void cacheTwo(String bucket, String key, OutputStream out1, OutputStream out2, TwoStreamProvider provider)
    {
        provider.write(ensureNotNull(out1), ensureNotNull(out2));
    }

    @Override
    public void clear() {
    }
}
