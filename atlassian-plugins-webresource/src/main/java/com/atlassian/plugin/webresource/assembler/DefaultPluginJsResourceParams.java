package com.atlassian.plugin.webresource.assembler;

import com.atlassian.webresource.api.assembler.resource.PluginJsResourceParams;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;

import java.util.Map;

/**
 * @since v3.0
 */
class DefaultPluginJsResourceParams extends DefaultPluginUrlResourceParams implements PluginJsResourceParams {
    public DefaultPluginJsResourceParams(Map<String, String> params, String key, BatchType batchType) {
        super(params, key, batchType);
    }
}
