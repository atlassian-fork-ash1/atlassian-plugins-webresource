package com.atlassian.plugin.webresource.cdn.mapper;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Represents a single mapping from a local resource to one or more CDN resources
 *
 * @since v3.5.7
 */
public interface Mapping {

    /**
     * @return Original relative URL from local instance, without context path.
     */
    @Nonnull
    String originalResource();

    /**
     * @return List with distinct relative URLs of resources on CDN.
     */
    @Nonnull
    List<String> mappedResources();

}
