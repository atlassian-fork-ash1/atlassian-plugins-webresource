package com.atlassian.plugin.webresource.cdn.mapper;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Failed to load mapping file.
 *
 * @since v3.5.0
 */
@ExperimentalApi
public class MappingParserException extends Exception {
    public MappingParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public MappingParserException(String message) {
        super(message);
    }
}
