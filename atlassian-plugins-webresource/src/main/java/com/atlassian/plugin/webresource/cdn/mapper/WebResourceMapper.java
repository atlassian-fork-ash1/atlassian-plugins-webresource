package com.atlassian.plugin.webresource.cdn.mapper;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

/**
 * Web resource mappings, i.e., URL transformations from local to pre-baked resources on CDN.
 * Products should implement or use the default {@link DefaultWebResourceMapper}.
 *
 * @since v3.5.0
 */
public interface WebResourceMapper {
    /**
     * <p>
     * Maps a local resource URL into a CDN-based URL list (a local resource could be mapped into multiple CDN resources).
     * </p>
     * For instance, it would transform a URL like {@code /jira/my/resource/hash/batch.js} into one or more
     * domain-relative URLs like {@code //my.cdn.com/my/resource/hash/batch.js}.
     * @param resourceUrl URL of wanted web resource (includes context).
     * @return CDN-based URLs of local mapped resources, if any, otherwise returns an empty list.
     */
    @Nonnull
    List<String> map(@Nonnull String resourceUrl);

    /**
     * <p>
     * Maps a local resource URL into a CDN-based URL.
     * </p>
     * For instance, it would transform a URL like {@code /jira/my/resource/hash/batch.js} into a
     * domain-relative URL like {@code //my.cdn.com/my/resource/hash/batch.js}.
     * @param resourceUrl URL of wanted web resource (includes context).
     * @return Single CDN-based URL of a local mapped resource, if any, otherwise returns an empty list.
     */
    @Nonnull
    Optional<String> mapSingle(@Nonnull String resourceUrl);

    /**
     * Inspects mappings used by one instance.
     * @return Current loaded mappings for this instance.
     * @since v3.5.7
     */
    @Nonnull
    MappingSet mappings();
}
