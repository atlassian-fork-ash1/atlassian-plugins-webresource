package com.atlassian.plugin.webresource.impl.support;

import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.helpers.UrlGenerationHelpers;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Cache for URL Generation, or more exactly three Caches for:
 * - Batches for given included and excluded.
 * - Legacy resources for given included and excluded.
 * - Resolved excluded for given include and excluded.
 *
 * @since v3.3
 */
public interface UrlCache
{
    public static class IncludedAndExcluded extends Tuple<LinkedHashSet<String>, LinkedHashSet<String>>
    {
        public IncludedAndExcluded(LinkedHashSet<String> included, LinkedHashSet<String> excluded)
        {
            super(included, excluded);
        }

        public LinkedHashSet<String> getIncluded()
        {
            return getFirst();
        }

        public LinkedHashSet<String> getExcluded()
        {
            return getLast();
        }
    }

    /**
     * Result of evaluated condition with the condition itself.
     */
    public static class EvaluatedCondition extends Tuple<CachedCondition, Boolean>
    {
        public EvaluatedCondition(CachedCondition cachedCondition, Boolean evaluationResult)
        {
            super(cachedCondition, evaluationResult);
        }
    }

    public static class IncludedExcludedConditionsAndBatchingOptions
    {
        private final IncludedAndExcluded inludedAndExcluded;
        private final Set<EvaluatedCondition> evaluatedConditions;
        private final boolean resplitMergedContextBatchesForThisRequest;

        public IncludedExcludedConditionsAndBatchingOptions(IncludedAndExcluded includedAndExcluded, Set<EvaluatedCondition> evaluatedConditions,
            boolean resplitMergedContextBatchesForThisRequest)
        {
            this.inludedAndExcluded = checkNotNull(includedAndExcluded);
            this.evaluatedConditions = checkNotNull(evaluatedConditions);
            this.resplitMergedContextBatchesForThisRequest = resplitMergedContextBatchesForThisRequest;
        }

        public LinkedHashSet<String> getIncluded()
        {
            return inludedAndExcluded.getIncluded();
        }

        public LinkedHashSet<String> getExcluded()
        {
            return inludedAndExcluded.getExcluded();
        }

        @Override
        public boolean equals(final Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (!(o instanceof IncludedExcludedConditionsAndBatchingOptions))
            {
                return false;
            }

            final IncludedExcludedConditionsAndBatchingOptions that = (IncludedExcludedConditionsAndBatchingOptions) o;

            if (resplitMergedContextBatchesForThisRequest != that.resplitMergedContextBatchesForThisRequest)
            {
                return false;
            }
            if (!evaluatedConditions.equals(that.evaluatedConditions))
            {
                return false;
            }
            if (!inludedAndExcluded.equals(that.inludedAndExcluded))
            {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = inludedAndExcluded.hashCode();
            result = 31 * result + evaluatedConditions.hashCode();
            result = 31 * result + (resplitMergedContextBatchesForThisRequest ? 1 : 0);
            return result;
        }
    }

    public static interface BatchesProvider
    {
        UrlGenerationHelpers.CalculatedBatches get(IncludedExcludedConditionsAndBatchingOptions key);
    }

    /**
     * Get batches for given included and excluded.
     * @return context batches, web resource batches, resolved excluded resources.
     */
    UrlGenerationHelpers.CalculatedBatches getBatches(IncludedExcludedConditionsAndBatchingOptions key, BatchesProvider provider);

    public static interface ResolvedExcludedProvider
    {
        LinkedHashSet<String> get(IncludedExcludedConditionsAndBatchingOptions key);
    }

    /**
     * Get resolved excluded for given include and excluded.
     * @return resolved excluded resources
     */
    LinkedHashSet<String> getResolvedExcluded(IncludedExcludedConditionsAndBatchingOptions key, ResolvedExcludedProvider provider);

    void clear();

    /**
     * Implementation.
     */
    public static class Impl implements UrlCache
    {
        private Cache<
            IncludedAndExcluded,
            List<CachedCondition>> cachedConditions;
        private Cache<IncludedExcludedConditionsAndBatchingOptions, UrlGenerationHelpers.CalculatedBatches> cachedBatches;
        private Cache<
            IncludedExcludedConditionsAndBatchingOptions,
            LinkedHashSet<String>> cachedResolvedExcluded;

        public Impl(int size)
        {
            cachedConditions = CacheBuilder.newBuilder().maximumSize(size).build();
            cachedBatches = CacheBuilder.newBuilder().maximumSize(size).build();
            cachedResolvedExcluded = CacheBuilder.newBuilder().maximumSize(size).build();
        }

        @Override
        public UrlGenerationHelpers.CalculatedBatches getBatches(final IncludedExcludedConditionsAndBatchingOptions key, final BatchesProvider provider)
        {
            try
            {
                return cachedBatches.get(key, () -> provider.get(key));
            }
            catch (ExecutionException e)
            {
                throw new RuntimeException(e);
            }
        }

        @Override
        public LinkedHashSet<String> getResolvedExcluded(final IncludedExcludedConditionsAndBatchingOptions key,
            final ResolvedExcludedProvider provider)
        {
            try
            {
                return cachedResolvedExcluded.get(key, () -> provider.get(key));
            }
            catch (ExecutionException e)
            {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void clear()
        {
            cachedConditions.invalidateAll();
            cachedBatches.invalidateAll();
            cachedResolvedExcluded.invalidateAll();
        }
    }

    /**
     * No caching implementation.
     */
    public static class PassThrough implements UrlCache
    {
        @Override
        public UrlGenerationHelpers.CalculatedBatches getBatches(IncludedExcludedConditionsAndBatchingOptions key, BatchesProvider provider)
        {
            return provider.get(key);
        }

        @Override
        public LinkedHashSet<String> getResolvedExcluded
            (IncludedExcludedConditionsAndBatchingOptions key, ResolvedExcludedProvider provider)
        {
            return provider.get(key);
        }

        @Override
        public void clear()
        {
        }
    }
}