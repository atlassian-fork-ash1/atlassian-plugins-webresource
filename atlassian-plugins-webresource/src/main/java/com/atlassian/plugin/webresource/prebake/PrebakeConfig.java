package com.atlassian.plugin.webresource.prebake;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.text.StrSubstitutor;

import javax.annotation.Nonnull;
import java.io.File;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Prebake configuration for clients to work with WebResourceMapper.
 *
 * @see com.atlassian.plugin.webresource.cdn.CDNStrategy
 * @since v3.5.0
 */
public interface PrebakeConfig {
    /**
     * Creates a PrebakeConfig that uses a mapping path pattern like <code>/path/to/${state}/mapping.json</code>
     */
    static PrebakeConfig forPattern(@Nonnull final String mappingPathPattern) {
        checkNotNull(mappingPathPattern, "mappingPathPattern null!");
        return new PrebakeConfig() {
            @Override
            @Nonnull
            public File getMappingLocation(@Nonnull final String globalStateHash) {
                checkNotNull(globalStateHash, "globalStateHash null!");
                return new File(StrSubstitutor.replace(
                        mappingPathPattern,
                        ImmutableMap.<String, String>builder().put("state", globalStateHash).build()
                ));
            }

            @Override
            public String getPattern() {
                return mappingPathPattern;
            }
        };
    }

    /**
     * Returns the location of the mapping.json file, given the global statue hash value
     */
    @Nonnull
    File getMappingLocation(@Nonnull String globalStateHash);

    /**
     * Returns the original pattern for information purposes
     */
    String getPattern();
}