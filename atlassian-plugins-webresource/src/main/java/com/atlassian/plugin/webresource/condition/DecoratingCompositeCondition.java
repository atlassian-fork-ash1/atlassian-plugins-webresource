package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.Dimensions;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract superclass for composite UrlReadingConditions.
 * <p>
 * Instances may contain child {@link UrlReadingCondition} and/or legacy {@link com.atlassian.plugin.web.Condition}
 * wrapped by {@link DecoratingLegacyCondition}. Evaluation is done as follows:
 * <ul>
 * <li>
 * At URL generation time: if a resource's condition contains only UrlReadingConditions it is included in the
 * batch. Query / hash parameters for UrlReadingConditions are added to the batch's URL.
 * <p>
 * Otherwise - if a resource contains legacy Conditions, these legacy Conditions are evaluated by calling
 * shouldDisplayImmediate(). If this method returns false, the resource is not included. If it returns true,
 * the resource is included as a separate out-of-batch resource. This is included after the batch. Query / hash
 * parameters for any UrlReadingConditions are added to the separate, unbatched PluginResource.
 * </li>
 * <li>
 * At Resource serving time, conditions for each UrlReadingCondition are evaluated by calling shouldDisplay().
 * This will be evaluated for either in the batch resource (if there are no legacy Conditions) or in
 * the out-of-batch resource (if there are legacy Conditions). Regardless of where the resource is included,
 * if the shouldDisplay() evaluates to false then empty content is returned in place where the resource would be.
 * </li>
 * </ul>
 *
 * @since v3.0
 */
abstract class DecoratingCompositeCondition implements com.atlassian.plugin.web.baseconditions.CompositeCondition<DecoratingCondition>, DecoratingCondition {
    protected List<DecoratingCondition> conditions = new ArrayList<>();

    public DecoratingCompositeCondition() {
    }

    @Override
    public void addCondition(DecoratingCondition condition) {
        this.conditions.add(condition);
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder, UrlBuildingStrategy urlBuilderStrategy) {
        for (DecoratingCondition condition : conditions) {
            condition.addToUrl(urlBuilder, urlBuilderStrategy);
        }
    }

    @Override
    public Dimensions computeDimensions() {
        Dimensions d = Dimensions.empty();
        for (DecoratingCondition condition : conditions) {
            d = d.product(condition.computeDimensions());
        }
        return d;
    }

    /**
     * @return true if this all child conditions can fully encode their state into the URL, otherwise false.
     */
    @Override
    public boolean canEncodeStateIntoUrl() {
        for (DecoratingCondition condition : conditions) {
            if (!condition.canEncodeStateIntoUrl()) {
                return false;
            }
        }
        return true;
    }

    public List<DecoratingCondition> getConditions() {
        return conditions;
    }
}
