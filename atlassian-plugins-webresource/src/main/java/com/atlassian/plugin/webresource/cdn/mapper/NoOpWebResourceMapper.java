package com.atlassian.plugin.webresource.cdn.mapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;

/**
 * No operation web resource mapper.
 *
 * @since v3.5.0
 */
public class NoOpWebResourceMapper implements WebResourceMapper {
    private final MappingSet mappings = new DefaultMappingSet(emptyList());
    private final Optional<Exception> reason;

    public NoOpWebResourceMapper(@Nullable Exception reason) {
        this.reason = Optional.ofNullable(reason);
    }

    @Nonnull
    @Override
    public List<String> map(@Nonnull String resourceUrl) {
        return emptyList();
    }

    @Nonnull
    @Override
    public Optional<String> mapSingle(@Nonnull String resourceUrl) {
        return Optional.empty();
    }

    @Nonnull
    @Override
    public MappingSet mappings() {
        return mappings;
    }

    /**
     * Special attribute used that provides information about why an operational instance if WebResourceMapper was not created.
     * @since v3.5.7
     */
    @Nonnull
    public Optional<Exception> reason() {
        return reason;
    }
}
