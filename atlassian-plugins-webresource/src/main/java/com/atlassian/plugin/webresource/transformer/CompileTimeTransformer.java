package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.sourcemap.SourceMap;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static com.atlassian.plugin.webresource.impl.support.Support.copy;
import static com.atlassian.webresource.spi.CompilerUtil.toInputStream;

/**
 * Integration with compile-time resource transformations, it's not a standard transformer and doesn't follow the transformer API.
 *
 * @since v 3.3.1
 */
public class CompileTimeTransformer {
    public static Content process(Globals globals, final Resource resource, final Content content) {
        return new ContentImpl(content.getContentType(), true) {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                Config config = globals.getConfig();

                boolean hasMinifiedVersion = false;
                String pathForMinifiedVersion = null;
                InputStream minifiedStream = null;
                if (isMinificationEnabledFor(config, resource)) {
                    pathForMinifiedVersion = getPathForMinifiedVersion(resource.getPath());
                    minifiedStream = null;

                    // Provide Globally minified resource if Global Minification is Enabled
                    if (config.isGlobalMinificationEnabled() && resource.getNameOrLocationType().equals(Config.JS_TYPE)) {
                        minifiedStream = toInputStream(config.getResourceCompiler(), resource.getPath());
                    }

                    if (minifiedStream == null) {
                        minifiedStream = resource.getStreamFor(pathForMinifiedVersion);
                    }

                    // Checking alternate path for minified resource.
                    if (minifiedStream == null) {
                        pathForMinifiedVersion = getAlternatePathForMinifiedVersion(resource.getPath());
                        minifiedStream = resource.getStreamFor(pathForMinifiedVersion);
                    }

                    hasMinifiedVersion = minifiedStream != null;
                }

                if (hasMinifiedVersion) {
                    copy(minifiedStream, out);
                } else {
                    // If there's no minified version serving original version.
                    content.writeTo(out, isSourceMapEnabled);
                }

                // Attaching source map.
                if (isSourceMapEnabled)
                {
                    try
                    {
                        String prebuildSourcePath = Resource.getPrebuiltSourcePath(resource.getLocation());
                        InputStream sourceStream = resource.getStreamFor(prebuildSourcePath);
                        boolean hasPrebuildSource = sourceStream != null;
                        if (sourceStream != null) {
                            try {
                                sourceStream.close();
                            } catch (IOException e) {
                            }
                        }

                        // Checking what source should be used, if there's the `<resource-name>-source` file using it, if not
                        // using the resource itself as a source.
                        String sourceUrl;
                        if (hasPrebuildSource) {
                            sourceUrl = globals.getRouter().prebuildSourceUrl(resource);
                        } else {
                            sourceUrl = globals.getRouter().sourceUrl(resource);
                        }

                        if (hasMinifiedVersion) {
                            return Support.getSourceMap(pathForMinifiedVersion, resource, sourceUrl);
                        } else {
                            return Support.getSourceMap(resource.getPath(), resource, sourceUrl);
                        }
                    }
                    catch (RuntimeException e)
                    {
                        Support.LOGGER.warn("can't parse source map for " + resource.getKey() + "/" + resource.getName(), e);
                        return null;
                    }
                } else {
                    return null;
                }
            }
        };
    }

    /**
     * If minification enabled for given resource.
     */
    private static boolean isMinificationEnabledFor(Config config, Resource resource) {
        // check if minification has been turned off for this resource (at the
        // module level)
        if (!resource.getParent().isMinificationEnabled()) {
            return false;
        }

        if (!config.isMinificationEnabled()) {
            return false;
        }

        String path = resource.getPath();
        // We only minify .js or .css files
        if (path.endsWith(".js")) {
            // Check if it is already the minified version of the file
            return !(path.endsWith("-min.js") || path.endsWith(".min.js"));
        }
        if (path.endsWith(".css")) {
            // Check if it is already the minified version of the file
            return !(path.endsWith("-min.css") || path.endsWith(".min.css"));
        }
        // Not .js or .css, don't bother trying to find a minified version (may
        // save some file operations)
        return false;
    }

    /**
     * The path to the minified version of the resource.
     */
    private static String getPathForMinifiedVersion(String path) {
        final int lastDot = path.lastIndexOf(".");
        // this can never but -1 since the method call is protected by a call to
        // minificationStrategyInPlay() first
        return path.substring(0, lastDot) + "-min" + path.substring(lastDot);
    }

    /**
     * The path to the minified version of the resource.
     */
    private static String getAlternatePathForMinifiedVersion(String path) {
        final int lastDot = path.lastIndexOf(".");
        // this can never but -1 since the method call is protected by a call to
        // minificationStrategyInPlay() first
        return path.substring(0, lastDot) + ".min" + path.substring(lastDot);
    }
}