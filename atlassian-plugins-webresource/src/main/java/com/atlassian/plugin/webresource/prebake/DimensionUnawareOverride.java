package com.atlassian.plugin.webresource.prebake;

import com.atlassian.webresource.api.prebake.Dimensions;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyMap;

/**
 * Override {@link Dimensions} values for dimension-unaware conditions.
 *
 * <p> This class is used to inject dimensionality in dimension-unaware conditions while we wait for the teams owning
 * those conditions to update them. It's a stopgap measure, hence its deprecated state, and should only be used when
 * we need a quick way to untaint some resources for prebaking.
 *
 * <p> It's important to note that injecting dimensions using this class only works when the overridden condition
 * does not contribute to the resource hash. In that case, the only way to add dimensionality is by updating the
 * condition to be dimension-aware.
 *
 * <p> The methods in this class are only used while prebaking.
 *
 * @since v3.5.27
 */
@Deprecated
public final class DimensionUnawareOverride {

    private static volatile Map<String, DimensionOverride> dimensionUnawareOverrides = emptyMap();

    /**
     * Registers {@link Dimensions} overrides to apply to dimension-unaware conditions. Invoking this method will
     * remove all existing overrides, and substitute them with the content of the collection passed as parameter.
     *
     * <p> A {@link DimensionOverride} indicates the condition class to which it has to be applied, the query string
     * key, and all the possible dimension values that the condition can assume at runtime.
     *
     * <p> {@link DimensionOverride}s are only applied if the condition is not dimension-aware.
     *
     * @param overrides dimension overrides
     */
    public static void setup(Collection<DimensionOverride> overrides) {
        checkNotNull(overrides);

        Map<String, DimensionOverride> om = new HashMap<>();
        for (DimensionOverride o : overrides) {
            om.put(o.className, o);
        }
        dimensionUnawareOverrides = Collections.unmodifiableMap(om);
    }

    /**
     * Removes all condition overrides
     */
    public static void reset() {
        dimensionUnawareOverrides = emptyMap();
    }

    /**
     * Indicates whether there are dimension overrides for a particular condition class.
     *
     * @param className name of the class
     * @return true if there are condition overrides for the class, false otherwise
     */
    public static boolean contains(String className) {
        return dimensionUnawareOverrides.containsKey(className);
    }

    /**
     * Returns the query string key to be used at prebake time when the condition identified by the class name passed
     * as parameter will contribute to the URL.
     *
     * @param className name of the class
     * @return query string key to be employed when prebaking resources that use the condition class passed as parameter
     */
    public static String key(String className) {
        return dimensionUnawareOverrides.get(className).getKey();
    }

    /**
     * Returns the dimension values to be used at prebake time when prebaking resources that use the condition
     * identified by the class name passed as parameter.
     *
     * @param className name of the class
     * @return dimensions to be employed when prebaking resources that use the condition class passed as parameter
     */
    public static Dimensions dimensions(String className) {
        return dimensionUnawareOverrides.get(className).getDimensions();
    }


    /**
     * Number of registered overrides.
     *
     * @return number of registered overrides.
     */
    public static int size() {
        return dimensionUnawareOverrides.size();
    }


    /**
     * A single dimension override.
     *
     * @since v3.5.27
     */
    @Deprecated
    public static class DimensionOverride {

        private final String className;
        private final String key;
        private final Dimensions dimensions;

        /**
         * Creates a new dimension override
         *
         * @param className name of the dimension-unaware class to be overridden
         * @param key query string key to be used when the dimension-unaware object needs to contribute to the URL at
         *            prebake-time
         * @param dimensions dimension values that the condition will assume at prebake time
         */
        public DimensionOverride(String className, String key, Collection<String> dimensions) {
            checkNotNull(dimensions);

            this.className = checkNotNull(className);
            this.key = checkNotNull(key);
            this.dimensions = Dimensions
                    .empty()
                    .andExactly(key, dimensions);
        }

        public String getClassName() {
            return className;
        }

        public String getKey() {
            return key;
        }

        public Dimensions getDimensions() {
            return dimensions;
        }
    }

}
