package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.BatchResourceContentsWebFormatter;
import com.atlassian.plugin.webresource.CssWebResource;
import com.atlassian.plugin.webresource.JavascriptWebResource;
import com.atlassian.plugin.webresource.PrefetchLinkWebResource;
import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.plugin.webresource.WebResourceFormatter;
import com.atlassian.plugin.webresource.data.DataTagWriter;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResource;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.data.PluginDataResource;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.transform;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/**
 * Implementation of WebResourceSet
 *
 * @since v3.0
 */
public class DefaultWebResourceSet implements WebResourceSet {
    private final List<Container> resources;
    private final List<PluginDataResource> data;
    private final boolean complete;
    private final RequestState requestState;
    private final Config config;
    private final List<ResourceUrl> resourceUrls;

    public DefaultWebResourceSet(RequestState requestState,
                                 List<PluginDataResource> data,
                                 List<ResourceUrl> resourceUrls,
                                 boolean complete,
                                 Config config
    ) {
        this.requestState = requestState;
        this.data = data;
        this.complete = complete;
        this.config = config;
        this.resourceUrls = resourceUrls;

        resources = new LinkedList<>();
        for (ResourceUrl resourceUrl : resourceUrls) {
            PluginUrlResource pluginUrlResource;
            String type = resourceUrl.getType();
            if (Config.JS_TYPE.equals(type)) {
                pluginUrlResource = new DefaultPluginJsResource(resourceUrl);
            } else if (Config.CSS_TYPE.equals(type)) {
                pluginUrlResource = new DefaultPluginCssResource(resourceUrl);
            } else {
                throw new RuntimeException("unsupported extension " + type);
            }
            resources.add(new Container(resourceUrl, pluginUrlResource));
        }
    }

    @Override
    public Iterable<WebResource> getResources() {
        List<WebResource> webResources = new LinkedList<>(data);
        for (Container resource : resources) {
            webResources.add(resource.getPluginUrlResource());
        }
        return webResources;
    }

    @Override
    public <T extends WebResource> Iterable<T> getResources(Class<T> clazz) {
        return (Iterable<T>) Iterables.filter(getResources(), Predicates.instanceOf(clazz));
    }

    @Override
    public boolean isComplete() {
        return complete;
    }

    @Override
    public void writeHtmlTags(Writer writer, UrlMode urlMode) {
        writeHtmlTags(writer, urlMode, Predicates.alwaysTrue());
    }

    @Override
    public void writeHtmlTags(Writer writer, UrlMode urlMode, final Predicate<WebResource> predicate) {
        writeHtmlTags(writer, urlMode, predicate, Predicates.alwaysTrue());
    }

    @Override
    public void writePrefetchLinks(Writer writer, UrlMode urlMode) {
        // Note we don't write data providers when generating prefetch links, only URLs.
        // Prefetch links make the same <link> HTML regardless of resource type, so we only have a single formatter.
        writeFormattedResources(writer, urlMode, Predicates.alwaysTrue(),
                singletonList(PrefetchLinkWebResource.FORMATTER), true);
    }

    public void writeHtmlTags(Writer writer, UrlMode urlMode, final Predicate<WebResource> predicate,
                              final Predicate<ResourceUrl> legacyPredicate) {
        writeSyncResources(writer);

        // Writing data resources.
        try {
            new DataTagWriter().write(writer, Iterables.filter(data, predicate));
        } catch (IOException ex) {
            Support.LOGGER.error("IOException encountered rendering data tags", ex);
        }

        // Writing urls.
        List<WebResourceFormatter> formatters = asList(new CssWebResource(config.amdEnabled()), new JavascriptWebResource(config.amdEnabled()));
        writeFormattedResources(writer, urlMode, new ContainerPredicate(predicate, legacyPredicate),
                formatters, false);
    }

    public List<ResourceUrl> getResourceUrls() {
        return Lists.newArrayList(this.resourceUrls);
    }

    private void writeFormattedResources(Writer writer, UrlMode urlMode, Predicate<Container> predicate,
                                         List<WebResourceFormatter> webResourceFormatters, boolean writingPrefetchLinks) {
        List<Container> localCopyOfResources = new LinkedList<>();
        for (Container resource : resources) {
            if (predicate.apply(resource)) {
                localCopyOfResources.add(resource);
            }
        }

        boolean isDeferJsAttributeEnabled = config.isDeferJsAttributeEnabled();

        for (final WebResourceFormatter formatter : webResourceFormatters) {
            for (final Iterator<Container> iter = localCopyOfResources.iterator(); iter.hasNext(); ) {
                final Container resource = iter.next();
                if (formatter.matches(resource.getResourceUrl().getName())) {
                    writeResourceTag(urlMode, resource, formatter, writer, isDeferJsAttributeEnabled, writingPrefetchLinks);
                    iter.remove();
                }
            }
        }

        for (Container resource : localCopyOfResources) {
            writeContentAndSwallowErrors(writer, "<!-- Error loading resource \"",
                    resource.getResourceUrl().getKey(),
                    "\".  No resource formatter matches \"", resource.getResourceUrl().getName(), "\" -->\n");
        }
    }

    private void writeSyncResources(Writer writer) {
        // Injecting before all scripts.
        Optional<DefaultWebResourceSet> syncSet = requestState.getSyncResourceSet();
        if (syncSet.isPresent()) {
            Stream<Resource> resources = syncSet.get().getResourceUrls().stream()
                    .flatMap(resourceUrl -> resourceUrl.getResources(requestState.getCache())
                    .stream());

            List<Content> resourceContents = resources
                    .filter(resource -> {
                        final boolean isJS = resource.getNameType().equals("js");
                        if (!isJS) {
                            Support.LOGGER.warn("'" + resource.getFullName() + "' resource has been filtered and will not show up on the page. "
                            + "Currently, only JS files are supported for sync resources.");
                        }
                        return isJS;
                    })
                    .map(resource -> transform(
                            requestState.getGlobals(),
                            new LinkedHashSet<>(singletonList(resource.getParent().getKey())),
                            null,
                            resource,
                            resource.getParams(),
                            true))
                    .collect(Collectors.toList());

            if (resourceContents.size() > 0) {
                try {
                    final OutputStream out = new OutputStream() {
                        @Override
                        public void write(int b) throws IOException {
                            writer.write(b);
                        }
                    };

                    writer.write("<script>");
                    for (Content content : resourceContents) {
                        content.writeTo(out, false);
                        writer.write("\n");
                    }
                    writer.write("</script>\n");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            requestState.markSyncResourcesAsWritten();
        }
    }

    private void writeResourceTag(final UrlMode urlMode, final Container resource,
                                  final WebResourceFormatter formatter, final Writer writer, boolean isDeferJsAttributeEnabled,
                                  boolean writingPrefetchLinks) {
        PluginUrlResource urlResource = resource.getPluginUrlResource();

        String formattedResource;
        if (formatter instanceof JavascriptWebResource) {
            Map<String, String> params = resource.getResourceUrl().getParams(); // this affects URI
            Map<String, String> attributes = new LinkedHashMap<>(urlResource.getParams().all());
            if (config.useAsyncAttributeForScripts()) {
                params.put(Config.ASYNC_SCRIPT_PARAM_NAME, "true");
                attributes.put(Config.ASYNC_SCRIPT_PARAM_NAME, "");
            }
            attributes.put(Config.INITIAL_RENDERED_SCRIPT_PARAM_NAME, "");
            formattedResource = ((JavascriptWebResource) formatter).formatResource(urlResource.getStaticUrl(urlMode), attributes, isDeferJsAttributeEnabled);
        } else {
            formattedResource = formatter.formatResource(urlResource.getStaticUrl(urlMode), urlResource.getParams().all());
        }

        // insert the dependency information into the formatted resource
        if (!writingPrefetchLinks && config.isBatchContentTrackingEnabled()) {
            formattedResource = BatchResourceContentsWebFormatter.insertBatchResourceContents(resource, formattedResource);
        }

        writeContentAndSwallowErrors(writer, formattedResource);
    }

    private void writeContentAndSwallowErrors(final Writer writer, final String... contents) {
        try {
            for (final String content : contents) {
                writer.write(content);
            }
        } catch (final IOException ex) {
            Support.LOGGER.error("IOException encountered rendering resource", ex);
        }
    }

    private static class ContainerPredicate implements Predicate<Container> {
        private final Predicate<WebResource> resourcePredicate;
        private final Predicate<ResourceUrl> legacyPredicate;

        public ContainerPredicate(Predicate<WebResource> resourcePredicate, Predicate<ResourceUrl> legacyPredicate) {
            this.resourcePredicate = resourcePredicate;
            this.legacyPredicate = legacyPredicate;
        }

        @Override
        public boolean apply(@Nullable Container container) {
            return container != null
                    && resourcePredicate.apply(container.getPluginUrlResource())
                    && legacyPredicate.apply(container.getResourceUrl());
        }
    }

    /**
     * Internal data structure to pass couple of objects together.
     */
    public static class Container {
        private final ResourceUrl resourceUrl;
        private final PluginUrlResource pluginUrlResource;

        public Container(final ResourceUrl resourceUrl, PluginUrlResource pluginUrlResource) {

            this.resourceUrl = resourceUrl;
            this.pluginUrlResource = pluginUrlResource;
        }

        public PluginUrlResource getPluginUrlResource() {
            return pluginUrlResource;
        }

        public ResourceUrl getResourceUrl() {
            return resourceUrl;
        }
    }
}
