package com.atlassian.plugin.webresource.impl.snapshot;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.plugin.webresource.impl.config.Config.CSS_CONTENT_TYPE;
import static com.atlassian.plugin.webresource.impl.config.Config.CSS_TYPE;
import static com.atlassian.plugin.webresource.impl.config.Config.JS_CONTENT_TYPE;
import static com.atlassian.plugin.webresource.impl.config.Config.JS_TYPE;

/**
 * Virtual resource for JS Module, stub to fit in existing API.
 *
 * @since v3.4.4
 */
public class ModuleResource extends Resource {
    private static final Map<String,String> DEFAULT_CONTENT_TYPES = ImmutableMap.<String,String>builder()
            .put(JS_TYPE, JS_CONTENT_TYPE)
            .put(CSS_TYPE, CSS_CONTENT_TYPE)
            // image types
            .put("svg", "image/svg+xml")
            .put("svgz", "image/svg+xml")
            // font types
            .put("ttf", "application/x-font-truetype")
            .put("woff", "application/font-woff")
            .put("woff2", "application/font-woff")
            .put("otf", "application/x-font-opentype")
            .put("eot", "application/vnd.ms-fontobject")
            .build();

    private final String pluginKey;
    private final String type;

    public ModuleResource(WebModule parent, String pluginKey, String filePath, String type) {
        super(parent, new ResourceLocation(filePath, filePath, type, null, null, new HashMap<>()), type, type);
        this.type = type;
        this.pluginKey = pluginKey;
    }

    public String getContentType() {
        switch (type) {
            case CSS_TYPE:
                // We want CSS files to always be considered CSS, even if developers provided their own content-type.
                return CSS_CONTENT_TYPE;
            case JS_TYPE:
                // We want JS files to always be considered JS, even if developers provided their own content-type.
                return JS_CONTENT_TYPE;
            default:
                final String fromResource = super.getContentType();
                return StringUtils.isBlank(fromResource) ? DEFAULT_CONTENT_TYPES.getOrDefault(type, fromResource) : fromResource;
        }
    }

    /**
     * Plugin key this resource belongs to.
     */
    public String getPluginKey() {
        return pluginKey;
    }
}
