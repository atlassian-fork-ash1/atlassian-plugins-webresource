package com.atlassian.plugin.webresource.cdn.mapper;

import com.google.common.base.MoreObjects;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Simple wrapper for a mapping.
 *
 * @since v3.5.7
 */
public class DefaultMapping implements Mapping {
    private final String originalResource;
    private final List<String> mappedResources;

    /**
     * @param originalResource Original local relative URL without context.
     * @param mappedResources  Ordered distinct list of URLs relative to CDN. If not distinct it'll force distinction.
     */
    public DefaultMapping(@Nonnull final String originalResource, @Nonnull final Stream<String> mappedResources) {
        checkNotNull(originalResource, "originalResource is null!");
        checkNotNull(mappedResources, "mappedResources is null for originalResource '%s'!", originalResource);
        this.originalResource = originalResource;
        this.mappedResources = mappedResources.distinct().filter(StringUtils::isNotEmpty).collect(Collectors.toList());
        checkArgument(!this.mappedResources.isEmpty(), "mappedResources is empty for originalResource '%s'!", originalResource);
    }

    @Override
    @Nonnull
    public String originalResource() {
        return originalResource;
    }

    @Override
    @Nonnull
    public List<String> mappedResources() {
        return mappedResources;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final DefaultMapping mapping = (DefaultMapping) o;
        return Objects.equals(originalResource, mapping.originalResource) &&
                Objects.equals(mappedResources, mapping.mappedResources);
    }

    @Override
    public int hashCode() {
        return Objects.hash(originalResource, mappedResources);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("originalResource", originalResource)
                .add("mappedResources", mappedResources)
                .toString();
    }
}
