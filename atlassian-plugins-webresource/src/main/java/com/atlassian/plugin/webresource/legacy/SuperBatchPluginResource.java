package com.atlassian.plugin.webresource.legacy;

import java.util.HashSet;

/**
 * Creates a batch of all like-typed resources that are declared as "super-batch="true"" in their plugin
 * definitions.
 * <p>
 * The URL for batch resources is /download/superbatch/&lt;type&gt;/batch.&lt;type. The additional type part in the path
 * is simply there to make the number of path-parts identical with other resources, so relative URLs will still work
 * in CSS files.
 */
public class SuperBatchPluginResource extends AbstractPluginResource {
    protected SuperBatchPluginResource() {
        super(new HashSet<>());
    }

    public void addBatchedWebResourceDescriptor(String key) {
        completeKeys.add(key);
    }

}
