package com.atlassian.plugin.webresource.cdn.mapper;

import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.prebake.PrebakeConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.emptyList;

/**
 * Implementation of {@link WebResourceMapper} which retrieves a mappings from a local JSON file and keeps it in memory.
 *
 * @since v3.5.0
 */
public class DefaultWebResourceMapper implements WebResourceMapper {
    private static final Logger log = LoggerFactory.getLogger(DefaultWebResourceMapper.class);

    private final MappingSet mappings;
    private final String ctCdnBaseUrl;
    private final String contextPath;
    private final WebResourceIntegration webResourceIntegration;

    /**
     * @param mappingParser   A parser to parse mappings from JSON.
     * @param prebakeConfig   PreBake configuration produced by the product which will point to the mappings file.
     * @param globalStateHash Hash which identify current product and enabled plugins as in {@link Config#computeGlobalStateHash()}
     * @param ctCdnBaseUrl    Cross-tenant CDN base URL. Will prefix all resource's mapped URLs.
     */
    public DefaultWebResourceMapper(@Nonnull final WebResourceIntegration webResourceIntegration,
                                    @Nonnull final MappingParser mappingParser,
                                    @Nonnull final PrebakeConfig prebakeConfig,
                                    @Nonnull final String globalStateHash,
                                    @Nonnull final String ctCdnBaseUrl,
                                    @Nonnull final String contextPath) throws MappingParserException, IOException {
        checkNotNull(webResourceIntegration, "webResourceIntegration is null!");
        checkNotNull(mappingParser, "mappingParser is null!");
        checkNotNull(prebakeConfig, "prebakeConfig is null!");
        checkNotNull(globalStateHash, "globalStateHash is null!");
        checkNotNull(ctCdnBaseUrl, "ctCdnBaseUrl is null!");
        this.webResourceIntegration = webResourceIntegration;
        this.mappings = loadMappings(mappingParser, globalStateHash, prebakeConfig);
        this.ctCdnBaseUrl = ctCdnBaseUrl;
        this.contextPath = contextPath;
    }

    @Nonnull
    @Override
    public List<String> map(@Nonnull final String resourceUrl) {
        if (!webResourceIntegration.isCtCdnMappingEnabled()) {
            return emptyList();
        }
        final List<String> mappedResources = this.mappings.getMappedResources(removeContext(resourceUrl));
        if (!mappedResources.isEmpty()) {
            log.debug("Mapped resource {} to {}", resourceUrl, mappedResources);
        } else {
            log.debug("Cache miss for resource {}", resourceUrl);
        }
        return mappedResources.stream().map(this::joinBaseUrlSafely).collect(Collectors.toList());
    }

    private String removeContext(String resourceUrl) {
        return contextPath == null || contextPath.isEmpty() || !resourceUrl.startsWith(contextPath) ? resourceUrl :
                resourceUrl.substring(contextPath.length());
    }

    private String joinBaseUrlSafely(String resourcePath) {
        boolean baseUrlHasSlash = ctCdnBaseUrl.endsWith("/");
        boolean resourcePathHasSlash = resourcePath.startsWith("/");
        if (baseUrlHasSlash && resourcePathHasSlash) {
            return ctCdnBaseUrl.substring(0, ctCdnBaseUrl.length() - 1) + resourcePath;
        } else if (!baseUrlHasSlash && !resourcePathHasSlash) {
            return ctCdnBaseUrl + "/" + resourcePath;
        } else {
            return ctCdnBaseUrl + resourcePath;
        }
    }

    @Nonnull
    @Override
    public Optional<String> mapSingle(@Nonnull String resourceUrl) {
        return map(resourceUrl).stream().findFirst();
    }

    private MappingSet loadMappings(
            final MappingParser mappingParser,
            final String globalStateHash,
            final PrebakeConfig prebakeConfig) throws MappingParserException, IOException {
        final long startTime = System.nanoTime();
        try {
            final File f = prebakeConfig.getMappingLocation(globalStateHash);
            log.info("Loading pre-baked CT-CDN mappings from '{}' with hash '{}'", f, globalStateHash);
            try (InputStreamReader reader = new InputStreamReader(new FileInputStream(f), StandardCharsets.UTF_8)) {
                MappingSet mappings = mappingParser.parse(reader);
                if (mappings.size() == 0) {
                    log.warn("Mappings loaded but empty! No CT-CDN pre-baked resources will be applied but will fall back to product's CDN Strategy, if any.");
                } else {
                    log.info("Mappings loaded with {} entries.", mappings.size());
                }
                return mappings;
            }
        } catch (FileNotFoundException e) {
            log.info("Mappings file not found. This is not an error but means pre-baked CT-CDN is disabled but will fall back to product's CDN Strategy, if any.");
            throw e;
        } catch (MappingParserException | IOException e) {
            log.warn("Could not load mappings from pattern '{}' with hash '{}'. Pre-baked CT-CDN is disabled but will fall back to product's CDN Strategy, if any.",
                    new Object[]{prebakeConfig.getPattern(), globalStateHash, e});
            throw e;
        } catch (RuntimeException e) {
            log.warn("Unexpected exception while loading mappings from pattern '{}' with hash '{}'. Pre-baked CT-CDN is disabled but will fall back to product's CDN Strategy, if any.",
                    new Object[]{prebakeConfig.getPattern(), globalStateHash, e});
            throw e;
        } finally {
            log.debug("Duration -> {} ms", System.nanoTime() - startTime);
        }
    }

    @Nonnull
    @Override
    public MappingSet mappings() {
        return mappings;
    }
}
