package com.atlassian.plugin.webresource.impl.annotators;

import com.atlassian.plugin.webresource.impl.snapshot.ContextResource;
import com.atlassian.plugin.webresource.impl.snapshot.ModuleResource;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.Map;

import static com.atlassian.plugin.webresource.impl.config.Config.JS_TYPE;
import static com.atlassian.plugin.webresource.impl.config.Config.virtualContextKeyToWebResourceKey;

/**
 * Add comment with information about the single resource included in batch.
 *
 * @since 3.3
 */
public class LocationContentAnnotator extends ResourceContentAnnotator {
    @Override
    public int beforeResourceInBatch(LinkedHashSet<String> requiredResources, Resource resource, final Map<String, String> params, OutputStream out) throws IOException {
        if (resource instanceof ModuleResource) {
            // No need to print location comment for JS modules because they already have it in the module definition.
            if (!JS_TYPE.equals(resource.getNameOrLocationType())) {
                out.write(String.format("/* module = '%s' */\n", resource.getKey()).getBytes());
                return 1;
            } else {
                return 0;
            }
        } else {
            if (resource instanceof ContextResource) {
                out.write(("/* loading modules for context \"" + virtualContextKeyToWebResourceKey(resource.getParent().getKey()) + "\" */\n").getBytes());
            } else {
                out.write(String.format("/* module-key = '%s', location = '%s' */\n", resource.getKey(),
                        resource.getLocation()).getBytes());
            }
            return 1;
        }
    }

    @Override
    public int hashCode() {
        return getClass().getName().hashCode();
    }
}