package com.atlassian.plugin.webresource.impl.helpers;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.ContextSubBatchResourceUrl;
import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.plugin.webresource.ResourceUrlImpl;
import com.atlassian.plugin.webresource.WebResourceSubBatchUrl;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.plugin.webresource.impl.support.UrlCache;
import com.atlassian.plugin.webresource.impl.support.http.BaseRouter;
import com.atlassian.plugin.webresource.legacy.LegacyUrlGenerationHelpers;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.webresource.impl.RequestCache.toResourceKeys;
import static com.google.common.base.Predicates.not;

/**
 * Stateless helper functions providing basic support for resource url generation.
 *
 * @since v3.3
 */
public class UrlGenerationHelpers extends BaseHelpers {
    /**
     * Data structure representing Web Resource Batch Key.
     */
    public static class ContextBatchKey {
        public final List<String> included;
        public LinkedHashSet<String> excluded;

        public ContextBatchKey(List<String> included, LinkedHashSet<String> excluded) {
            this.included = included;
            this.excluded = excluded;
        }

        @Override
        public String toString() {
            StringBuilder buff = new StringBuilder();
            buff.append("[").append(StringUtils.join(included, ", "));
            if (excluded.size() > 0) {
                buff.append("-");
                buff.append(StringUtils.join(excluded, ", "));
            }
            return buff.append("]").toString();
        }
    }

    /**
     * Data structure representing Web Resource Batch, it is used in cache and shouldn't contain any references.
     */
    public static class WebResourceBatch {
        public final String key;
        public final List<SubBatch> subBatches;
        protected final Map<String, RequestCache.ResourceKeysSupplier> standaloneResourceKeysByType = new
                HashMap<String, RequestCache.ResourceKeysSupplier>();

        public WebResourceBatch(String key, List<SubBatch> subBatches, List<Resource> standaloneResources) {
            this.key = key;
            this.subBatches = subBatches;
            for (String type : Config.BATCH_TYPES) {
                // Instead of storing the reference to the resources storing its keys.
                RequestCache.ResourceKeysSupplier resourceKeysSupplier =
                        new RequestCache.ResourceKeysSupplier(toResourceKeys(resourcesOfType(standaloneResources, type)));
                standaloneResourceKeysByType.put(type, resourceKeysSupplier);
            }
        }

        public List<Resource> getStandaloneResourcesOfType(RequestCache requestCache, String type) {
            return requestCache.getCachedResources(standaloneResourceKeysByType.get(type));
        }
    }

    /**
     * Data structure representing Context Batch, it is used in cache and shouldn't contain any references.
     */
    public static class ContextBatch {
        public final List<String> included;
        public final LinkedHashSet<String> excluded;
        public final List<String> skippedWebResourcesWithUrlReadingConditions;
        public final List<String> excludedWithoutApplyingConditions;
        public final List<SubBatch> subBatches;
        protected final Map<String, RequestCache.ResourceKeysSupplier> standaloneResourceKeysByType = new HashMap<>();
        public final boolean isAdditionalSortingRequired;

        public ContextBatch(List<String> included, LinkedHashSet<String> excluded,
                            List<String> skippedWebResourcesWithUrlReadingConditions, List<String> excludedWithoutApplyingConditions,
                            List<SubBatch> subBatches, List<Resource> standaloneResources, boolean isAdditionalSortingRequired) {
            this.included = included;
            this.excluded = excluded;
            this.skippedWebResourcesWithUrlReadingConditions = skippedWebResourcesWithUrlReadingConditions;
            this.excludedWithoutApplyingConditions = excludedWithoutApplyingConditions;
            this.subBatches = subBatches;
            for (String type : Config.BATCH_TYPES) {
                // Instead of storing the reference to the resources storing its keys.
                RequestCache.ResourceKeysSupplier resourceKeysSupplier =
                        new RequestCache.ResourceKeysSupplier(toResourceKeys(resourcesOfType(standaloneResources, type)));
                standaloneResourceKeysByType.put(type, resourceKeysSupplier);
            }
            this.isAdditionalSortingRequired = isAdditionalSortingRequired;
        }

        public List<Resource> getStandaloneResourcesOfType(RequestCache requestCache, String type) {
            return requestCache.getCachedResources(standaloneResourceKeysByType.get(type));
        }

        @Override
        public String toString() {
            StringBuilder buff = new StringBuilder();
            buff.append(StringUtils.join(included, ", "));
            if (excluded.size() > 0) {
                buff.append("-");
                buff.append(StringUtils.join(excluded, ", "));
            }
            return buff.toString();
        }
    }

    /**
     * Data structure representing Sub Batch of both Web Resource Batch and Context Batch, it is used in cache and
     * shouldn't contain any references.
     */
    public static class SubBatch {
        public final Map<String, String> resourcesParams;
        // Bundles can't be inferred from resourceKeys because there could be an empty bundle without any
        // resources but with condition and dependencies.
        // It is ok to have reference to Bundle because it is anyway stored in Snapshot cache.
        public final List<Bundle> bundles;
        private final Map<String, RequestCache.ResourceKeysSupplier> resourceKeysByType =
                new HashMap<>();

        public SubBatch(Map<String, String> resourcesParams, Bundle bundle, List<Resource> resources) {
            this(resourcesParams, Lists.newArrayList(bundle), resources);
        }

        public SubBatch(Map<String, String> resourcesParams, List<Bundle> bundles, List<Resource> resources) {
            this.resourcesParams = resourcesParams;
            this.bundles = bundles;
            for (String type : Config.BATCH_TYPES) {
                // Instead of storing the reference to the resources storing its keys.
                RequestCache.ResourceKeysSupplier resourceKeysSupplier =
                        new RequestCache.ResourceKeysSupplier(toResourceKeys(resourcesOfType(resources, type)));
                resourceKeysByType.put(type, resourceKeysSupplier);
            }
        }

        public List<Resource> getResourcesOfType(RequestCache requestCache, String type) {
            return requestCache.getCachedResources(resourceKeysByType.get(type));
        }
    }

    /**
     * DTO.
     */
    public static class Resolved {
        public final LinkedHashMap<String, Jsonable> data;
        public final List<ResourceUrl> urls;
        public final LinkedHashSet<String> excludedResolved;

        Resolved(LinkedHashMap<String, Jsonable> data, List<ResourceUrl> urls, LinkedHashSet<String> excludedResolved) {
            this.data = data;
            this.urls = urls;
            this.excludedResolved = excludedResolved;
        }
    }

    /**
     * Resolve given included and excluded resources.
     *
     * @return data resources, url resources, resolved excluded resources.
     */
    public static Resolved resolve(final RequestState requestState) {
        if (requestState.getIncluded().isEmpty() && requestState.getIncludedData().isEmpty()) {
            return new Resolved(new LinkedHashMap<>(), new ArrayList<>(), requestState.getExcluded());
        }

        CalculatedBatches calculatedBatches;
        if (requestState.getGlobals().getConfig().isUrlCachingEnabled()) {
            UrlCache.IncludedExcludedConditionsAndBatchingOptions cacheKey = buildIncludedExcludedConditionsAndBatchingOptions(
                    requestState.getCache(), requestState.getUrlStrategy(),
                    requestState.getIncluded(), requestState.getExcluded());

            // Calculating
            calculatedBatches = requestState.getGlobals().getUrlCache().getBatches(cacheKey, key -> {
                return calculateBatches(requestState.getCache(), requestState.getUrlStrategy(), key.getIncluded(), key.getExcluded());
            });
        } else {
            calculatedBatches = calculateBatches(requestState.getCache(), requestState.getUrlStrategy(), requestState.getIncluded(), requestState.getExcluded());
        }

        // Assembling data resources.
        List<String> dataResources = requestState.getSnapshot().find()
                .included(requestState.getIncluded())
                .excluded(requestState.getExcluded(), isConditionsSatisfied(requestState.getCache(), requestState.getUrlStrategy()))
                .deepFilter(isConditionsSatisfied(requestState.getCache(), requestState.getUrlStrategy()))
                .end();

        // Building data resources.
        LinkedHashMap<String, Jsonable> dataJsonResources = buildDataResources(requestState, dataResources);

        // Collecting transformer parameters and assembling resource urls.
        List<ResourceUrl> resourceUrls = collectUrlStateAndBuildResourceUrls(requestState, requestState.getUrlStrategy(),
                calculatedBatches.contextBatches, calculatedBatches.webResourceBatches);

        return new Resolved(dataJsonResources, resourceUrls, calculatedBatches.excludedResolved);
    }

    /**
     * Building the cache key of included, excluded and conditions.
     */
    protected static UrlCache.IncludedExcludedConditionsAndBatchingOptions buildIncludedExcludedConditionsAndBatchingOptions(
            RequestCache requestCache, UrlBuildingStrategy urlBuilderStrategy,
            LinkedHashSet<String> included, LinkedHashSet<String> excluded) {
        // requestState could be modified in future, creating new included and excluded sets.
        UrlCache.IncludedAndExcluded includedAndExcluded = new UrlCache.IncludedAndExcluded(
                new LinkedHashSet<>(included), new LinkedHashSet<>(excluded));

        Set<CachedCondition> conditions = getConditions(requestCache, includedAndExcluded, urlBuilderStrategy);

        // Evaluating conditions.
        Set<UrlCache.EvaluatedCondition> evaluatedConditions = new HashSet<>();
        for (CachedCondition condition : conditions) {
            evaluatedConditions.add(new UrlCache.EvaluatedCondition(condition, condition.evaluateSafely(requestCache, urlBuilderStrategy)));
        }

        // Building cache key. Instead of using list of all the condition we using only set of unique conditions, it
        // should be enough.
        return new UrlCache.IncludedExcludedConditionsAndBatchingOptions(includedAndExcluded, evaluatedConditions,
                requestCache.getGlobals().getConfig().resplitMergedContextBatchesForThisRequest());
    }

    /**
     * Get web resources with conditions, the subset of the resolve web resources for given set of included and excluded.
     */
    protected static Set<CachedCondition> getConditions(final RequestCache requestCache,
                                                        UrlCache.IncludedAndExcluded includedAndExcluded, final UrlBuildingStrategy urlBuilderStrategy) {
        // Getting list of conditions by traversing the dependency graph. It's optimised, if resource has
        // condition evaluated to false - its dependencies will be skipped.
        Set<CachedCondition> conditions = new HashSet<>();
        requestCache.getSnapshot().find()
                .included(includedAndExcluded.getIncluded())
                .excluded(includedAndExcluded.getExcluded(), isConditionsSatisfied(requestCache, urlBuilderStrategy))
                .deepFilter(bundle -> {
                    CachedCondition condition = bundle.getCondition();
                    if (condition != null) {
                        conditions.add(condition);

                        if (condition.isLegacy()) {
                            // Legacy conditions can't be optimised, if it is a legacy condition we'll continue to
                            // check its dependencies even if it's false.
                            return true;
                        } else {
                            // If condition will be evaluated to false, the conditions for its dependencies will be
                            // skipped.
                            return condition.evaluateSafely(requestCache, urlBuilderStrategy);
                        }
                    }
                    return true;
                })
                .end();
        return conditions;
    }

    /**
     * Resolves list of excluded resources.
     *
     * @param requestCache request cache.
     * @param allIncluded  included web resources and contexts.
     * @param allExcluded  excluded web resources and contexts.
     * @return list of actually excluded web resources and contexts.
     */
    public static LinkedHashSet<String> resolveExcluded(final RequestCache requestCache,
                                                        UrlBuildingStrategy urlBuilderStrategy,
                                                        final List<String> allIncluded, final LinkedHashSet<String> allExcluded) {
        if (requestCache.getGlobals().getConfig().isUrlCachingEnabled()) {
            UrlCache.IncludedExcludedConditionsAndBatchingOptions cacheKey = buildIncludedExcludedConditionsAndBatchingOptions(
                    requestCache, urlBuilderStrategy, new LinkedHashSet<>(allIncluded), allExcluded);

            return requestCache.getGlobals().getUrlCache().getResolvedExcluded(cacheKey, key -> {
                return LegacyUrlGenerationHelpers.calculateBatches(requestCache, urlBuilderStrategy, key.getIncluded(),
                        key.getExcluded(), false).excludedResolved;
            });
        } else {
            return LegacyUrlGenerationHelpers.calculateBatches(requestCache, urlBuilderStrategy, new LinkedHashSet<>(allIncluded),
                    allExcluded, false).excludedResolved;
        }
    }

    /**
     * DTO.
     */
    public static class CalculatedBatches {
        public final List<Helpers.ContextBatch> contextBatches;
        public final List<Helpers.WebResourceBatch> webResourceBatches;
        public final LinkedHashSet<String> excludedResolved;

        public CalculatedBatches(List<UrlGenerationHelpers.ContextBatch> contextBatches,
                                 List<UrlGenerationHelpers.WebResourceBatch> webResourceBatches, LinkedHashSet<String> excludedResolved) {
            this.contextBatches = contextBatches;
            this.webResourceBatches = webResourceBatches;
            this.excludedResolved = excludedResolved;
        }
    }

    /**
     * Calculate batches.
     */
    protected static CalculatedBatches calculateBatches(
            RequestCache requestCache, UrlBuildingStrategy urlBuilderStrategy, LinkedHashSet<String> included, LinkedHashSet<String> excluded) {
        LegacyUrlGenerationHelpers.Resolved resolved =
                LegacyUrlGenerationHelpers.calculateBatches(requestCache, urlBuilderStrategy, included, excluded, false);

        // Splitting list of batches into list of sub-batches.
        Tuple<List<ContextBatch>, List<WebResourceBatch>> subBatches =
                splitIntoSubBatches(requestCache, urlBuilderStrategy, resolved.contextBatchKeys, resolved.webResourceBatchKeys);
        List<ContextBatch> contextBatches = subBatches.getFirst();
        List<WebResourceBatch> webResourceBatches = subBatches.getLast();

        return new CalculatedBatches(contextBatches, webResourceBatches, resolved.excludedResolved);
    }

    /**
     * Build data resources.
     */
    protected static LinkedHashMap<String, Jsonable> buildDataResources(RequestState requestState,
                                                                        List<String> webResourceKeys) {
        LinkedHashMap<String, Jsonable> dataResources = new LinkedHashMap<>();

        for (Bundle bundle : requestState.getSnapshot().toBundles(webResourceKeys)) {
            for (Map.Entry<String, Jsonable> entry : bundle.getData().entrySet()) {
                dataResources.put(bundle.getKey() + "." + entry.getKey(), entry.getValue());
            }
        }

        // Adding explicitly required data.
        for (Map.Entry<String, Jsonable> entry : requestState.getIncludedData().entrySet()) {
            if (!requestState.getExcludedData().contains(entry.getKey())) {
                dataResources.put(entry.getKey(), entry.getValue());
            }
        }
        return dataResources;
    }

    /**
     * Collect URL State for Conditions and Transformers and build Resource URLs.
     *
     * @return list of Resource URLs.
     */
    protected static List<ResourceUrl> collectUrlStateAndBuildResourceUrls(
            RequestState requestState, UrlBuildingStrategy urlBuilderStrategy,
            List<ContextBatch> contextBatches, List<WebResourceBatch> webResourceBatches) {
        RequestCache requestCache = requestState.getCache();
        Globals globals = requestCache.getGlobals();
        List<ResourceUrl> resourceUrls = new ArrayList<>();
        for (String type : Config.BATCH_TYPES) {
            // Assembling resource urls for context batches.
            for (ContextBatch contextBatch : contextBatches) {
                List<String> excludedResolvedWithoutApplyingConditions = requestCache.getSnapshot().find()
                        .included(contextBatch.excludedWithoutApplyingConditions)
                        .end();

                List<ContextSubBatchResourceUrl> contextBatchResourceUrls = new ArrayList<>();
                for (SubBatch subBatch : contextBatch.subBatches) {
                    if (!subBatch.getResourcesOfType(requestCache, type).isEmpty()) {
                        StateEncodedUrlResult taintAndUrlBuilder = encodeStateInUrlIfSupported(requestCache, urlBuilderStrategy,
                                type, subBatch.resourcesParams, subBatch.bundles,
                                requestCache.getSnapshot().toBundles(contextBatch.skippedWebResourcesWithUrlReadingConditions),
                                requestCache.getSnapshot().toBundles(excludedResolvedWithoutApplyingConditions));
                        boolean taint = taintAndUrlBuilder.isTaint();
                        DefaultUrlBuilder urlBuilder = taintAndUrlBuilder.getUrlBuilder();

                        // Creating resource url.
                        contextBatchResourceUrls.add(new ContextSubBatchResourceUrl(requestCache.getGlobals(),
                                contextBatch, subBatch, type, urlBuilder.buildParams(), urlBuilder.buildHash(), taint,
                                urlBuilder.getPrebakeErrors()));
                    }
                }

                // Sorting batch by urls, it's not needed and exists only for backward compatibility,
                // because for some unknown reason in the previous version sub-batches where sorted by its urls.
                // It exists only to ease backward compatibility testing, after some time this code should
                // be removed.
                if (contextBatch.isAdditionalSortingRequired) {
                    final ParamsComparator paramsComparator = new ParamsComparator();
                    Collections.sort(contextBatchResourceUrls, new Comparator<ResourceUrl>() {
                        @Override
                        public int compare(ResourceUrl a, ResourceUrl b) {
                            int result = paramsComparator.compare(a.getParams(), b.getParams());
                            // Only the part of the url with parameters should be compared, because hashes could
                            // be different and change sorting.
                            String aUrl = BaseRouter.buildUrl("", a.getParams());
                            String bUrl = BaseRouter.buildUrl("", b.getParams());
                            return result == 0 ? aUrl.compareTo(bUrl) : result;
                        }
                    });
                }

                if (globals.getConfig().isContextBatchingEnabled()) {
                    resourceUrls.addAll(contextBatchResourceUrls);
                } else {
                    // Destructing context batch into web resource batches or into individual resources.
                    for (ContextSubBatchResourceUrl contextSubBatchResourceUrl : contextBatchResourceUrls) {
                        Map<String, String> contextBatchResourceParams = contextSubBatchResourceUrl.getSubBatch().resourcesParams;
                        SubBatch subBatch = contextSubBatchResourceUrl.getSubBatch();
                        List<Resource> contextBatchResources = subBatch.getResourcesOfType(requestCache, type);

                        // Getting list of bundles.
                        Map<Bundle, List<Resource>> bundles = new LinkedHashMap<>();
                        for (Resource resource : contextBatchResources) {
                            List<Resource> bundleResources = bundles.get(resource.getParent());
                            if (bundleResources == null) {
                                bundleResources = new ArrayList<>();
                                bundles.put(resource.getParent(), bundleResources);
                            }
                            bundleResources.add(resource);
                        }

                        // Calculating web resource batches.
                        for (Map.Entry<Bundle, List<Resource>> entry : bundles.entrySet()) {
                            Bundle bundle = entry.getKey();
                            List<Resource> resources = entry.getValue();

                            // Calculating parameters for web batch.
                            StateEncodedUrlResult taintAndUrlBuilder = encodeStateInUrlIfSupported(requestCache, urlBuilderStrategy,
                                    type, contextBatchResourceParams, bundle, new ArrayList<>(), new ArrayList<>());
                            boolean taint = taintAndUrlBuilder.isTaint();
                            DefaultUrlBuilder urlBuilder = taintAndUrlBuilder.getUrlBuilder();
                            String webResourceBatchHash = urlBuilder.buildHash();
                            Map<String, String> webResourceBatchHttpParams = urlBuilder.buildParams();

                            if (globals.getConfig().isWebResourceBatchingEnabled()) {
                                // Creating web resource batch url.
                                SubBatch webResourceSubBatch = new SubBatch(webResourceBatchHttpParams, bundle, resources);
                                resourceUrls.add(new WebResourceSubBatchUrl(requestCache.getGlobals(), bundle.getKey(),
                                        webResourceSubBatch, type, webResourceBatchHttpParams, webResourceBatchHash, taint,
                                        urlBuilder.getPrebakeErrors()));
                            } else {
                                // Creating resources url.
                                for (Resource resource : resources) {
                                    resourceUrls.add(new ResourceUrlImpl(globals, resource, webResourceBatchHttpParams, webResourceBatchHash, taint,
                                            urlBuilder.getPrebakeErrors()));
                                }
                            }
                        }
                    }
                }

                resourceUrls.addAll(createResourceUrlsForRedirectResources(requestCache, urlBuilderStrategy,
                        contextBatch.getStandaloneResourcesOfType(requestCache, type)));
            }

            // Assembling resource urls for all web resources, usually it's resources with legacy stuff or special legacy case
            // when the web-resource has been excluded.
            for (WebResourceBatch webResourceBatch : webResourceBatches) {
                for (SubBatch subBatch : webResourceBatch.subBatches) {
                    List<Resource> resources = subBatch.getResourcesOfType(requestCache, type);
                    if (resources.size() > 0) {
                        StateEncodedUrlResult taintAndUrlBuilder = encodeStateInUrlIfSupported(requestCache, urlBuilderStrategy,
                                type, subBatch.resourcesParams, subBatch.bundles, new ArrayList<>(), new ArrayList<>());
                        boolean taint = taintAndUrlBuilder.isTaint();
                        DefaultUrlBuilder urlBuilder = taintAndUrlBuilder.getUrlBuilder();

                        if (globals.getConfig().isWebResourceBatchingEnabled()) {
                            resourceUrls.add(new WebResourceSubBatchUrl(requestCache.getGlobals(), webResourceBatch.key,
                                    subBatch, type, urlBuilder.buildParams(), urlBuilder.buildHash(), taint, urlBuilder.getPrebakeErrors()));
                        } else {
                            for (Resource resource : resources) {
                                resourceUrls.add(new ResourceUrlImpl(globals, resource, urlBuilder.buildParams(),
                                        urlBuilder.buildHash(), taint, urlBuilder.getPrebakeErrors()));
                            }
                        }
                    }
                }
                resourceUrls.addAll(createResourceUrlsForRedirectResources(requestCache, urlBuilderStrategy,
                        webResourceBatch.getStandaloneResourcesOfType(requestCache, type)));
            }

            // If module has been injected in the context it should be required immediately.
            // Normally it would happen automatically but not in this legacy case.
            // We need to explicitly handle it.
            if (globals.getConfig().amdEnabled() && !webResourceBatches.isEmpty()) {
                // Getting all the contexts and requiring modules that has been injected into it.
                for (String key : requestState.getIncluded()) {
                    if (Config.isContextKey(key) && !requestState.getExcluded().contains(key)) {
                        Bundle bundle = requestCache.getSnapshot().get(key);
                        if (bundle != null) {
                            for (Resource resource : bundle.getResources(requestCache).values()) {
                                // Context resources are virtual and doesn't have any transformers or conditions, so
                                // there's no need to calculate parameters or hashes.
                                if (type.equals(resource.getNameOrLocationType())) {
                                    // There's no UrlBuilder, so there's no pre-bake errors, using the empty list.
                                    resourceUrls.add(new ResourceUrlImpl(globals, resource, new HashMap<>(), "", false, new ArrayList<>()));
                                }
                            }
                        }
                    }
                }
            }

        }
        return resourceUrls;
    }

    /**
     * Split batches into sub batches grouped by URL parameters and set of standalone resources.
     *
     * @param contextBatchKeys     keys of context batches
     * @param webResourceBatchKeys keys of web resource batches.
     * @return context batches and web resource batches split into sub batches.
     */
    protected static Tuple<List<ContextBatch>, List<WebResourceBatch>> splitIntoSubBatches(
            RequestCache requestCache,
            UrlBuildingStrategy urlBuilderStrategy,
            List<ContextBatchKey> contextBatchKeys, List<String> webResourceBatchKeys) {
        // Processing context batches.
        List<ContextBatch> contextBatches = new ArrayList<>();
        for (ContextBatchKey key : contextBatchKeys) {
            Found found = requestCache.getSnapshot().find()
                    .included(key.included)
                    .excluded(key.excluded, isConditionsSatisfied(requestCache, urlBuilderStrategy))
                    .deepFilter(isConditionsSatisfied(requestCache, urlBuilderStrategy))
                    .deepFilter(not(hasLegacyCondition()))
                    .endAndGetResult();

            // Only UrlReadingConditions should be in Context Batch, removing any legacy conditions.
            List<String> skippedWebResourcesWithUrlReadingConditions = new ArrayList<>();
            for (Bundle bundle : requestCache.getSnapshot().toBundles(found.skipped)) {
                if ((bundle.getCondition() != null) && !bundle.getCondition().isLegacy()) {
                    skippedWebResourcesWithUrlReadingConditions.add(bundle.getKey());
                }
            }

            // We need to add url parameters for excluded resources too, see PLUGWEB-339.
            LinkedHashSet<String> excludedWithoutApplyingConditions = key.excluded;

            SplitSubBatches result = splitBatchIntoSubBatches(requestCache, found.found, true);
            contextBatches.add(new ContextBatch(key.included, key.excluded, skippedWebResourcesWithUrlReadingConditions,
                    new ArrayList<>(excludedWithoutApplyingConditions), result.contextSubBatches, result.contextStandaloneResources,
                    result.isAdditionalSortingRequired));
        }

        // Processing web resource batches.
        List<WebResourceBatch> webResourceBatches = new ArrayList<>();
        {
            // Splitting into sub batches.
            for (String key : webResourceBatchKeys) {
                List<String> keys = new ArrayList<>();
                keys.add(key);
                Found found = requestCache.getSnapshot().find()
                        .included(keys)
                        .deep(false)
                        .deepFilter(isConditionsSatisfied(requestCache, urlBuilderStrategy))
                        .endAndGetResult();
                SplitSubBatches result = splitBatchIntoSubBatches(requestCache, found.found, false);
                if (result.contextSubBatches.size() > 0 && result.legacyWebResources.size() == 0) {
                    webResourceBatches.add(
                            new WebResourceBatch(key, result.contextSubBatches, result.contextStandaloneResources));
                } else if (result.contextSubBatches.size() == 0 && result.legacyWebResources.size() > 0) {
                    if (result.contextStandaloneResources.size() > 0) {
                        throw new RuntimeException("single web resource cannot have context standalone resources!");
                    }
                    if (result.legacyWebResources.size() > 1) {
                        throw new RuntimeException("single web resource cannot split into multiple web resources!");
                    }
                    webResourceBatches.add(result.legacyWebResources.get(0));
                } else if (contextBatchKeys.size() > 0 && result.legacyWebResources.size() > 0) {
                    throw new RuntimeException("single web resource batch could be either legacy or not, "
                            + "but not both at the same time!");
                }
            }
        }

        return new Tuple<>(contextBatches, webResourceBatches);
    }

    /**
     * Temporary data structure used during splitting batches into sub batches.
     */
    protected static class SplitSubBatches {
        List<SubBatch> contextSubBatches;
        List<Resource> contextStandaloneResources;
        List<WebResourceBatch> legacyWebResources;
        boolean isAdditionalSortingRequired;
    }

    /**
     * Split batch into sub batches and standalone resources, same code used to split both context and resource batches.
     *
     * @param completeKeys list of complete keys that the batch contains.
     * @param doSorting    some legacy stuff, in some cases it sorted in some not.
     * @return batch split into sub batches and
     */
    protected static SplitSubBatches splitBatchIntoSubBatches(RequestCache requestCache, Iterable<String> completeKeys,
                                                              boolean doSorting) {
        final String IS_STANDALONE = "_isStandalone";
        List<Bundle> bundles = requestCache.getSnapshot().toBundles(completeKeys);

        // During splitting into sub-batches the order of standalone resources would be lost,
        // this comparator needed to restore the right order.
        final Map<Resource, Integer> allResourcesOrdered = new HashMap<>();
        int i = 0;
        Comparator<Resource> RESOURCE_COMPARATOR = new Comparator<Resource>() {
            @Override
            public int compare(final Resource a, final Resource b) {
                return allResourcesOrdered.get(a) - allResourcesOrdered.get(b);
            }
        };

        // Getting list of unique params combinations, each combination would produce one sub-batch.
        Map<Map<String, String>, List<Resource>> uniqueParams = new LinkedHashMap<>();
        LinkedHashMap<Bundle, Map<Map<String, String>, List<Resource>>> legacyUniqueParams =
                new LinkedHashMap<>();
        for (Bundle bundle : bundles) {
            for (Resource resource : bundle.getResources(requestCache).values()) {
                allResourcesOrdered.put(resource, i);
                i += 1;

                Map<String, String> params = resource.getUrlParams();
                if (!resource.isBatchable()) {
                    params.put(IS_STANDALONE, "true");
                }

                // Checking only for the legacy conditions and ignoring legacy transformers.
                if (!resource.getParent().hasLegacyConditions()) {
                    List<Resource> resources = uniqueParams.get(params);
                    if (resources == null) {
                        resources = new ArrayList<>();
                        uniqueParams.put(params, resources);
                    }
                    resources.add(resource);
                } else {
                    Map<Map<String, String>, List<Resource>> webResourceUniqueParams =
                            legacyUniqueParams.get(resource.getParent());
                    if (webResourceUniqueParams == null) {
                        webResourceUniqueParams = new LinkedHashMap<>();
                        legacyUniqueParams.put(resource.getParent(), webResourceUniqueParams);
                    }
                    List<Resource> resources = webResourceUniqueParams.get(params);
                    if (resources == null) {
                        resources = new ArrayList<>();
                        webResourceUniqueParams.put(params, resources);
                    }
                    resources.add(resource);
                }
            }
        }

        // Sorting list of unique params combination, the sub-batches would be sorted in the same way.
        SplitSubBatches result = new SplitSubBatches();
        List<Map<String, String>> uniqueParamsSorted = new ArrayList<>(uniqueParams.keySet());
        ParamsComparator paramsComparator = new ParamsComparator();
        if (doSorting) {
            Collections.sort(uniqueParamsSorted, paramsComparator);
        }
        result.isAdditionalSortingRequired = paramsComparator.isAdditionalSortingRequired();

        // Sorting list of unique params combination for legacy web resources, the sub-batches would be sorted in the
        // same way.
        Map<Bundle, List<Map<String, String>>> legacyUniqueParamsSorted = new LinkedHashMap<>();
        for (Map.Entry<Bundle, Map<Map<String, String>, List<Resource>>> entry : legacyUniqueParams.entrySet()) {
            List<Map<String, String>> webResourceUniqueParamsSorted = new ArrayList<>(entry.getValue().keySet());
            // According to the previous implementation the sub batches for web resources should be never sorted.
            // if (doSorting)
            // {
            //     Collections.sort(webResourceUniqueParamsSorted, PARAMS_COMPARATOR);
            // }
            legacyUniqueParamsSorted.put(entry.getKey(), webResourceUniqueParamsSorted);
        }

        // Assembling sub batches.
        result.contextSubBatches = new ArrayList<>();
        result.contextStandaloneResources = new ArrayList<>();
        for (Map<String, String> params : uniqueParamsSorted) {
            if (params.containsKey(IS_STANDALONE)) {
                result.contextStandaloneResources.addAll(uniqueParams.get(params));
            } else {
                result.contextSubBatches.add(new SubBatch(params, bundles, uniqueParams.get(params)));
            }
        }
        Collections.sort(result.contextStandaloneResources, RESOURCE_COMPARATOR);

        // Assembling legacy sub batches, it should be last and should be re-grouped into web resource batches.
        result.legacyWebResources = new ArrayList<>();
        for (Map.Entry<Bundle, Map<Map<String, String>, List<Resource>>> entry : legacyUniqueParams.entrySet()) {
            List<Resource> webResourceStandaloneResources = new ArrayList<>();
            List<SubBatch> webResourceSubBatches = new ArrayList<>();
            for (Map<String, String> params : legacyUniqueParamsSorted.get(entry.getKey())) {
                if (params.containsKey(IS_STANDALONE)) {
                    webResourceStandaloneResources.addAll(uniqueParams.get(params));
                } else {
                    List<Resource> resources = legacyUniqueParams.get(entry.getKey()).get(params);
                    webResourceSubBatches.add(new SubBatch(params, entry.getKey(), resources));
                }
            }
            Collections.sort(webResourceStandaloneResources, RESOURCE_COMPARATOR);
            result.legacyWebResources.add(
                    new WebResourceBatch(entry.getKey().getKey(), webResourceSubBatches, webResourceStandaloneResources));
        }

        return result;
    }

    private static final HashSet<String> PARAMS_SORT_ORDER_SET = new HashSet<>(Config.PARAMS_SORT_ORDER);

    /**
     * Comparator for sorting sub batches.
     */
    public static final class ParamsComparator implements Comparator<Map<String, String>> {
        // The only reason this exists it to preserve backward compatibility when sub-batches where sorted
        // by its urls.
        private boolean isAdditionalSortingRequired = false;

        /**
         * Keys will be sorted according to {@link Config#PARAMS_SORT_ORDER}.
         */
        @Override
        public int compare(Map<String, String> a, Map<String, String> b) {
            // the fewer conditions, the high priority
            Set<String> aKeys = Sets.intersection(a.keySet(), PARAMS_SORT_ORDER_SET);
            Set<String> bKeys = Sets.intersection(b.keySet(), PARAMS_SORT_ORDER_SET);

            if ((aKeys.size() == 1) && (bKeys.size() == 1)) {
                String aKey = Iterables.getOnlyElement(aKeys);
                String bKey = Iterables.getOnlyElement(bKeys);

                int aIndex = Config.PARAMS_SORT_ORDER.indexOf(aKey);
                int bIndex = Config.PARAMS_SORT_ORDER.indexOf(bKey);

                if (aIndex != bIndex) {
                    return aIndex - bIndex;
                }
            }

            if (aKeys.size() == bKeys.size()) {
                // In old implementation sub batches where sorted by its urls, technically it should be
                // safe to sort it by query string only, as written below, but in order to ease
                // backward compatibility testing temporarily it is disabled and 100% backward compatibility
                // maintained, after some time this code should be uncommented and `isAdditionalSortingRequired`
                // stuff should be removed.
                //
                // String aQueryString = BaseRouter.buildUrl("", Resource.getUrlParamsStatic(a));
                // String bQueryString = BaseRouter.buildUrl("", Resource.getUrlParamsStatic(b));
                // return aQueryString.compareTo(bQueryString);
                //
                isAdditionalSortingRequired |= true;
                return 0;
            }

            return aKeys.size() - bKeys.size();
        }

        public boolean isAdditionalSortingRequired() {
            return isAdditionalSortingRequired;
        }
    }

    /**
     * Get resources of given type.
     */
    public static List<Resource> resourcesOfType(Collection<Resource> resources, String type) {
        List<Resource> result = new ArrayList<>();
        for (Resource resource : resources) {
            if (type.equals(resource.getNameOrLocationType())) {
                result.add(resource);
            }
        }
        return result;
    }

    /**
     * Collect and encode URL state for given Resources, it collects resource params, condition params and
     * transformer params.
     */
    protected static StateEncodedUrlResult encodeStateInUrlIfSupported(
            RequestCache requestCache, UrlBuildingStrategy urlBuilderStrategy,
            String type, Map<String, String> params, Bundle bundle, List<Bundle> skipped,
            List<Bundle> excludedWithoutApplyingConditions)

    {
        List<Bundle> bundles = new ArrayList<>();
        bundles.add(bundle);
        return encodeStateInUrlIfSupported(requestCache, urlBuilderStrategy, type, params, bundles, skipped,
                excludedWithoutApplyingConditions);
    }

    /**
     * Collect and encode URL state for given Resources, it collects resource params, condition params and
     * transformer params.
     */
    protected static StateEncodedUrlResult encodeStateInUrlIfSupported(
            RequestCache requestCache, UrlBuildingStrategy urlBuilderStrategy,
            String type, Map<String, String> params, List<Bundle> bundles, List<Bundle> skipped,
            List<Bundle> excludedWithoutApplyingConditions)

    {
        // Adding resource params.
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        boolean taint = false;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            urlBuilder.addToQueryString(entry.getKey(), entry.getValue());
        }

        // Adding parameters for UrlReadingConditions. The url parameters from failed and excluded UrlReadingConditions
        // also should be added.
        List<Bundle> bundlesWithSkippedAndExcluded = new ArrayList<>(bundles);
        bundlesWithSkippedAndExcluded.addAll(skipped);
        bundlesWithSkippedAndExcluded.addAll(excludedWithoutApplyingConditions);
        for (Bundle bundle : bundlesWithSkippedAndExcluded) {
            CachedCondition condition = bundle.getCondition();
            if (condition != null) {
                condition.addToUrlSafely(requestCache, urlBuilder, urlBuilderStrategy);
                taint |= condition.isLegacy();
            }
        }

        // Adding transformer params.
        for (Bundle bundle : bundles) {
            CachedTransformers transformers = bundle.getTransformers();
            if (transformers != null) {
                for (String locationType : bundle.getLocationResourceTypesFor(type)) {
                    taint |= transformers.addToUrlSafely(urlBuilder, urlBuilderStrategy, locationType, requestCache.getGlobals().getConfig()
                            .getTransformerCache(), bundle.getTransformerParameters(), bundle.getKey());
                }
            }
            for (String locationType : bundle.getLocationResourceTypesFor(type)) {
                requestCache.getGlobals().getConfig().getStaticTransformers().addToUrl(locationType, bundle.getTransformerParameters(), urlBuilder, urlBuilderStrategy);
            }
        }
        return new StateEncodedUrlResult(taint, urlBuilder);
    }

    /**
     * Creates Resource URLs for redirect resources.
     */
    protected static List<ResourceUrl> createResourceUrlsForRedirectResources(
            RequestCache requestCache, UrlBuildingStrategy urlBuilderStrategy,
            List<Resource> resources) {
        List<ResourceUrl> resourceUrls = new ArrayList<>();
        for (Resource resource : resources) {
            // Calculating params and hash.
            DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
            for (Map.Entry<String, String> entry : resource.getParams().entrySet()) {
                urlBuilder.addToQueryString(entry.getKey(), entry.getValue());
            }

            boolean taint = false;
            Bundle webResource = resource.getParent();
            CachedCondition condition = webResource.getCondition();
            if (condition != null) {
                condition.addToUrlSafely(requestCache, urlBuilder, urlBuilderStrategy);
                taint |= condition.isLegacy();
            }
            CachedTransformers transformers = webResource.getTransformers();
            if (transformers != null) {
                taint |= transformers.addToUrlSafely(urlBuilder, urlBuilderStrategy, resource.getLocationType(), requestCache.getGlobals().getConfig().getTransformerCache(), webResource.getTransformerParameters(), webResource.getKey());
            }
            requestCache.getGlobals().getConfig().getStaticTransformers().addToUrl(resource.getLocationType(), webResource.getTransformerParameters(), urlBuilder, urlBuilderStrategy);

            resourceUrls.add(new ResourceUrlImpl(requestCache.getGlobals(), resource, urlBuilder.buildParams(),
                    urlBuilder.buildHash(), taint, urlBuilder.getPrebakeErrors()));
        }
        return resourceUrls;
    }
}
