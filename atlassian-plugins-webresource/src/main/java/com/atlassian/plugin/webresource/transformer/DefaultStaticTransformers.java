package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.StreamSupport;

import static com.atlassian.webresource.api.prebake.Dimensions.empty;

/**
 * {@link com.atlassian.plugin.webresource.transformer.StaticTransformers} implementation.
 *
 * @since v3.1.0
 */
public class DefaultStaticTransformers implements StaticTransformers {
    private final StaticTransformersSupplier staticTransformersSupplier;
    private Set<String> paramKeys = new HashSet<>();

    public DefaultStaticTransformers(StaticTransformersSupplier staticTransformersSupplier) {
        this.staticTransformersSupplier = staticTransformersSupplier;
        staticTransformersSupplier.computeDimensions().cartesianProduct().forEach(coordinate -> {
            for (String key : coordinate.getKeys()) {
                paramKeys.add(key);
            }
        });
    }

    @Override
    public Dimensions computeDimensions() {
        return staticTransformersSupplier.computeDimensions();
    }

    @Override
    public Dimensions computeBundleDimensions(Bundle bundle) {
        if (bundle == null) {
            return empty();
        }
        RequestCache requestCache = new RequestCache(null);
        return bundle.getResources(requestCache)
                .values()
                .stream()
                .flatMap(r -> StreamSupport.stream(staticTransformersSupplier.get(r.getNameOrLocationType()).spliterator(), false))
                .map(DimensionAwareWebResourceTransformerFactory::computeDimensions)
                .reduce(Dimensions::product)
                .orElse(empty());
    }

    @Override
    public void addToUrl(String locationType, TransformerParameters transformerParameters, UrlBuilder urlBuilder, UrlBuildingStrategy urlBuildingStrategy) {
        for (DimensionAwareWebResourceTransformerFactory transformerFactory : transformersForType(locationType)) {
            urlBuildingStrategy.addToUrl(transformerFactory.makeUrlBuilder(transformerParameters), urlBuilder);
        }
    }

    @Override
    public Content transform(Content content, TransformerParameters transformerParameters, ResourceLocation resourceLocation, String filePath,
                             QueryParams queryParams, String sourceUrl) {
        for (DimensionAwareWebResourceTransformerFactory transformerFactory : transformersForLocation(resourceLocation)) {
            TransformableResource tr = new TransformableResource(
                    resourceLocation,
                    filePath,
                    ResourceServingHelpers.asDownloadableResource(content)
            );
            content = ResourceServingHelpers.asContent(transformerFactory.makeResourceTransformer(transformerParameters)
                    .transform(tr, queryParams), null, true);
        }
        return content;
    }

    @Override
    public Set<String> getParamKeys() {
        return paramKeys;
    }

    private Iterable<DimensionAwareWebResourceTransformerFactory> transformersForType(final String locationType) {
        return staticTransformersSupplier.get(locationType);
    }

    private Iterable<DimensionAwareWebResourceTransformerFactory> transformersForLocation(final ResourceLocation location) {
        return staticTransformersSupplier.get(location);
    }
}
