package com.atlassian.plugin.webresource.impl.snapshot;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * AMD Module.
 *
 * @since 3.4.4
 */
public class WebModule extends WebResource {
    private final String pluginKey;
    private final String filePath;
    private final LinkedHashMap<String, String> unresolvedDependencies;
    private final LinkedHashSet<String> webResourceDependencies;
    private final String type;
    private final String srcType;
    private final String baseName;
    private final String soyNamespace;

    public WebModule(Snapshot snapshot, String pluginKey, String key, String baseName, List<String> dependencies,
                     LinkedHashMap<String, String> unresolvedDependencies, LinkedHashSet<String> webResourceDependencies, Date updatedAt, String version,
                     TransformerParameters transformerParameters, String filePath, String type, String srcType, String soyNamespace,
                     Map<String, Set<String>> locationResourceTypes) {
        super(snapshot, key, dependencies, updatedAt, version, true, transformerParameters, locationResourceTypes);
        this.baseName = baseName;
        this.pluginKey = pluginKey;
        this.filePath = filePath;
        this.type = type;
        this.srcType = srcType;
        this.soyNamespace = soyNamespace;
        this.unresolvedDependencies = unresolvedDependencies;
        this.webResourceDependencies = webResourceDependencies;
    }

    @Override
    public String getWebResourceKey() {
        // It's intentionally null, we don't want to expose any details to transformer.
        return null;
    }

    @Override
    public String getPluginKey() {
        // It would be better to return an empty string here because it's not used for modules,
        // except for the one use case - the Less transformer needs the plugin key.
        return pluginKey;
    }

    @Override
    public LinkedHashMap<String, Resource> getResources(RequestCache cache) {
        Resource resource = getResource();
        LinkedHashMap<String, Resource> resources = new LinkedHashMap<>();
        resources.put(resource.getName(), resource);
        return resources;
    }

    /**
     * Get resource for this module. The module always has only one resource.
     */
    public Resource getResource() {
        return new ModuleResource(this, pluginKey, filePath, type);
    }

    public LinkedHashMap<String, String> getUnresolvedDependencies() {
        return unresolvedDependencies;
    }

    public LinkedHashMap<String, Jsonable> getData() {
        return new LinkedHashMap<>();
    }

    /**
     * Get base name of the module.
     */
    public String getBaseName() {
        return baseName;
    }

    public String getSrcType() {
        return srcType;
    }

    public String getSoyNamespace() {
        return soyNamespace;
    }
}