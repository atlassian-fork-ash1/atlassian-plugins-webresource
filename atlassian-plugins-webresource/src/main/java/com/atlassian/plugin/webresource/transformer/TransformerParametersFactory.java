package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;

/**
 * It is not used in WRM, but maybe it's used in some other plugins, so marking it as deprecated and leaving for now
 * after some time it should be deleted.
 */
@Deprecated
public class TransformerParametersFactory {
    /**
     * @param webResourceModuleDescriptor web-resource descriptor
     * @return TransformerParameters for the given web-resource and transformer config element
     */
    public static TransformerParameters of(WebResourceModuleDescriptor webResourceModuleDescriptor) {
        return new TransformerParameters(webResourceModuleDescriptor.getPluginKey(), webResourceModuleDescriptor.getKey(), null);
    }
}

