package com.atlassian.plugin.webresource.impl.snapshot;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.sourcemap.SourceMap;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;


/**
 * Virtual resource for Context, needed to add `require` statement for modules.
 *
 * @since v3.4.4
 */
public class ContextResource extends Resource {
    private final Context contextParent;

    public ContextResource(Context parent) {
        super(parent, new ResourceLocation("context-virtual-resource.js", "context-virtual-resource.js", "js", null, null, new HashMap<>()), "js", "js");
        this.contextParent = parent;
    }

    @Override
    public Content getContent() {
        return new ContentImpl(getContentType(), false) {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                List<WebModule> jsModules = contextParent.getJsModulesDependencies();
                if (jsModules.size() > 0) {
                    try {
                        out.write("require([".getBytes());
                        for (int i = 0; i < jsModules.size(); i++) {
                            if (i != 0) {
                                out.write(", ".getBytes());
                            }
                            out.write(("\"" + jsModules.get(i).getKey() + "\"").getBytes());
                        }
                        out.write("], function(){});\n".getBytes());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
                return null;
            }
        };
    }

    @Override
    public boolean isTransformable() {
        return false;
    }
}
