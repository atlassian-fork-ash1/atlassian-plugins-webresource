package com.atlassian.plugin.webresource.prebake;

import com.atlassian.webresource.api.assembler.WebResourceAssembler;

/**
 * @since v3.5.0
 */
public interface PrebakeWebResourceAssembler extends WebResourceAssembler {
}
