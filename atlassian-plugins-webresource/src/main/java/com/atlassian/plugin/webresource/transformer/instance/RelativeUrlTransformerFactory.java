package com.atlassian.plugin.webresource.transformer.instance;

import com.atlassian.plugin.servlet.AbstractFileServerServlet;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.transformer.SearchAndReplaceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.SearchAndReplacer;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import io.atlassian.util.concurrent.LazyReference;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.DimensionAwareTransformerUrlBuilder;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.google.common.base.Function;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.plugin.servlet.AbstractFileServerServlet.PATH_SEPARATOR;

/**
 * Transformer that converts relative urls in CSS resources into absolute urls.
 * <p>
 * This will only transform relative urls. It will not transform urls that are:
 * <ul>
 * <li>absolute to the domain, eg /my-resource</li>
 * <li>absolute to another domain, eg http://blah.com/my-resource or https://blah.com/my-resource</li>
 * <li>data urls</li>
 * </ul>
 *
 * @since v3.1.0
 */
public class RelativeUrlTransformerFactory implements DimensionAwareWebResourceTransformerFactory {
    public static final String RELATIVE_URL_QUERY_KEY = "relative-url";

    private static final Pattern CSS_URL_PATTERN = Pattern.compile("url\\s*\\(\\s*+([\"'])?+(?!/|https?://|data:)");

    private final WebResourceIntegration webResourceIntegration;
    private final WebResourceUrlProvider webResourceUrlProvider;
    private final CdnResourceUrlTransformer cdnResourceUrlTransformer;
    private final boolean usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins;

    public RelativeUrlTransformerFactory(
            WebResourceIntegration webResourceIntegration,
            WebResourceUrlProvider webResourceUrlProvider,
            CdnResourceUrlTransformer cdnResourceUrlTransformer) {
        this.webResourceIntegration = webResourceIntegration;
        this.webResourceUrlProvider = webResourceUrlProvider;
        this.cdnResourceUrlTransformer = cdnResourceUrlTransformer;
        // It is not allowed to change `usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins` dynamically, once set it can't be changed.
        this.usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins = webResourceIntegration.usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins();
    }

    @Override
    public Dimensions computeDimensions() {
        return Dimensions.empty()
                .andExactly(RELATIVE_URL_QUERY_KEY, "true")
                .andAbsent(RELATIVE_URL_QUERY_KEY);
    }

    @Override
    public DimensionAwareTransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return new RelativeUrlTransformerUrlBuilder();
    }

    class RelativeUrlTransformerUrlBuilder implements DimensionAwareTransformerUrlBuilder {
        @Override
        public void addToUrl(UrlBuilder urlBuilder) {
            if (webResourceIntegration.getCDNStrategy() != null &&
                    webResourceIntegration.getCDNStrategy().supportsCdn()) {
                addRelativeUrlQueryKey(urlBuilder);
            }
        }

        @Override
        public void addToUrl(UrlBuilder urlBuilder, Coordinate coord) {
            if (coord.get(RELATIVE_URL_QUERY_KEY) != null) {
                addRelativeUrlQueryKey(urlBuilder);
            }
        }

        private void addRelativeUrlQueryKey(UrlBuilder urlBuilder) {
            // Note that we do not need to know whether the CSS resource being transformed by this resource
            // is tainted or not. It may be tainted, and not be served via CDN; yet the resources inside it
            // (eg icon fonts) are still able to be served via CDN.
            urlBuilder.addToQueryString(RELATIVE_URL_QUERY_KEY, String.valueOf(true));
        }
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters parameters) {
        return new RelativeUrlTransformer(parameters);
    }

    class RelativeUrlTransformer implements UrlReadingWebResourceTransformer {
        private final TransformerParameters parameters;

        RelativeUrlTransformer(final TransformerParameters parameters) {
            this.parameters = parameters;
        }

        @Override
        public DownloadableResource transform(TransformableResource transformableResource, QueryParams params) {
            boolean requestCdnUrl = Boolean.valueOf(params.get(RELATIVE_URL_QUERY_KEY));
            final LazyReference<String> urlPrefix = createUrlPrefixRef(cdnResourceUrlTransformer, requestCdnUrl);
            Function<Matcher, CharSequence> replacer = new Function<Matcher, CharSequence>() {
                public CharSequence apply(Matcher matcher) {
                    return new StringBuilder(matcher.group()).append(urlPrefix.get());
                }
            };
            // Note that this uses the deprecated SearchAndReplacer transformer util - when that class is removed,
            // we will make it protected and local.
            SearchAndReplacer grep = SearchAndReplacer.create(CSS_URL_PATTERN, replacer);
            return new SearchAndReplaceDownloadableResource(transformableResource.nextResource(), grep);
        }

        private LazyReference<String> createUrlPrefixRef(final CdnResourceUrlTransformer cdnResourceUrlTransformer, final boolean requestCdnUrl) {
            return new LazyReference<String>() {
                @Override
                protected String create() {
                    // This transform's only "variable" is the context path, and hence is not even variable since the
                    // context path is already in the url path. Therefore we don't need to contribute anything additional
                    // to the hash - this transform is a "constant" and other transformers / conditions don't apply to data
                    // uri's or images.
                    final String version = Config.getPluginVersionOrInstallTime(webResourceIntegration.getPluginAccessor().getPlugin(parameters.getPluginKey()), usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins);
                    String resourcePrefix = PATH_SEPARATOR + AbstractFileServerServlet.SERVLET_PATH + PATH_SEPARATOR + AbstractFileServerServlet.RESOURCE_URL_PREFIX;
                    String localRelativeUrl = webResourceUrlProvider.getStaticResourcePrefix(version, UrlMode.RELATIVE) + resourcePrefix + PATH_SEPARATOR + parameters.getPluginKey() + ":" + parameters.getModuleKey() + PATH_SEPARATOR;
                    if (requestCdnUrl) {
                        final CDNStrategy cdnStrategy = webResourceIntegration.getCDNStrategy();
                        if (cdnStrategy == null || !cdnStrategy.supportsCdn()) {
                            // This will throw an exception if we have the CDN parameter in the URL, but
                            // webResourceIntegration.getCdnStrategy() returns null. This situation will occur
                            // if the cdn strategy is changed (eg via dark feature) between url-generation time and
                            // resource-fetch time. In this situation, it is correct to throw an exception - the alternative
                            // is to poison the CDN-side cache with a resource that has not been transformed to CDN.
                            throw new CdnStrategyChangedException("CDN strategy has changed between url generation time and resource fetch time");
                        }
                        return cdnResourceUrlTransformer.getResourceCdnPrefix(localRelativeUrl);
                    } else {
                        return localRelativeUrl;
                    }
                }
            };
        }
    }

    public static class CdnStrategyChangedException extends RuntimeException {
        public CdnStrategyChangedException(String message) {
            super(message);
        }
    }
}
