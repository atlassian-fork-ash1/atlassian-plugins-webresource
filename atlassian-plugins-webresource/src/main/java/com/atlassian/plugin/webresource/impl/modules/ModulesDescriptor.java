package com.atlassian.plugin.webresource.impl.modules;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.condition.UrlReadingConditionElementParser;
import org.dom4j.Element;

import java.util.List;

/**
 * Descriptor for AMD modules.
 *
 * @since v 3.5.0
 */
public class ModulesDescriptor extends AbstractModuleDescriptor<Void> {
    private final HostContainer hostContainer;
    private String dir;
    private UrlReadingConditionElementParser conditionElementParser;
    private List<WebResourceTransformation> transformations;
    private DecoratingCondition condition;
    private Element element;

    public ModulesDescriptor(ModuleFactory moduleFactory, HostContainer hostContainer) {
        super(moduleFactory);
        this.hostContainer = hostContainer;
    }

    @Override
    public void init(Plugin plugin, Element element) throws PluginParseException {
        super.init(plugin, element);
        this.element = element;

        dir = element.attributeValue("dir");
        transformations = WebResourceModuleDescriptor.parseTransformations(element);
        this.conditionElementParser = new UrlReadingConditionElementParser(hostContainer);
    }

    @Override
    public void enabled() {
        super.enabled();
        condition = WebResourceModuleDescriptor.parseCondition(conditionElementParser, plugin, element);
    }

    @Override
    public void disabled() {
        super.disabled();
        condition = null;
    }

    @Override
    public Void getModule() {
        throw new UnsupportedOperationException("there's no module for modules descriptor");
    }

    public String getDir() {
        return dir;
    }

    public List<WebResourceTransformation> getTransformations() {
        return transformations;
    }

    public DecoratingCondition getCondition() {
        return condition;
    }
}