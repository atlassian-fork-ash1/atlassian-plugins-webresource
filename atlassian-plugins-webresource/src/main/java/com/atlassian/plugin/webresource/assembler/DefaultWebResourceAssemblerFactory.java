package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerBuilder;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.webresource.api.prebake.Dimensions;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.stream.Stream;

import static com.atlassian.webresource.api.prebake.Dimensions.empty;
import static java.util.stream.Stream.of;

/**
 * Default implementation of WebResourceAssemblerFactory
 *
 * @since v3.0
 */
public class DefaultWebResourceAssemblerFactory implements PrebakeWebResourceAssemblerFactory {
    private final Globals globals;

    public DefaultWebResourceAssemblerFactory(Globals globals) {
        this.globals = globals;
    }

    public DefaultWebResourceAssemblerFactory(PluginResourceLocator pluginResourceLocator) {
        this(pluginResourceLocator.temporaryWayToGetGlobalsDoNotUseIt());
    }

    @Override
    public PrebakeWebResourceAssemblerBuilder create() {
        return new DefaultWebResourceAssemblerBuilder(globals);
    }

    @Override
    public Dimensions computeDimensions() {
        Snapshot snapshot = globals.getSnapshot();
        Dimensions d = empty();
        for (CachedCondition condition : snapshot.conditions()) {
            d = d.product(condition.computeDimensions());
        }
        for (CachedTransformers transformer : snapshot.transformers()) {
            d = d.product(transformer.computeDimensions(globals.getConfig().getTransformerCache()));
        }
        StaticTransformers staticTransformers = globals.getConfig().getStaticTransformers();
        d = d.product(staticTransformers.computeDimensions());
        return d;
    }

    @Override
    public Dimensions computeBundleDimensions(Bundle bundle) {
        if (bundle == null) {
            return empty();
        }
        return computeBundleDimensionsRecursively(of(bundle));
    }

    private Dimensions computeBundleDimensionsRecursively(Stream<Bundle> bundles) {
        return bundles.map(b -> {
            Dimensions d = empty();
            Config config = globals.getConfig();
            CachedCondition condition = b.getCondition();
            if (condition != null) {
                d = d.product(condition.computeDimensions());
            }
            CachedTransformers transformers = b.getTransformers();
            if (transformers != null) {
                d = d.product(transformers.computeDimensions(config.getTransformerCache()));
            }
            List<String> dependencies = b.getDependencies();
            if (CollectionUtils.isNotEmpty(dependencies)) {
                d = d.product(computeBundleDimensionsRecursively(globals.getSnapshot().toBundles(dependencies).stream()));
            }
            d = d.product(config.getStaticTransformers().computeBundleDimensions(b));
            return d;
        }).reduce(Dimensions::product).orElse(empty());
    }

    @Override
    public String computeGlobalStateHash() {
        return globals.getConfig().computeGlobalStateHash();
    }

    /**
     * @deprecated since 3.3.2
     */
    @Override
    @Deprecated
    public void clearCache() {
        globals.triggerStateChange();
    }
}
