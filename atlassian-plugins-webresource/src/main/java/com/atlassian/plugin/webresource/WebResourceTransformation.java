package com.atlassian.plugin.webresource;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.transformer.ContentTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.UrlReadingContentTransformer;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.atlassian.webresource.spi.TransformationDto;
import com.atlassian.webresource.spi.TransformerDto;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.impl.helpers.Helpers.asContent;
import static com.atlassian.plugin.webresource.impl.helpers.Helpers.asDownloadableResource;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

/**
 * Represents a set of transformer invocations for a specific web resource set and extension.  Transformers are retrieved
 * from the plugin system on request, not plugin initialisation, since plugin start order is indeterminate.
 *
 * @since 2.5.0
 */
public class WebResourceTransformation {
    private final String extension;
    private final String type;
    private final Map<String, Element> transformerElements;
    private final Iterable<String> transformerKeys;
    private Logger log = LoggerFactory.getLogger(WebResourceTransformation.class);

    public WebResourceTransformation(TransformationDto transformationDto) {
        requireNonNull(transformationDto.extension);

        type = transformationDto.extension;
        extension = "." + type;

        List<String> keys = new ArrayList<>();
        for (TransformerDto transformerDto : transformationDto.transformers) {
            keys.add(transformerDto.key);
        }
        transformerKeys = keys;
        // It's ok to set it as null, because it's used only for legacy transformers and we don't
        // allow legacy transformers here.
        transformerElements = null;
    }

    public WebResourceTransformation(Element element) {
        checkArgument(element.attribute("extension") != null, "extension");

        this.type = element.attributeValue("extension");
        this.extension = "." + type;

        LinkedHashMap<String, Element> transformers = new LinkedHashMap<>();
        for (Element transformElement : (List<Element>) element.elements("transformer")) {
            transformers.put(transformElement.attributeValue("key"), transformElement);
        }
        transformerElements = Collections.unmodifiableMap(transformers);
        transformerKeys = transformerElements.keySet();
    }

    // TODO there are two matches - one that accepts ResourceLocation and another that accepts the type (as String).
    // And, they not exactly the same, because there are cases when transformers matched against
    //     "some-name.public.js".matches("public.js")
    // so, how it works isn't very obvious, maybe we should deprecate or change it.
    // See https://ecosystem.atlassian.net/browse/PLUGWEB-195

    public boolean matches(ResourceLocation location) {
        String loc = location.getLocation();
        if (loc == null || "".equals(loc.trim())) {
            loc = location.getName();
        }
        return loc.endsWith(extension);
    }

    public boolean matches(String locationType) {
        return locationType.equals(type);
    }

    public void addTransformParameters(TransformerCache transformerCache,
                                       TransformerParameters transformerParameters,
                                       UrlBuilder urlBuilder, UrlBuildingStrategy urlBuildingStrategy) {
        for (String key : transformerKeys) {
            Object descriptorAsObject = transformerCache.getDescriptor(key);
            if ((descriptorAsObject != null)) {
                if (descriptorAsObject instanceof UrlReadingWebResourceTransformerModuleDescriptor) {
                    UrlReadingWebResourceTransformerModuleDescriptor descriptor = (UrlReadingWebResourceTransformerModuleDescriptor) descriptorAsObject;
                    urlBuildingStrategy.addToUrl(descriptor.getModule().makeUrlBuilder(transformerParameters), urlBuilder);
                } else if (descriptorAsObject instanceof ContentTransformerModuleDescriptor) {
                    ContentTransformerModuleDescriptor descriptor = (ContentTransformerModuleDescriptor) descriptorAsObject;
                    urlBuildingStrategy.addToUrl(descriptor.getModule().makeUrlBuilder(transformerParameters), urlBuilder);
                } else if (descriptorAsObject instanceof WebResourceTransformerModuleDescriptor) {
                    // Do nothing.
                } else {
                    throw new RuntimeException("invalid usage, transformer descriptor expected but got " + descriptorAsObject);
                }
            }
        }
    }

    public boolean containsOnlyPureUrlReadingTransformers(TransformerCache transformerCache) {
        for (String key : transformerKeys) {
            Object descriptorAsObject = transformerCache.getDescriptor(key);
            if ((descriptorAsObject != null) && (descriptorAsObject instanceof WebResourceTransformerModuleDescriptor)) {
                return false;
            }
        }
        return true;
    }

    public Iterable<WebResourceTransformerModuleDescriptor> getDeprecatedTransformers(TransformerCache transformerCache) {
        List<WebResourceTransformerModuleDescriptor> deprecatedTransformers = new LinkedList<>();
        for (String key : transformerKeys) {
            Object descriptorAsObject = transformerCache.getDescriptor(key);
            if ((descriptorAsObject != null) && (descriptorAsObject instanceof WebResourceTransformerModuleDescriptor)) {
                deprecatedTransformers.add((WebResourceTransformerModuleDescriptor) descriptorAsObject);
            }
        }
        return deprecatedTransformers;
    }

    public Content transform(final CdnResourceUrlTransformer cdnResourceUrlTransformer, final TransformerCache transformerCache,
                             Resource resource, final Content content, ResourceLocation resourceLocation, final String filePath,
                             final QueryParams params, final String sourceUrl) {
        Content lastContent = content;

        for (String transformerKey : transformerKeys) {
            Object descriptorAsObject = transformerCache.getDescriptor(transformerKey);
            if (descriptorAsObject != null) {
                if (descriptorAsObject instanceof ContentTransformerModuleDescriptor) {
                    ContentTransformerModuleDescriptor descriptor = (ContentTransformerModuleDescriptor) descriptorAsObject;
                    UrlReadingContentTransformer transformer = descriptor.getModule().makeResourceTransformer(resource.getParent().getTransformerParameters());
                    lastContent = transformer.transform(cdnResourceUrlTransformer, lastContent, resourceLocation, params, sourceUrl);
                } else if (descriptorAsObject instanceof UrlReadingWebResourceTransformerModuleDescriptor) {
                    UrlReadingWebResourceTransformerModuleDescriptor descriptor = (UrlReadingWebResourceTransformerModuleDescriptor) descriptorAsObject;
                    TransformableResource transformableResource = new TransformableResource(resourceLocation, filePath, asDownloadableResource(lastContent));
                    UrlReadingWebResourceTransformer transformer = descriptor.getModule().makeResourceTransformer(resource.getParent().getTransformerParameters());
                    lastContent = asContent(transformer.transform(transformableResource, params), null, true);
                } else if (descriptorAsObject instanceof WebResourceTransformerModuleDescriptor) {
                    if (transformerElements == null) {
                        throw new RuntimeException("can't apply legacy transformer '" + transformerKey + "' to '" + resource.getKey() + "', legacy transformers are not allowed here!");
                    }
                    WebResourceTransformerModuleDescriptor descriptor = (WebResourceTransformerModuleDescriptor) descriptorAsObject;
                    lastContent = asContent(
                            descriptor.getModule().transform(transformerElements.get(transformerKey), resourceLocation,
                                    filePath, asDownloadableResource(lastContent)), null, true
                    );
                } else {
                    throw new RuntimeException("invalid usage, transformer descriptor expected but got " + descriptorAsObject);
                }
            } else {
                log.warn("Web resource transformer {} not found for resource {}, skipping", transformerKey,
                        resourceLocation.getName());
            }
        }
        return lastContent;
    }

    public String toString() {
        return getClass().getSimpleName() + " with " + transformerKeys.toString() + " transformers";
    }

    public Dimensions computeDimensions(TransformerCache transformerCache) {
        Dimensions d = Dimensions.empty();
        for (String key : transformerKeys) {
            Object descriptorAsObject = transformerCache.getDescriptor(key);
            if (descriptorAsObject instanceof UrlReadingWebResourceTransformerModuleDescriptor) {
                UrlReadingWebResourceTransformerModuleDescriptor descriptor = (UrlReadingWebResourceTransformerModuleDescriptor) descriptorAsObject;
                WebResourceTransformerFactory module = descriptor.getModule();
                if (module instanceof DimensionAwareWebResourceTransformerFactory) {
                    d = d.product(((DimensionAwareWebResourceTransformerFactory) module).computeDimensions());
                }
            }
        }
        return d;
    }
}