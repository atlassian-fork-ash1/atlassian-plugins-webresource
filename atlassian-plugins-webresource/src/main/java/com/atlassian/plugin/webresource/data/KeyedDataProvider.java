package com.atlassian.plugin.webresource.data;

import org.dom4j.Element;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Container for data providers
 * @since v3.0
 */
class KeyedDataProvider
{
    private final String key;
    private final String className;

    KeyedDataProvider(Element e)
    {
        checkArgument(e.attribute("key") != null, "key");
        checkArgument(e.attribute("class") != null, "class");

        this.key = e.attributeValue("key");
        this.className = e.attributeValue("class");
    }

    String getKey()
    {
        return key;
    }

    String getClassName()
    {
        return className;
    }
}
