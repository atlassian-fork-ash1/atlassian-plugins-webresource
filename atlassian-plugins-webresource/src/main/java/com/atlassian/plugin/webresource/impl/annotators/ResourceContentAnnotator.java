package com.atlassian.plugin.webresource.impl.annotators;

import com.atlassian.plugin.webresource.impl.snapshot.Resource;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Implemented by classes that want to annotate the write of a plugin resource.
 */
public abstract class ResourceContentAnnotator
{
    /**
     * Called before the resource content is written in batch.
     *
     * @return amount of lines the annotator adds to the source, used for source map generation.
     * @throws IOException if there is a problem writing the annotation.
     */
    public int beforeResourceInBatch(LinkedHashSet<String> requiredResources, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException
    {
        return 0;
    }
    
    /**
     * Called after the resource content is written in batch.
     *
     * @throws IOException if there is a problem writing the annotation.
     */
    public void afterResourceInBatch(LinkedHashSet<String> requiredResources, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException
    {
    }

    /**
     * Called before all content is written in batch.
     * @return amount of lines the annotator adds to the source, used for source map generation.
     * @throws IOException
     */
    public int beforeAllResourcesInBatch(LinkedHashSet<String> requiredResources, String url, final Map<String, String> params, OutputStream stream) throws IOException
    {
        return 0;
    }

    /**
     * Called after all content is written in batch.
     * @throws IOException
     */
    public void afterAllResourcesInBatch(LinkedHashSet<String> requiredResources, String url, final Map<String, String> params, OutputStream stream) throws IOException
    {
    }

    /**
     * Called before the resource content is written.
     */
    public int beforeResource(LinkedHashSet<String> requiredResources, String url, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException
    {
        return 0;
    }

    /**
     * Called after the resource content is written.
     */
    public void afterResource(LinkedHashSet<String> requiredResources, String url, Resource resource, final Map<String, String> params, OutputStream stream) throws IOException
    {
    }

    /**
     * Produce a hash for this instance. Should be stable across jvms.
     */
    @Override
    public abstract int hashCode();
}
