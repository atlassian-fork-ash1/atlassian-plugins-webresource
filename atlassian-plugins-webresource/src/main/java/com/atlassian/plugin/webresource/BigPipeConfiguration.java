package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.util.TimeSpan;

import java.util.concurrent.TimeUnit;

/**
 */
public interface BigPipeConfiguration {
    /**
     * The default bigpipe deadline, see
     * {@link com.atlassian.webresource.api.assembler.WebResourceAssemblerBuilder#asyncDataDeadline(long, TimeUnit)}.
     */
    TimeSpan getDefaultBigPipeDeadline();

    /**
     * Default value: false
     * True disables deadline for bigpipe.
     */
    boolean getBigPipeDeadlineDisabled();
}
