package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.Util;
import com.atlassian.webresource.api.prebake.Dimensions;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;


public class OffsetStaticTransformersWithSourceMap implements StaticTransformers {

    @Override
    public Dimensions computeDimensions() {
        return Dimensions.empty();
    }

    @Override
    public Dimensions computeBundleDimensions(Bundle bundle) {
        return computeDimensions();
    }

    @Override
    public void addToUrl(String locationType, TransformerParameters transformerParameters, UrlBuilder urlBuilder, UrlBuildingStrategy urlBuildingStrategy) {
    }

    @Override
    public Content transform(Content content, TransformerParameters transformerParameters, ResourceLocation resourceLocation, String filePath,
                             QueryParams queryParams, String sourceUrl) {
        return new ContentImpl(content.getContentType(), true) {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled) {
                SourceMap sourceMap;
                try {
                    out.write(("offset-static-transformer\n").getBytes());
                    sourceMap = content.writeTo(out, isSourceMapEnabled);
                    out.write(("\noffset-static-transformer").getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                if (isSourceMapEnabled && (sourceMap != null)) {
                    sourceMap = Util.offset(sourceMap, 1);
                }
                return sourceMap;
            }
        };
    }

    @Override
    public Set<String> getParamKeys() {
        return new HashSet<>();
    }
}