package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.helpers.Helpers;
import junit.framework.TestCase;

import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;

// Note, the previous version of comparator used lexicographical order of resource url to sort resources. But, as soon
// as the urls for sub batches are the same except the querystring parts, it seems that using querystring only
// should be sufficient to maintain the same order.
public class TestResourceBatchComparator extends TestCase {
    static Helpers.ParamsComparator PARAMS_COMPARATOR = new Helpers.ParamsComparator();

    private static int compare(Map<String, String> a, Map<String, String> b) {
        return PARAMS_COMPARATOR.compare(a, b);
    }

    public void testResourceBatchComparatorFirstPriorityToUnconditionalResources() {
        assertTrue(compare(buildMap(Config.MEDIA_PARAM_NAME, "print"), buildMap()) > 0);
    }

    public void testResourceBatchComparatorFirstPriorityToUnconditionalResourcesOnly() {
        assertTrue(compare(
                buildMap("content-type", "application/xml", Config.CACHE_PARAM_NAME, "no-cache"),
                buildMap(Config.MEDIA_PARAM_NAME, "print")
        ) < 0);
    }

    public void testResourceBatchComparatorFewerConditionsComesFirst() {
        assertTrue(compare(
                buildMap(Config.MEDIA_PARAM_NAME, "blah", Config.CACHE_PARAM_NAME, "true"),
                buildMap(Config.MEDIA_PARAM_NAME, "print")
        ) > 0);
    }

    @Deprecated
    public void testResourceBatchComparatorMediaConditionComesBeforeIeOnly() {
        assertTrue(compare(buildMap(Config.MEDIA_PARAM_NAME, "print"), buildMap(Config.IEONLY_PARAM_NAME, "true")) < 0);
    }

    public void testResourceBatchComparatorOrdersByUrlAmongstUnconditionals() {
        assertTrue(compare(buildMap(Config.MEDIA_PARAM_NAME, "a"), buildMap(Config.MEDIA_PARAM_NAME, "b")) == 0);
    }

    @Deprecated
    public void testResourceBatchComparatorOrdersByUrlAmongstSameSetOfConditions() {
        assertTrue(compare(
                buildMap(Config.MEDIA_PARAM_NAME, "blah", Config.CONDITIONAL_COMMENT_PARAM_NAME, "lte+IE+9"),
                buildMap(Config.MEDIA_PARAM_NAME, "blah", Config.CONDITIONAL_COMMENT_PARAM_NAME, "lte+IE+8")
        ) == 0);
    }

    @Deprecated
    public void testResourceBatchComparatorNumberOfConditionsTrumpsOrderByUrl() {
        assertTrue(compare(
                buildMap(Config.MEDIA_PARAM_NAME, "blah"),
                buildMap(Config.MEDIA_PARAM_NAME, "blah", Config.CONDITIONAL_COMMENT_PARAM_NAME, "lte+IE+8")
        ) < 0);
    }

    public void testResourceBatchComparatorCacheParamVsLocale() {
        assertTrue(compare(
                buildMap(Config.CACHE_PARAM_NAME, "true", "locale", "en"),
                buildMap("locale", "en")
        ) > 0);
    }

    public void testResourceBatchComparatorCacheParam() {
        assertTrue(compare(
                buildMap(Config.CACHE_PARAM_NAME, "true"),
                buildMap()
        ) > 0);
    }

    @Deprecated
    public void testResourceBatchComparatorCacheParamBeforeConditions() {
        assertTrue(compare(
                buildMap(Config.CACHE_PARAM_NAME, "true"),
                buildMap(Config.CONDITIONAL_COMMENT_PARAM_NAME, "lte+IE+8")
        ) < 0);
    }
}
