package com.atlassian.plugin.webresource.prebake;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.SearchAndReplaceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.SearchAndReplacer;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.google.common.base.Function;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 */
public class MockI18nTransformer implements WebResourceTransformerFactory {
    @Override
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return new I18nTransformerUrlBuilder();
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters parameters) {
        return new I18nUrlReadingWebResourceTransformer();
    }

    public static class I18nTransformerUrlBuilder implements TransformerUrlBuilder {
        @Override
        public void addToUrl(UrlBuilder urlBuilder) {
            urlBuilder.addToQueryString("locale", "en-GB");
        }
    }

    public static class I18nUrlReadingWebResourceTransformer implements UrlReadingWebResourceTransformer {
        @Override
        public DownloadableResource transform(TransformableResource transformableResource, QueryParams params) {
            return transformImpl(transformableResource, params);
        }
    }

    static DownloadableResource transformImpl(TransformableResource transformableResource, QueryParams params) {
        String locale = params.get("locale");
        Function<Matcher, CharSequence> replacer = matcher -> locale.toUpperCase();
        SearchAndReplacer grep = SearchAndReplacer.create(Pattern.compile("REPLACE"), replacer);
        return new SearchAndReplaceDownloadableResource(transformableResource.nextResource(), grep);
    }
}
