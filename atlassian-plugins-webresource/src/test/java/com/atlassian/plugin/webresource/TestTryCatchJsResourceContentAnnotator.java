package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.annotators.ResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.annotators.TryCatchJsResourceContentAnnotator;
import junit.framework.TestCase;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.ByteArrayOutputStream;

public class TestTryCatchJsResourceContentAnnotator extends TestCase {
    public void testGoodSyntax() throws Exception {
        ResourceContentAnnotator annotator = new TryCatchJsResourceContentAnnotator();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        annotator.beforeResourceInBatch(null, null, null, stream);
        stream.write("6 * 2;\n".getBytes());
        annotator.afterResourceInBatch(null, null, null, stream);

        String tryCatchBlock = new String(stream.toByteArray());
        assertTrue(tryCatchBlock.startsWith("try"));

        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine jsEngine = mgr.getEngineByName("JavaScript");
        try {
            Object eval = jsEngine.eval(tryCatchBlock);
            assertEquals(12.0, ((Number) eval).doubleValue());
        } catch (ScriptException ex) {
            fail("Exception running the created javascript: " + ex);
        }
    }
}
