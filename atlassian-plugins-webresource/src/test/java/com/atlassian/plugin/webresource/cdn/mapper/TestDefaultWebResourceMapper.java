package com.atlassian.plugin.webresource.cdn.mapper;

import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.prebake.PrebakeConfig;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultWebResourceMapper {
    private MappingParser mappingParser;
    private DefaultWebResourceMapper wrm;
    private String wrmFilePath;

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    @Mock
    private WebResourceIntegration webResourceIntegration;

    @Before
    public void setUp() throws IOException, MappingParserException {
        mappingParser = spy(new MappingParser());
        wrmFilePath = createMapping("{ \"/file1\": [\"file1\"], \"/file2\": [\"file2a\", \"file2b\"] }");
        when(webResourceIntegration.isCtCdnMappingEnabled()).thenReturn(true);
        wrm = new DefaultWebResourceMapper(webResourceIntegration, mappingParser, PrebakeConfig.forPattern(wrmFilePath), "ha$s", "//cdn/", "");
    }

    @Test
    public void testSingleMapping() throws IOException, MappingParserException {
        assertThat(wrm.map("/file1"), hasItem("//cdn/file1"));
        assertThat(wrm.map("/file1"), hasSize(1));
    }

    @Test
    public void testNonExistentUrl() throws IOException, MappingParserException {
        assertThat(wrm.map("/file.not.exits"), empty());
    }

    @Test
    public void testMultipleMapping() throws IOException, MappingParserException {
        assertThat(wrm.map("/file2"), hasItems("//cdn/file2a", "//cdn/file2b"));
        assertThat(wrm.map("/file2"), hasSize(2));
    }

    @Test
    public void testMappingSingle() throws IOException, MappingParserException {
        assertThat(wrm.mapSingle("/file2").get(), is("//cdn/file2a"));
    }

    @Test
    public void testMappingSingleDefault() throws IOException, MappingParserException {
        assertThat(wrm.mapSingle("/file.not.exists").isPresent(), is(false));
    }

    @Test
    public void testLoadOnce() throws IOException, MappingParserException {
        wrm.map("/file1");
        wrm.map("/file1");
        verify(mappingParser, times(1)).parse(any());
    }

    @Test(expected = MappingParserException.class)
    public void testMappingParserException() throws IOException, MappingParserException {
        String wrmFilePath = createMapping("{ \"mappings\": { \"/file1\": [\"file1\"], \"/file2\": [\"file2a\", \"file2b\"] }");
        new DefaultWebResourceMapper(webResourceIntegration, new MappingParser(), PrebakeConfig.forPattern(wrmFilePath), "ha$s", "//cdn/", "");
    }

    @Test(expected = IOException.class)
    public void testMappingsFileNotExists() throws IOException, MappingParserException {
        new DefaultWebResourceMapper(webResourceIntegration, new MappingParser(), PrebakeConfig.forPattern("/nothing/void"), "ha$s", "//cdn/", "");
    }

    @Test(expected = RuntimeException.class)
    public void testNullPattern() throws IOException, MappingParserException {
        new DefaultWebResourceMapper(webResourceIntegration, new MappingParser(), PrebakeConfig.forPattern("/nothing/void"), null, "//cdn/", "");
    }

    @Test
    public void testMappingWithContextPath() throws IOException, MappingParserException {
        wrmFilePath = createMapping("{ \"/file1\": [\"file1\"], \"/file2\": [\"file2a\", \"file2b\"] }");
        wrm = new DefaultWebResourceMapper(webResourceIntegration, mappingParser, PrebakeConfig.forPattern(wrmFilePath), "ha$s", "//cdn/", "/context");
        assertThat(wrm.map("/context/file1"), hasItem("//cdn/file1"));
        assertThat(wrm.map("/context/file1"), hasSize(1));
    }

    @Test
    public void testMappingWithNullContextPath() throws IOException, MappingParserException {
        wrmFilePath = createMapping("{ \"/file1\": [\"file1\"] }");
        wrm = new DefaultWebResourceMapper(webResourceIntegration, mappingParser, PrebakeConfig.forPattern(wrmFilePath), "ha$s", "//cdn/", null);
        assertThat(wrm.map("/file1"), hasItem("//cdn/file1"));
    }

    @Test
    public void testMappingUrlNotContainingCurrentContextPath() throws IOException, MappingParserException {
        wrmFilePath = createMapping("{ \"/other/file1\": [\"file1\"] }");
        wrm = new DefaultWebResourceMapper(webResourceIntegration, mappingParser, PrebakeConfig.forPattern(wrmFilePath), "ha$s", "//cdn/", "/context");
        assertThat(wrm.map("/other/file1"), hasItem("//cdn/file1"));
    }

    @Test
    public void testSlashConsistencyInMapping() throws IOException, MappingParserException {
        wrmFilePath = createMapping("{ \"/file1\": [\"file1\"], \"/file2\": [\"/file2\"] }");
        wrm = new DefaultWebResourceMapper(webResourceIntegration, mappingParser, PrebakeConfig.forPattern(wrmFilePath), "ha$s", "//cdn", "/context");
        assertThat(wrm.map("/context/file1"), hasItem("//cdn/file1"));
        assertThat(wrm.map("/context/file2"), hasItem("//cdn/file2"));
        wrm = new DefaultWebResourceMapper(webResourceIntegration, mappingParser, PrebakeConfig.forPattern(wrmFilePath), "ha$s", "//cdn/", "/context");
        assertThat(wrm.map("/context/file1"), hasItem("//cdn/file1"));
        assertThat(wrm.map("/context/file2"), hasItem("//cdn/file2"));
    }

    private String createMapping(final String mapping) throws IOException {
        Path mappingFile = tmpFolder.newFile().toPath();
        try (OutputStream out = new FileOutputStream(mappingFile.toFile())) {
            out.write(mapping.getBytes(StandardCharsets.UTF_8));
        }
        return mappingFile.toFile().getAbsolutePath();
    }
}
