package com.atlassian.plugin.webresource.integration.stub.dsl;

import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.impl.support.Tuple;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.impl.config.Config.parseNameWithLoader;

public class ModuleData {
    public static class DependencyData {
        public final String name;
        public final String resolvedName;
        public final String loader;

        public DependencyData(String name, String resolvedName) {
            this.name = name;
            if (resolvedName.contains("!")) {
                throw new RuntimeException("resolved name can't contain loader " + resolvedName);
            }
            this.resolvedName = resolvedName;
            loader = parseNameWithLoader(name).getFirst();
        }
    }

    public final String modulesDescriptorDir;
    public final String name;
    public final String filePath;
    public final String content;
    public final String type;
    public final String srcType;
    public final String baseName;
    public Map<String, DependencyData> dependencies = new LinkedHashMap<>();
    public List<String> contexts = new ArrayList<>();
    public List<String> soyTemplates = new ArrayList<>();
    public List<String> soyDependencies = new ArrayList<>();
    public String soyNamespace;

    public ModuleData(String modulesDescriptorFilePath, String name, String filePath, String content) {
        this.modulesDescriptorDir = modulesDescriptorFilePath;
        this.name = name;
        this.filePath = filePath;
        this.baseName = baseName(name);
        String type = ResourceUtils.getType(name);
        // In terms of WRM the LESS resources have type of CSS.
        switch (type) {
            case "less":
                this.type = "css";
                this.srcType = "less";
                break;
            case "soy":
                this.type = "js";
                this.srcType = "soy";
                break;
            default:
                this.type = type;
                this.srcType = type;
        }
        this.content = content;
    }

    private static String baseName(String name) {
        return name.replaceAll("\\.[^\\.\\d]+$", "");
    }

    public String toAtlassianModulesXml() {
        StringBuilder buff = new StringBuilder();
        buff.append("<module")
                .append(" modules-descriptor-dir=\"").append(modulesDescriptorDir).append("\"")
                .append(" name=\"").append(name).append("\"")
                .append(" base-name=\"").append(baseName).append("\"")
                .append(" file-path=\"").append(filePath).append("\"")
                .append(" type=\"").append(type).append("\"")
                .append(" src-type=\"").append(srcType).append("\"");
        if (soyNamespace != null) {
            buff.append(" soy-namespace=\"").append(soyNamespace).append("\"");
        }
        buff.append(">\n");
        for (DependencyData dependency : dependencies.values()) {
            buff.append("<dependency")
                    .append(" resolved-name=\"").append(dependency.resolvedName).append("\"");
            if (dependency.loader != null) {
                buff.append(" loader=\"").append(dependency.loader).append("\"");
            }
            buff.append(">").append(dependency.name).append("</dependency>\n");
        }
        for (String context : contexts) {
            buff.append("<context>").append(context).append("</context>\n");
        }
        for (String soyTemplate : soyTemplates) {
            buff.append("<soy-template>").append(soyTemplate).append("</soy-template>\n");
        }
        for (String soyDependency : soyDependencies) {
            buff.append("<soy-dependency>").append(soyDependency).append("</soy-dependency>\n");
        }
        buff.append("</module>");
        return buff.toString();
    }
}