package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;

import static java.util.Collections.emptyMap;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class TestDefaultStaticTransformersSupplier {
    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;

    private final CdnResourceUrlTransformer cdnResourceUrlTransformer = (url) -> url;
    private StaticTransformersSupplier staticTransformersSupplier;

    @Before
    public void setup() {
        final WebResourceIntegration webResourceIntegration = mock(WebResourceIntegration.class);
        doReturn(false).when(webResourceIntegration).usePluginInstallTimeInsteadOfTheVersionForSnapshotPlugins();
        staticTransformersSupplier =
                new DefaultStaticTransformersSupplier(
                        webResourceIntegration,
                        webResourceUrlProvider,
                        cdnResourceUrlTransformer
                );
    }

    @Test
    public void testCssType() {
        ArrayList<DimensionAwareWebResourceTransformerFactory> transformers = Lists.newArrayList(staticTransformersSupplier.get("css"));
        assertEquals(1, transformers.size());
        assertTrue(transformers.get(0) instanceof RelativeUrlTransformerFactory);
    }

    @Test
    public void testCssLocation() {
        ResourceLocation resourceLocation = createResourceLocation("my/path/blah.css", "blah.css");
        ArrayList<DimensionAwareWebResourceTransformerFactory> transformers = Lists.newArrayList(staticTransformersSupplier.get(resourceLocation));
        assertEquals(1, transformers.size());
        assertTrue(transformers.get(0) instanceof RelativeUrlTransformerFactory);
    }

    @Test
    public void testLessLocation() {
        ResourceLocation resourceLocation = createResourceLocation("my/path/blah.less", "blah.css");
        ArrayList<DimensionAwareWebResourceTransformerFactory> transformers = Lists.newArrayList(staticTransformersSupplier.get(resourceLocation));
        assertEquals(1, transformers.size());
        assertTrue(transformers.get(0) instanceof RelativeUrlTransformerFactory);
    }

    @Test
    public void testComputeDimensions() {
        Dimensions dims = staticTransformersSupplier.computeDimensions();
        Dimensions expected = Dimensions.empty()
                .andExactly(RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY, String.valueOf(true))
                .andAbsent(RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY);
        assertThat(dims, equalTo(expected));
    }

    private static ResourceLocation createResourceLocation(String location, String name) {
        return new ResourceLocation(location, name, null, null, null, emptyMap());
    }
}
