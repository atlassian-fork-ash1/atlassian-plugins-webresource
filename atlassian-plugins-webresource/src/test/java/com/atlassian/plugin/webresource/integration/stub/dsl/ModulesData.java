package com.atlassian.plugin.webresource.integration.stub.dsl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ModulesData {
    public final String dir;
    public List<ModuleData> modules = new ArrayList<>();
    public Map<String, List<String>> contexts = new HashMap<>();
    public List<Class> conditions = new ArrayList<>();
    public Map<String, List<String>> transformations = new LinkedHashMap<>();

    public ModulesData(String dir) {
        this.dir = dir;
    }

    public String toDescriptorXml() {
        StringBuilder buff = new StringBuilder();
        buff.append("<web-modules key=\"" + dir + "\" dir=\"").append(dir).append("\">\n");
        for (Map.Entry<String, List<String>> entry : contexts.entrySet()) {
            for (String context : entry.getValue()) {
                buff.append("<context name=\"").append(context).append("\">").append(entry.getKey()).append("</context>\n");
            }
        }
        for (Map.Entry<String, List<String>> entry : transformations.entrySet()) {
            buff.append("<transformation extension=\"").append(entry.getKey()).append("\">\n");
            for (String key : entry.getValue()) {
                buff.append("<transformer key=\"").append(key).append("\"/>\n");
            }
            buff.append("</transformation>\n");
        }
        for (Class klass : conditions) {
            buff.append("<condition class=\"").append(klass.getName()).append("\"/>\n");
        }
        buff.append("</web-modules>");
        return buff.toString();
    }
}