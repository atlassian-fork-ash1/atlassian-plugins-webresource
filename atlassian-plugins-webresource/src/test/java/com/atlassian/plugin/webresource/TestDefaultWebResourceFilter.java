package com.atlassian.plugin.webresource;

import junit.framework.TestCase;

public class TestDefaultWebResourceFilter extends TestCase {
    public void testMatches() {
        assertTrue(new DefaultWebResourceFilter(true).matches("foo.css"));
        assertTrue(new DefaultWebResourceFilter(true).matches("foo.js"));
        assertFalse(new DefaultWebResourceFilter(true).matches("foo.html"));
    }
}
