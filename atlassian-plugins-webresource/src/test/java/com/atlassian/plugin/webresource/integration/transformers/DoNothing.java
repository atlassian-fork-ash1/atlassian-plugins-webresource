package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;

public class DoNothing implements WebResourceTransformerFactory {
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters) {
        return urlBuilder -> {};
    }

    public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters) {
        return (resource, params) -> resource.nextResource();
    }
}