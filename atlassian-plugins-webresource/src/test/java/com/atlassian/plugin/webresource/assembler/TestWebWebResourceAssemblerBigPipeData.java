package com.atlassian.plugin.webresource.assembler;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.DefaultBigPipeConfiguration;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.plugin.webresource.TestUtils.jsonableOf;
import static com.atlassian.plugin.webresource.data.DataTestFixture.assertDataTag;
import static com.atlassian.plugin.webresource.data.DataTestFixture.assertEmptyDataTag;
import static com.google.common.collect.Iterables.isEmpty;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 */
public class TestWebWebResourceAssemblerBigPipeData {
    private AssemblerTestFixture f;

    @Before
    public void setup() {
        f = new AssemblerTestFixture();
    }

    @Test
    public void testImmediatelyAvailableFutureOnDrain() {
        WebResourceAssembler assembler = f.create();
        CompletableFuture<Jsonable> success = CompletableFuture.completedFuture(jsonableOf("man"));
        CompletableFuture<Jsonable> failure = new CompletableFuture<>();
        failure.completeExceptionally(new RuntimeException("daddy"));

        assembler.data().requireData("duff", success);
        assembler.data().requireData("puff", failure);
        WebResourceSet resources = assembler.assembled().drainIncludedResources();
        assertDataTag(resources,
                "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n" +
                        "WRM._unparsedErrors[\"puff\"]=\"\";\n"
        );
        assertTrue(resources.isComplete());
    }

    @Test
    public void testResourceSetNotCompleteUntilFuturesComplete() {
        WebResourceAssembler assembler = f.create();

        CompletableFuture<Jsonable> success = new CompletableFuture<>();
        CompletableFuture<Jsonable> failure = new CompletableFuture<>();
        assembler.data().requireData("duff", success);
        assembler.data().requireData("puff", failure);

        WebResourceSet resources = assembler.assembled().drainIncludedResources();
        assertTrue(isEmpty(resources.getResources()));
        assertFalse(resources.isComplete());

        success.complete(jsonableOf("man"));
        failure.completeExceptionally(new RuntimeException("daddy"));

        resources = assembler.assembled().drainIncludedResources();
        assertDataTag(resources,
                "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n" +
                        "WRM._unparsedErrors[\"puff\"]=\"\";\n"
        );
        assertTrue(resources.isComplete());
    }

    @Test
    public void testFailuresOnExceededDeadline() throws Exception {

        int deadlineSeconds = 1;
        WebResourceAssembler assembler = f.factory().create().asyncDataDeadline(deadlineSeconds, TimeUnit.SECONDS).build();

        CompletableFuture<Jsonable> neverCompletes = new CompletableFuture<>();
        assembler.data().requireData("duff", neverCompletes);

        WebResourceSet resources = assembler.assembled().drainIncludedResources();
        assertTrue(isEmpty(resources.getResources()));
        assertFalse(resources.isComplete());

        TimeUnit.SECONDS.sleep(deadlineSeconds + 1);

        resources = assembler.assembled().drainIncludedResources();
        assertDataTag(resources,
                "WRM._unparsedErrors[\"duff\"]=\"\";\n"
        );
        assertTrue(resources.isComplete());
    }

    //RAID-551
    @Test(timeout = 60000)
    public void testNoDeadlineWorksJustFine() throws Exception {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        Thread currentThread = Thread.currentThread();

        Properties props = System.getProperties();
        props.setProperty(DefaultBigPipeConfiguration.BIGPIPE_DEADLINE_DISABLED, "true");

        int deadlineMilliseconds = 100;
        WebResourceAssembler assembler = f.factory().create().asyncDataDeadline(deadlineMilliseconds, TimeUnit.MILLISECONDS).build();

        CompletableFuture<Jsonable> willCompleteSomeday = new CompletableFuture<>();
        assembler.data().requireData("duff", willCompleteSomeday);

        AtomicBoolean interrupted = new AtomicBoolean(false);
        executor.schedule(() -> {
            interrupted.set(true);
            currentThread.interrupt();
        }, 200, TimeUnit.MILLISECONDS);

        WebResourceSet resources = assembler.assembled().pollIncludedResources();

        assertTrue(interrupted.get());//makes sure that poll was interrupted
        assertTrue(isEmpty(resources.getResources()));
        assertFalse(resources.isComplete());

        willCompleteSomeday.complete(jsonableOf("man"));

        resources = assembler.assembled().pollIncludedResources();
        assertDataTag(resources,
                "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n");
    }

    @Test
    public void testPastDeadlineIncludesAlreadyCompletedResults() throws Exception {

        int deadlineSeconds = 1;
        WebResourceAssembler assembler = f.factory().create().asyncDataDeadline(deadlineSeconds, TimeUnit.SECONDS).build();

        CompletableFuture<Jsonable> completesAfterDeadline = new CompletableFuture<>();
        assembler.data().requireData("duff", completesAfterDeadline);

        WebResourceSet resources = assembler.assembled().drainIncludedResources();
        assertTrue(isEmpty(resources.getResources()));
        assertFalse(resources.isComplete());

        TimeUnit.SECONDS.sleep(deadlineSeconds + 1);
        completesAfterDeadline.complete(jsonableOf("man"));

        resources = assembler.assembled().drainIncludedResources();
        assertDataTag(resources,
                "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n"
        );
        assertTrue(resources.isComplete());
    }

    @Test
    public void testPollActuallyBlocks() {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

        WebResourceAssembler assembler = f.create();
        CompletableFuture<Jsonable> completesLater = new CompletableFuture<>();

        assembler.data().requireData("duff", completesLater);

        scheduler.schedule(() -> completesLater.complete(jsonableOf("man")), 1, TimeUnit.SECONDS);
        WebResourceSet resources = assembler.assembled().pollIncludedResources();
        assertDataTag(resources,
                "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n"
        );
        assertTrue(resources.isComplete());
    }

    @Test
    public void testPollDoesNotBlockForInitiallyEmptyBigpipe() {
        WebResourceAssembler assembler = f.create();
        WebResourceSet resources = assertDoesNotBlock(() -> assembler.assembled().pollIncludedResources());
        assertEmptyDataTag(resources);
        assertTrue(resources.isComplete());

    }

    @Test
    public void testPollDoesNotBlockForEmptyBigpipe() {
        WebResourceAssembler assembler = f.create();
        CompletableFuture<Jsonable> future = CompletableFuture.completedFuture(jsonableOf("man"));
        assembler.data().requireData("duff", future);

        // should not block (it is already complete)
        WebResourceSet resources = assertDoesNotBlock(() -> assembler.assembled().pollIncludedResources());
        assertDataTag(resources,
                "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n"
        );
        assertTrue(resources.isComplete());

        // call poll again, still should not block
        resources = assertDoesNotBlock(() -> assembler.assembled().pollIncludedResources());
        assertEmptyDataTag(resources);
        assertTrue(resources.isComplete());
    }

    private <T> T assertDoesNotBlock(Callable<T> runnable) {
        try {
            long t0 = System.currentTimeMillis();
            T result = runnable.call();
            long t1 = System.currentTimeMillis();
            long timetaken = t1 - t0;
            // the deadline in ASYNC_DEADLINE_SECONDS is 5s, so if it took less than 100s it probably didn't block
            assertTrue("took too long: " + timetaken, timetaken < 100);
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testCopyDoesNotIncludeCompletePromises() {
        WebResourceAssembler assembler = f.create();

        CompletableFuture<Jsonable> success = CompletableFuture.completedFuture(jsonableOf("man"));

        assembler.data().requireData("duff", success);
        WebResourceAssembler copyAssembler = assembler.copy();

        WebResourceSet resourcesOrig = assembler.assembled().drainIncludedResources();
        WebResourceSet resourcesCopy = copyAssembler.assembled().drainIncludedResources();

        assertDataTag(resourcesOrig, "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n");
        assertTrue(resourcesOrig.isComplete());
        assertTrue(isEmpty(resourcesCopy.getResources()));
        assertTrue(resourcesCopy.isComplete());

    }

    @Test
    public void testCopyDoesNotIncludeIncompletePromises() {
        WebResourceAssembler assembler = f.create();

        CompletableFuture<Jsonable> completeAfterCopy = new CompletableFuture<>();

        assembler.data().requireData("duff", completeAfterCopy);
        WebResourceAssembler copyAssembler = assembler.copy();

        completeAfterCopy.complete(jsonableOf("man"));

        WebResourceSet resourcesOrig = assembler.assembled().drainIncludedResources();
        WebResourceSet resourcesCopy = copyAssembler.assembled().drainIncludedResources();

        assertDataTag(resourcesOrig, "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n");
        assertTrue(resourcesOrig.isComplete());
        assertTrue(isEmpty(resourcesCopy.getResources()));
        assertTrue(resourcesCopy.isComplete());
    }

    @Test
    public void testPeekDoesNotReturnCompletePromises() {
        WebResourceAssembler assembler = f.create();

        CompletableFuture<Jsonable> completeBeforePeek = new CompletableFuture<>();

        assembler.data().requireData("duff", completeBeforePeek);

        completeBeforePeek.complete(jsonableOf("man"));

        WebResourceSet resourcesPeek = assembler.assembled().peek();
        WebResourceSet resourcesOrig = assembler.assembled().drainIncludedResources();

        assertDataTag(resourcesOrig, "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n");
        assertTrue(resourcesOrig.isComplete());
        assertTrue(isEmpty(resourcesPeek.getResources()));
        assertFalse(resourcesPeek.isComplete());

    }
}
