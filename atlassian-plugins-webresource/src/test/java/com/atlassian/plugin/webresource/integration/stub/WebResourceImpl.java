package com.atlassian.plugin.webresource.integration.stub;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.webresource.DefaultBigPipeConfiguration;
import com.atlassian.plugin.webresource.DefaultResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.PluginResourceLocatorImpl;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptorBuilder;
import com.atlassian.plugin.webresource.WebResourceUrlProviderImpl;
import com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.impl.annotators.ListOfAnnotators;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.modules.ModulesDescriptor;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.integration.stub.dsl.ModuleData;
import com.atlassian.plugin.webresource.integration.stub.dsl.ModulesData;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.servlet.PluginResourceDownload;
import com.atlassian.plugin.webresource.transformer.ContentTransformerFactory;
import com.atlassian.plugin.webresource.transformer.ContentTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformers;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformersSupplier;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.spi.NoOpResourceCompiler;
import org.apache.commons.io.IOUtils;
import org.mockito.ArgumentMatchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.setField;
import static com.atlassian.plugin.webresource.impl.support.Support.parseXml;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

// Web Resource stub using modern implementation.
public class WebResourceImpl extends WebResource {
    protected PluginResourceLocator resourceLocator;
    protected PluginAccessor pluginAccessor;
    protected WebResourceIntegration integration;
    protected PageBuilderService pageBuilderService;
    protected ResourceBatchingConfiguration batchingConfiguration;
    protected Config config;
    protected PluginResourceDownload pluginResourceDownload;
    protected WebResourceUrlProviderImpl urlProvider;
    protected StaticTransformers staticTransformers;
    protected Map<String, Object> requestCache;
    private DefaultWebResourceAssemblerFactory webResourceAssemblerFactory;

    @Override
    public void requireResource(String key) {
        pageBuilderService.assembler().resources().requireWebResource(key);
    }

    @Override
    public void requireModule(String name) {
        pageBuilderService.assembler().resources().requireModule(name);
    }

    @Override
    public PageBuilderService getPageBuilderService() {
        return pageBuilderService;
    }

    @Override
    public void requireContext(String key) {
        pageBuilderService.assembler().resources().requireContext(key);
    }

    @Override
    public void include(List<String> keys) {
        for (String key : keys) {
            if (key.contains(Config.CONTEXT_PREFIX)) {
                requireContext(key.replaceAll(Config.CONTEXT_PREFIX + ":", ""));
            } else {
                requireResource(key);
            }
        }
    }

    @Override
    public void exclude(List<String> keys) {
        if (keys.isEmpty()) {
            return;
        }
        LinkedHashSet<String> excludedWebResources = new LinkedHashSet<>();
        LinkedHashSet<String> excludedContexts = new LinkedHashSet<>();
        for (String key : keys) {
            if (key.contains(Config.CONTEXT_PREFIX)) {
                excludedContexts.add(key.replaceAll(Config.CONTEXT_PREFIX + ":", ""));
            } else {
                excludedWebResources.add(key);
            }
        }
        getPageBuilderService().assembler().resources().exclude(excludedWebResources, excludedContexts);
    }

    @Override
    public void requireData(String key, String value) {
        pageBuilderService.assembler().data().requireData(key, value);
    }

    // Returns HTML that should be included in the head of the page to include all the required resources.
    @Override
    public String pathsAsHtml() {
        StringWriter out = new StringWriter();
        pageBuilderService.assembler().assembled().drainIncludedResources().writeHtmlTags(out, UrlMode.AUTO);
        return out.toString();
    }

    public PrebakeWebResourceAssemblerFactory getWebResourceAssemblerFactory() {
        return webResourceAssemblerFactory;
    }

    @Override
    public void get(HttpServletRequest request, HttpServletResponse response) {
        try {
            pluginResourceDownload.serveFile(request, response);
        } catch (DownloadException e) {
            throw new RuntimeException(e);
        }
    }

    // Configure web resources according to given XML configuration.
    protected void initialize(ConfigurationData configurationData) {
        // Mocking external parts.
        pluginAccessor = mock(PluginAccessor.class);

        PluginEventManager eventManager = mock(PluginEventManager.class);

        integration = mock(WebResourceIntegration.class);
        when(integration.getRequestCache()).thenAnswer(invocationOnMock -> requestCache);
        when(integration.getPluginAccessor()).thenReturn(pluginAccessor);
        when(integration.getSystemBuildNumber()).thenReturn("system-build-number");
        when(integration.getSystemCounter()).thenReturn("system-build-counter");
        when(integration.getSuperBatchVersion()).thenReturn("super-batch-version");
        when(integration.getBaseUrl(com.atlassian.plugin.webresource.UrlMode.AUTO)).thenReturn("/app");
        when(integration.getI18nStateHash()).thenReturn("stat&");
        when(integration.getBigPipeConfiguration()).thenReturn(new DefaultBigPipeConfiguration());
        when(integration.getTemporaryDirectory()).thenReturn(getTempDir());
        when(integration.getCDNStrategy()).thenReturn(configurationData.cdnStrategy);
        when(integration.isCtCdnMappingEnabled()).thenReturn(configurationData.isCtCdnMappingEnabled);
        when(integration.getHostApplicationVersion()).thenReturn("product-version");
        when(integration.amdEnabled()).thenReturn(true);
        when(integration.getDefaultAmdModuleTransformers()).thenReturn(configurationData.defaultAmdModuleTransformers);
        when(integration.useAsyncAttributeForScripts()).thenReturn(configurationData.useAsync);
        when(integration.getSyncWebResourceKeys()).thenReturn(configurationData.syncWebResourceKeys);

        // Batching configuration.
        batchingConfiguration = spy(new DefaultResourceBatchingConfiguration());
        when(batchingConfiguration.isSuperBatchingEnabled()).thenReturn(configurationData.isSuperBatchEnabled);
        when(batchingConfiguration.isContextBatchingEnabled()).thenReturn(configurationData.isContextBatchingEnabled);
        when(batchingConfiguration.isPluginWebResourceBatchingEnabled()).thenReturn(configurationData.isWebResourceBatchingEnabled);
        when(batchingConfiguration.isJavaScriptTryCatchWrappingEnabled()).thenReturn(
                configurationData.isJavaScriptTryCatchWrappingEnabled);
        when(batchingConfiguration.isBatchContentTrackingEnabled()).thenReturn(false);
        when(batchingConfiguration.isSourceMapEnabled()).thenReturn(configurationData.isSourceMapEnabled);
        when(batchingConfiguration.optimiseSourceMapsForDevelopment()).thenReturn(configurationData.optimiseSourceMapForDevelopment);

        // Url Provider.
        urlProvider = new WebResourceUrlProviderImpl(integration);

        // Static Transformers.
        if (configurationData.staticTransformers != null) {
            this.staticTransformers = configurationData.staticTransformers;
        } else {
            this.staticTransformers = new DefaultStaticTransformers(new DefaultStaticTransformersSupplier
                    (integration, urlProvider, (url) -> url));
        }

        // Config.
        config = new Config(batchingConfiguration, integration, urlProvider, null,
                new TransformerCache(eventManager, integration.getPluginAccessor()), new NoOpResourceCompiler()) {
            @Override
            public String getCtCdnBaseUrl() {
                return configurationData.ctCdnBaseUrl;
            }
        };
        config.setStaticTransformers(staticTransformers);
        config = spy(config);
        doReturn(true).when(config).useConditionsForWebModules();
        doReturn(new ArrayList<>()).when(config).getBeforeAllResources();
        for (ConfigCallback cb : configurationData.configCallbacks) {
            cb.apply(config);
        }

        resourceLocator = new PluginResourceLocatorImpl(eventManager, config);

        // Globals.
        globals = resourceLocator.temporaryWayToGetGlobalsDoNotUseIt();

        // Support for HTTP.
        ContentTypeResolver contentTypeResolver = spy(new ContentTypeResolver() {
            @Override
            public String getContentType(String url) {
                return buildMap("css", "text/css", "js", "application/javascript", "png",
                        "image/png").get(Request.getType(url));
            }
        });
        pluginResourceDownload = new PluginResourceDownload(resourceLocator, contentTypeResolver, "UTF-8");

        reset();
    }

    public static File getTempDir() {
        try {
            return File.createTempFile("foo", "foo").getParentFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void reset() {
        requestCache = new HashMap<>();
        // Page builder service.
        webResourceAssemblerFactory = new DefaultWebResourceAssemblerFactory(globals);
        pageBuilderService = new DefaultPageBuilderService(integration, webResourceAssemblerFactory);
    }

    @Override
    public void applyConfiguration(final ConfigurationData configurationData) {
        initialize(configurationData);

        List<ContentTransformerModuleDescriptor> contentTransformerDescriptor = new
                ArrayList<ContentTransformerModuleDescriptor>();
        List<UrlReadingWebResourceTransformerModuleDescriptor> transformerDescriptors = new
                ArrayList<UrlReadingWebResourceTransformerModuleDescriptor>();
        List<WebResourceTransformerModuleDescriptor> legacyTransformerDescriptors = new
                ArrayList<WebResourceTransformerModuleDescriptor>();

        // Default transformers.
        for (Map.Entry<String, Class> entry : configurationData.defaultTransformerClasses.entrySet()) {
            transformerDescriptors.add(TestUtils.mockTransformer(entry.getKey(), entry.getKey(), entry.getValue()));
        }

        // Annotators.
        if (configurationData.annotators != null) {
            when(globals.getConfig().getContentAnnotator(any(String.class))).thenReturn(new ListOfAnnotators(configurationData.annotators));
            setField(globals, "config", globals.getConfig());
        }

        // Configuring plugins.
        List<WebResourceModuleDescriptor> listOfWebResourceDescriptors = new ArrayList<>();
        List<ModulesDescriptor> listOfModulesDescriptors = new ArrayList<>();

        for (PluginData pluginData : configurationData.plugins) {
            // Searching for classes.
            List<Class> classes = new ArrayList<>();
            for (WebResourceData webResourceData : pluginData.webResources) {
                for (WebResource.ConditionData condition : webResourceData.conditions) {
                    classes.add(condition.clazz);
                }
            }
            for (ModulesData moduleData : pluginData.modules) {
                classes.addAll(moduleData.conditions);
            }

            // Creating plugin.
            Plugin plugin = TestUtils.createTestPlugin(pluginData.key, "1", classes.toArray(new Class[classes.size()]));
            when(pluginAccessor.getPlugin(pluginData.key)).thenReturn(plugin);
            when(pluginAccessor.getEnabledPlugin(pluginData.key)).thenReturn(plugin);

            // Plugin resources.
            for (ResourceData resourceData : pluginData.resources) {
                when(plugin.getResourceLocation("download", resourceData.name)).thenReturn(new ResourceLocation
                        (resourceData.location, resourceData.name, ResourceUtils.getType(resourceData.name),
                                resourceData.contentType, resourceData.content, resourceData.params));
                final ResourceData finalResourceData = resourceData;
                // Callback should be used because otherwise resource input stream wouldn't be reset for the new usage.
                when(plugin.getResourceAsStream(resourceData.location + resourceData.filePath)).thenAnswer(new Answer<Object>() {
                    public Object answer(InvocationOnMock invocationOnMock) {
                        return IOUtils.toInputStream(finalResourceData.content);
                    }
                });
            }

            // Creating web resources.
            for (WebResourceData webResourceData : pluginData.webResources) {
                // Conditions.
                WebResourceModuleDescriptorBuilder descriptorBuilder = new WebResourceModuleDescriptorBuilder(plugin,
                        webResourceData.key);
                for (ConditionData condition : webResourceData.conditions) {
                    descriptorBuilder.addCondition(condition.clazz, condition.params, condition.invert);
                }

                // Resources.
                for (final ResourceData resourceData : webResourceData.resources) {
                    descriptorBuilder.addDescriptor("download", resourceData.name, resourceData.location,
                            resourceData.contentType, resourceData.params);
                    // Callback should be used because otherwise resource input stream wouldn't be reset for the new
                    // usage.
                    when(plugin.getResourceAsStream(resourceData.location + resourceData.filePath)).thenAnswer(new Answer<Object>() {
                        public Object answer(InvocationOnMock invocationOnMock) {
                            return IOUtils.toInputStream(resourceData.content);
                        }
                    });
                }

                // Files.
                for (final FileData fileData : webResourceData.files) {
                    // Callback should be used because otherwise resource input stream wouldn't be reset for the new
                    // usage.
                    when(plugin.getResourceAsStream(fileData.name)).thenAnswer(new Answer<Object>() {
                        public Object answer(InvocationOnMock invocationOnMock) {
                            return IOUtils.toInputStream(fileData.content);
                        }
                    });
                }

                // Dependencies.
                for (String dependencyKey : webResourceData.dependencies) {
                    descriptorBuilder.addDependency(dependencyKey);
                }

                // Module dependencies.
                for (String dependencyKey : webResourceData.moduleDependencies) {
                    descriptorBuilder.addModuleDependency(dependencyKey);
                }

                // Contexts.
                for (String contextKey : webResourceData.contexts) {
                    descriptorBuilder.addContext(contextKey);
                }

                // Tranformations.
                for (Map.Entry<String, List<String>> entry : webResourceData.transformations.entrySet()) {
                    descriptorBuilder.addTransformers(entry.getKey(), entry.getValue());
                }

                // Data providers.
                for (Tuple<String, Class<?>> item : webResourceData.dataProviders) {
                    String key = item.getFirst();
                    Class<?> klass = item.getLast();
                    descriptorBuilder.addDataProvider(key, klass);
                    try {
                        when(plugin.loadClass(ArgumentMatchers.eq(klass.getName()), any())).thenReturn((Class<Object>) klass);
                    } catch (ClassNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                }

                ModuleDescriptor descriptor = descriptorBuilder.build();
                listOfWebResourceDescriptors.add((WebResourceModuleDescriptor) descriptor);
                when(pluginAccessor.getEnabledPluginModule(pluginData.key + ":" + webResourceData.key)).thenReturn(descriptor);
            }

            // Creating web modules.
            for (ModulesData modulesData : pluginData.modules) {
                String descriptorXml = modulesData.toDescriptorXml();
                ModulesDescriptor descriptor = new ModulesDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, new DefaultHostContainer());
                descriptor.init(plugin, parseXml(descriptorXml));
                descriptor.enabled();
                listOfModulesDescriptors.add(descriptor);

                // Modules.
                for (final ModuleData moduleData : modulesData.modules) {
                    // Callback should be used because otherwise resource input stream wouldn't be reset for the new
                    // usage.
                    when(plugin.getResourceAsStream(moduleData.filePath)).thenAnswer(invocationOnMock ->
                            IOUtils.toInputStream(moduleData.content)
                    );
                }
            }

            // Preparing transformers.
            for (Map.Entry<String, Tuple<String, Class>> entry : pluginData.transformers.entrySet()) {
                String key = entry.getKey();
                String aliasKey = entry.getValue().getFirst();
                Class<?> klass = entry.getValue().getLast();
                if (ContentTransformerFactory.class.isAssignableFrom(klass)) {
                    contentTransformerDescriptor.add(TestUtils.mockContentTransformer(key, klass));
                } else if (WebResourceTransformerFactory.class.isAssignableFrom(klass)) {
                    transformerDescriptors.add(TestUtils.mockTransformer(key, aliasKey, klass));
                } else if (WebResourceTransformer.class.isAssignableFrom(klass)) {
                    if (aliasKey != null) {
                        throw new RuntimeException("legacy transformers can't have alias key!");
                    }
                    legacyTransformerDescriptors.add(TestUtils.mockLegacyTransformer(key, klass));
                } else {
                    throw new RuntimeException("invalid transformer class " + klass.getCanonicalName() + "!");
                }
            }

            String modulesXml = pluginData.toAtlassianModulesXml();
            when(plugin.getResourceAsStream(Config.ATLASSIAN_MODULES_XML)).thenAnswer(invocationOnMock ->
                    IOUtils.toInputStream(modulesXml)
            );
        }

        // Adding web resources.
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
                .thenReturn(listOfWebResourceDescriptors);

        // Adding modules.
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ModulesDescriptor.class))
                .thenReturn(listOfModulesDescriptors);

        // Adding module keys that should be included in superbatch.
        if (configurationData.addEverythingToSuperbatch) {
            List<String> allWebResourceKeys = new ArrayList<>();
            for (PluginData plugin : configurationData.plugins) {
                for (WebResourceData webResource : plugin.webResources) {
                    allWebResourceKeys.add(plugin.key + ":" + webResource.key);
                }
            }
            when(batchingConfiguration.getSuperBatchModuleCompleteKeys()).thenReturn(allWebResourceKeys);
        } else {
            when(batchingConfiguration.getSuperBatchModuleCompleteKeys()).thenReturn(configurationData.webResourcesToAddToSuperbatch);
        }

        // Adding transformers.
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ContentTransformerModuleDescriptor.class)).thenReturn
                (contentTransformerDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(UrlReadingWebResourceTransformerModuleDescriptor
                .class)).thenReturn(transformerDescriptors);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceTransformerModuleDescriptor.class))
                .thenReturn(legacyTransformerDescriptors);

        // make sure that snapshot is generated
        globals.getSnapshot();
    }

    @Override
    public PluginResourceLocator getResourceLocator() {
        return resourceLocator;
    }

    @Override
    public Config getConfig() {
        return config;
    }
}