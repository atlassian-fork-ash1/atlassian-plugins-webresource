package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import org.junit.Before;
import org.junit.Test;

import java.io.StringWriter;

import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;
import static org.junit.Assert.assertEquals;

public class TestWebResourceSetWithAsync {
    private AssemblerTestFixture f;
    private String beforeAll;

    @Before
    public void setUp() throws Exception {
        f = new AssemblerTestFixture(true, true, true);
        beforeAll = "<script>" + AssemblerTestFixture.getMockedInOrderLoaderContent() + "\n</script>";
    }

    @Test
    public void shouldInOrderLoaderBeWrittenAsInlineScript() throws Exception {
        f.mockPlugin("com.atlassian.plugins.atlassian-plugins-webresource-plugin:in-order-loader")
                .res("js/in-order-loader.js");
        WebResourceAssembler assembler = f.create();

        StringWriter writer = new StringWriter();
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        assertEquals(beforeAll, removeWebResourceLogs(writer.toString()));
    }

    @Test
    public void shouldAsyncAttrAndParamBePresentInScriptTag() throws Exception {
        f.mockPlugin("test.atlassian:resource-js").res("js-1.js");
        f.mockPlugin("com.atlassian.plugins.atlassian-plugins-webresource-plugin:in-order-loader")
                .res("js/in-order-loader.js");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:resource-js");
        StringWriter writer = new StringWriter();
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        assertEquals(
                beforeAll +
                "\n<script type=\"text/javascript\" src=\"/download/batch/test.atlassian:resource-js/test.atlassian:resource-js.js?async=true\" " +
                "data-wrm-key=\"test.atlassian:resource-js\" data-wrm-batch-type=\"resource\" " + Config.ASYNC_SCRIPT_PARAM_NAME +
                " " + Config.INITIAL_RENDERED_SCRIPT_PARAM_NAME + "></script>",
                removeWebResourceLogs(writer.toString()));
    }
}
