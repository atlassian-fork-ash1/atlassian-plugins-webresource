package com.atlassian.plugin.webresource.bigpipe;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestQueueFutureCompletionService {
    private static final long TIMEOUT = 100;
    private static final String FAIL = "__FAIL__";

    @Test
    public void testImmediateContent() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Gate gate = new Gate(completor, "key1");
        gate.open("peace pipe");
        Iterable<KeyedValue<String, String>> result = completor.poll();

        assertValues(result, v("key1", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testImmediateContentMultiple() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");
        Gate gate2 = new Gate(completor, "key2");
        gate1.open("peace pipe");
        gate2.open("crack pipe");
        Iterable<KeyedValue<String, String>> result = completor.poll();

        assertValues(result, v("key1", "peace pipe"), v("key2", "crack pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testImmediateContentWithDelay() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");
        Iterable<KeyedValue<String, String>> result = completor.poll();

        assertValues(result);
        assertFalse(completor.isComplete());

        gate1.open("peace pipe");

        result = completor.poll();
        assertValues(result, v("key1", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testWaitForContent() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");

        Future<Iterable<KeyedValue<String, String>>> future = waitForContent(completor);
        gate1.open("peace pipe");

        assertValues(future.get(), v("key1", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testWaitForContentAvailableImmediately() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");

        gate1.open("peace pipe");
        Iterable<KeyedValue<String, String>> result = completor.poll(TIMEOUT, TimeUnit.MILLISECONDS);

        assertValues(result, v("key1", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testWaitForContentSequential() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");
        Gate gate2 = new Gate(completor, "key2");

        Future<Iterable<KeyedValue<String, String>>> future = waitForContent(completor);
        gate1.open("peace pipe");

        assertValues(future.get(), v("key1", "peace pipe"));
        assertFalse(completor.isComplete());

        future = waitForContent(completor);
        gate2.open("crack pipe");

        assertValues(future.get(), v("key2", "crack pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testWaitForContentSequentialInverseOrder() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");
        Gate gate2 = new Gate(completor, "key2");

        Future<Iterable<KeyedValue<String, String>>> future = waitForContent(completor);
        gate2.open("crack pipe");

        assertValues(future.get(), v("key2", "crack pipe"));
        assertFalse(completor.isComplete());

        future = waitForContent(completor);
        gate1.open("peace pipe");

        assertValues(future.get(), v("key1", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testFail() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Gate gate = new Gate(completor, "key1");
        gate.fail();
        Iterable<KeyedValue<String, String>> result = completor.poll();

        assertValues(result, v("key1", FAIL));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testFailAndPass() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");
        Gate gate2 = new Gate(completor, "key2");
        gate1.fail();
        gate2.open("peace pipe");
        Iterable<KeyedValue<String, String>> result = completor.poll();

        assertValues(result, v("key1", FAIL), v("key2", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testImmediateNoContent() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Iterable<KeyedValue<String, String>> result = completor.poll();

        assertNotNull(result);
        assertValues(result);
        assertTrue(completor.isComplete());
    }

    @Test
    public void testWaitForNoContent() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();

        Iterable<KeyedValue<String, String>> result = completor.poll(TIMEOUT, TimeUnit.MILLISECONDS);

        assertNotNull(result);
        assertValues(result);
        assertTrue(completor.isComplete());
    }

    @Test
    public void testMoreAvailableChecksCompletedPromises() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();
        Gate gate = new Gate(completor, "key1");
        gate.open("hi");

        assertFalse(completor.isComplete());
    }

    @Test
    public void testForceAllCompletesRemainingPromises() throws Exception {
        FutureCompletionService<String, String> completor = createCompletionService();
        Gate gate = new Gate(completor, "key1");

        assertFalse(completor.isComplete());

        completor.forceCompleteAll();

        assertFalse(completor.isComplete());

        Iterable<KeyedValue<String, String>> result = completor.poll();
        assertNotNull(result);
        assertValues(result, v("key1", FAIL));
        assertTrue(completor.isComplete());

    }

    @Test
    public void spamTest() throws Exception {
        // Smoke test; tests that a crapload of concurrent executions does not result in
        // obviously incorrect behaviour.
        // This DOES NOT prove correctness; but it can prove incorrectness.
        spamTest(5, 2000, 5);
    }

    //RAID-551
    @Test(timeout = 60000)
    public void testWaitForContentBlockingBehaviour() {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        Thread currentThread = Thread.currentThread();

        FutureCompletionService<String, String> completor = createCompletionService();
        assertTrue(completor.isComplete());
        Gate gate1 = new Gate(completor, "bazinga");

        try {
            executor.schedule(currentThread::interrupt, 100, TimeUnit.MILLISECONDS);
            completor.waitAnyPendingToComplete();//hold in there, at least 100ms.
            fail();
        } catch (InterruptedException e) {
            //waitAnyPendingToComplete should be waiting and get interrupted
        }

        Thread.interrupted();

        completor.forceCompleteAll();
        try {
            executor.schedule(currentThread::interrupt, 100, TimeUnit.MILLISECONDS);
            completor.waitAnyPendingToComplete();
            //waitAnyPendingToComplete shouldn't be waiting
        } catch (InterruptedException e) {
            fail();
        }
    }

    private void spamTest(int numConsumers, int numKeys, int numTasksPerKey) throws Exception {
        // Tests multiple threads processing bigpipe tasks
        final ExecutorService executor = Executors.newCachedThreadPool();
        final FutureCompletionService<String, String> completor = createCompletionService();

        // Create bigpipe consumers
        Collection<Callable<Integer>> consumers = new ArrayList<>();
        for (int i = 0; i < numConsumers; ++i) {
            consumers.add(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    int count = 0;
                    boolean complete = false;
                    while (!complete) {
                        Iterable<KeyedValue<String, String>> result = completor.poll(TIMEOUT, TimeUnit.MILLISECONDS);
                        count += Iterables.size(result);
                        complete = completor.isComplete();
                    }
                    return count;
                }
            });
        }

        // Service generating bigpipe content
        int taskid = 0;
        for (int key = 0; key < numKeys; ++key) {
            for (int task = 0; task < numTasksPerKey; ++task) {
                final int value = task;
                CompletableFuture<String> future = new CompletableFuture<>();
                executor.submit(() -> future.complete(String.valueOf(value)));
                completor.add(String.valueOf(taskid++), future);
            }
        }

        // Service consuming bigpipe content
        List<Future<Integer>> futures = Executors.newCachedThreadPool().invokeAll(consumers);
        int count = 0;
        for (Future<Integer> future : futures) {
            count += future.get();
        }
        assertEquals(count, numKeys * numTasksPerKey);
    }

    private <K extends Comparable, V> Future<Iterable<KeyedValue<K, V>>> waitForContent(final FutureCompletionService<K, V> completor) {
        return Executors.newSingleThreadExecutor().submit(() -> completor.poll(TIMEOUT, TimeUnit.MILLISECONDS));
    }

    private TestValue v(String key, String value) {
        return new TestValue(key, value);
    }

    private static final class TestValue implements Comparable<TestValue> {
        final String key;
        final String value;

        private TestValue(String key, String value) {
            this.key = checkNotNull(key);
            this.value = checkNotNull(value);
        }

        @Override
        public boolean equals(Object o) {
            return EqualsBuilder.reflectionEquals(this, o);
        }

        @Override
        public int hashCode() {
            return HashCodeBuilder.reflectionHashCode(this);
        }

        @Override
        public int compareTo(TestValue testValue) {
            int c = key.compareTo(testValue.key);
            return 0 != c ? c : value.compareTo(value);
        }
    }

    private void assertValues(Iterable<KeyedValue<String, String>> actual, TestValue... expectedCompleted) throws IOException {
        Iterable<TestValue> transformedActual = Iterables.transform(actual, new Function<KeyedValue<String, String>,
                TestValue>() {
            @Override
            public TestValue apply(KeyedValue<String, String> value) {
                return new TestValue(value.key(), value.value().isRight() ? value.value().right().get() : FAIL);
            }
        });
        Iterable<TestValue> transformedExcepted = null == expectedCompleted ? emptyList() : asList(expectedCompleted);

        assertIterablesEqualIgnoreOrder(transformedExcepted, transformedActual);
    }

    private static void assertIterablesEqualIgnoreOrder(Iterable<TestValue> expected, Iterable<TestValue> actual) {
        List<TestValue> expectedList = Lists.newLinkedList(expected);
        List<TestValue> actualList = Lists.newLinkedList(actual);
        // Sort before comparing, as order is unimportant
        Collections.sort(expectedList);
        Collections.sort(actualList);
        assertEquals(expectedList, actualList);
    }

    private static class Gate {
        private final CompletableFuture<String> future;

        private Gate(final FutureCompletionService<String, String> futureCompletionService, String key) {
            this.future = new CompletableFuture<>();
            futureCompletionService.add(key, future);
        }

        void open(String value) {
            this.future.complete(value);
        }

        void fail() {
            this.future.completeExceptionally(new RuntimeException());
        }

    }


    private QueueFutureCompletionService<String, String> createCompletionService() {
        return new QueueFutureCompletionService<>();
    }
}

