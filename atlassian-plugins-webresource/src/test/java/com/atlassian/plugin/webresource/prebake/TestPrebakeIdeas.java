package com.atlassian.plugin.webresource.prebake;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.AlwaysTrueCondition;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.condition.SimpleUrlReadingCondition;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.stub.ResponseData;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResource;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.DimensionAwareUrlReadingCondition;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static com.google.common.collect.ImmutableMap.of;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.size;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 */
public class TestPrebakeIdeas extends TestCase {
    public static class IsAdminCondition extends SimpleUrlReadingCondition {
        @Override
        protected boolean isConditionTrue() {
            throw new UnsupportedOperationException("Should not get here in prebake mode");
        }

        @Override
        protected String queryKey() {
            return "isadmin";
        }
    }

    public static class FeatureFlagCondition implements DimensionAwareUrlReadingCondition {
        private String flag;

        @Override
        public Dimensions computeDimensions() throws PluginParseException {
            return Dimensions.empty()
                    .andExactly(flag, "on", "off");
        }

        @Override
        public void addToUrl(UrlBuilder urlBuilder, Coordinate coord) {
            coord.copyTo(urlBuilder, flag);
        }

        @Override
        public void init(Map<String, String> params) throws PluginParseException {
            this.flag = params.get("flag");
        }

        @Override
        public void addToUrl(UrlBuilder urlBuilder) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean shouldDisplay(QueryParams params) {
            return "on".equals(params.get(flag));
        }
    }

    @Test
    public void spikePrebakeIdea() {
        wr.configure()
                .setStaticTransformers(TestUtils.emptyStaticTransformers())

                .plugin("plug1")
                .transformer("I18N", DimensionAwareMockI18nTransformer.class)
                .webResource("wr1")
                .transformation("js", "I18N")
                .resource("bar.js", "insidebarjs REPLACE")

                .plugin("plug2")
                .webResource("wr2")
                .dependency("plug1:wr1")
                .condition(FeatureFlagCondition.class, of("flag", "awesome"))
                .condition(IsAdminCondition.class)
                .context("general")
                .resource("foo.js", "insidefoojs")

                .webResource("wr3")
                .dependency("plug1:wr1")
                .context("general")
                .condition(IsAdminCondition.class)
                .resource("boo.js", "insideboojs")

                .webResource("wr4")
                .context("general")
                .resource("dog.js", "insidedogjs")

                .end();


        PrebakeWebResourceAssemblerFactory prebaker = wr.getWebResourceAssemblerFactory();
        Dimensions result = prebaker.computeDimensions();
        Dimensions expected = Dimensions.empty()
                .andExactly("locale", "en", "fr")
                .andExactly("isadmin", "true")
                .andAbsent("isadmin")
                .andExactly("awesome", "on", "off");

        assertEquals(expected, result);

        List<Multimap<String, ResponseData>> bakedcontent = result.cartesianProduct().map(coord -> {
            PrebakeWebResourceAssembler assembler = prebaker.create().withCoordinate(coord).build();
            assembler.resources().requireContext("general");

            Multimap<String, ResponseData> url2content = LinkedListMultimap.create();

            WebResourceSet webResourceSet;
            do {
                webResourceSet = assembler.assembled().pollIncludedResources();

                for (WebResource resource : webResourceSet.getResources()) {
                    if (resource instanceof PluginUrlResource) {
                        PluginUrlResource pr = (PluginUrlResource) resource;
                        assertTrue(pr.getPrebakeErrors().isEmpty());
                        String url = pr.getStaticUrl(UrlMode.RELATIVE);
                        url2content.put(url, wr.get(url));
                    }
                }
            } while (!webResourceSet.isComplete());

            return url2content;
        }).collect(Collectors.toList());

        assertEquals(8, bakedcontent.size());

        for (Multimap<String, ResponseData> responses : bakedcontent) {
            assertEquals(1, responses.size());
            String url = responses.keySet().iterator().next();
            Collection<ResponseData> responseDatas = responses.get(url);
            assertEquals(1, responseDatas.size());
            ResponseData response = responseDatas.iterator().next();

            // check for coords because we know what will be in the url
            assertThat(url, matches("contextbatch/js/general/batch.js"));
            boolean isadmin = url.contains("isadmin=true");
            boolean awesomeon = url.contains("awesome=on");
            boolean awesomeoff = url.contains("awesome=off");

            // If isadmin is false, the `plug1:wr1` resource with the i18n transformer will be excluded, and there
            // will be no locale in querystring.
            String insidebarjs = "";
            if (isadmin) {
                Matcher localeMatcher = Pattern.compile("locale=([a-z]+)").matcher(url);
                assertTrue(localeMatcher.find());
                String locale = localeMatcher.group(1);
                insidebarjs = "insidebarjs " + locale.toUpperCase();
            }

            if (isadmin && awesomeon) {
                assertThat(response.getContent(), matches(
                        insidebarjs, "insidefoojs", "insideboojs", "insidedogjs"
                ));
            } else if (isadmin && awesomeoff) {
                assertThat(response.getContent(), matches(
                        insidebarjs, "insideboojs", "insidedogjs"
                ));
                assertThat(response.getContent(), not(matches(
                        "insidefoojs"
                )));
            } else if (!isadmin && awesomeon) {
                assertThat(response.getContent(), matches(
                        "insidedogjs"
                ));
                assertThat(response.getContent(), not(matches(
                        "insideboojs", "insidefoojs", insidebarjs
                )));
            } else if (!isadmin && awesomeoff) {
                assertThat(response.getContent(), matches(
                        "insidedogjs"
                ));
                assertThat(response.getContent(), not(matches(
                        "insideboojs", "insidefoojs", insidebarjs
                )));
            } else {
                fail("unexpected url: " + url);
            }
        }

    }

    @Test
    public void testTaintedCondition() {
        wr.configure()
                .setStaticTransformers(TestUtils.emptyStaticTransformers())
                .plugin("plug1")
                .webResource("wr1")
                .context("general")
                .condition(AlwaysTrueCondition.class)
                .resource("bar.js", "insidebarjs")
                .end();

        PrebakeWebResourceAssemblerFactory prebaker = wr.getWebResourceAssemblerFactory();
        Dimensions dims = prebaker.computeDimensions();
        List<Coordinate> coordinates = dims.cartesianProduct().collect(Collectors.toList());
        assertEquals(1, coordinates.size());

        Coordinate coord = coordinates.get(0);
        PrebakeWebResourceAssembler assembler = prebaker.create().withCoordinate(coord).build();
        assembler.resources().requireContext("general");

        WebResourceSet webResourceSet = assembler.assembled().drainIncludedResources();
        assertEquals(1, size(webResourceSet.getResources()));

        PluginUrlResource webResource = (PluginUrlResource) get(webResourceSet.getResources(), 0);
        assertEquals(1, webResource.getPrebakeErrors().size());

    }

    @Test
    public void testTaintedTransform() {
        wr.configure()
                .setStaticTransformers(TestUtils.emptyStaticTransformers())
                .plugin("plug1")
                .transformer("I18N", MockI18nTransformer.class)
                .webResource("wr1")
                .transformation("js", "I18N")
                .context("general")
                .resource("bar.js", "insidebarjs")
                .end();

        PrebakeWebResourceAssemblerFactory prebaker = wr.getWebResourceAssemblerFactory();
        Dimensions dims = prebaker.computeDimensions();
        List<Coordinate> coordinates = dims.cartesianProduct().collect(Collectors.toList());
        assertEquals(1, coordinates.size());

        Coordinate coord = coordinates.get(0);
        PrebakeWebResourceAssembler assembler = prebaker.create().withCoordinate(coord).build();
        assembler.resources().requireContext("general");

        WebResourceSet webResourceSet = assembler.assembled().drainIncludedResources();
        assertEquals(1, size(webResourceSet.getResources()));

        PluginUrlResource webResource = (PluginUrlResource) get(webResourceSet.getResources(), 0);
        assertEquals(1, webResource.getPrebakeErrors().size());

    }

    @Test
    public void testTaintedWebResourceMessage() {
        wr.configure()
                .setStaticTransformers(TestUtils.emptyStaticTransformers())
                .plugin("plug1")
                .transformer("I18N", MockI18nTransformer.class)
                .webResource("wr1")
                .transformation("js", "I18N")
                .resource("bar.js", "insidebarjs")
                .webResource("wr2")
                .resource("foo.js", "insidefoojs")
                .end();

        PrebakeWebResourceAssemblerFactory prebaker = wr.getWebResourceAssemblerFactory();
        Dimensions dims = prebaker.computeDimensions();
        List<Coordinate> coordinates = dims.cartesianProduct().collect(Collectors.toList());
        assertEquals(1, coordinates.size());

        Coordinate coord = coordinates.get(0);
        PrebakeWebResourceAssembler assembler = prebaker.create().withCoordinate(coord).build();
        assembler.resources().requireWebResource("plug1:wr1");
        assembler.resources().requireWebResource("plug1:wr2");

        WebResourceSet webResourceSet = assembler.assembled().drainIncludedResources();
        assertEquals(2, size(webResourceSet.getResources()));

        for (WebResource webResource : webResourceSet.getResources()) {
            PluginJsResource jsResource = (PluginJsResource) webResource;
            String url = jsResource.getStaticUrl(UrlMode.RELATIVE);
            if (url.contains("wr1")) {
                assertEquals(1, jsResource.getPrebakeErrors().size());
            } else if (url.contains("wr2")) {
                assertEquals(0, jsResource.getPrebakeErrors().size());
            } else {
                fail("unexpected web resource " + url);
            }
        }

    }

    @Test
    public void testPrebakeBundleRelatedDimensions() {
        wr.configure()

                .plugin("plug1")
                .transformer("I18N", DimensionAwareMockI18nTransformer.class)
                .webResource("wr1")
                .transformation("js", "I18N")
                .resource("bar.js", "insidebarjs REPLACE")

                .plugin("plug2")
                .webResource("wr2")
                .resource("bee.css", "insidebeecss")

                .plugin("plug3")
                .webResource("wr3")
                .dependency("plug1:wr1")
                .dependency("plug2:wr2")
                .condition(FeatureFlagCondition.class, of("flag", "awesome"))
                .context("general")
                .resource("foo.js", "insidefoojs")

                .webResource("wr4")
                .dependency("plug1:wr1")
                .context("general")
                .condition(IsAdminCondition.class)
                .resource("boo.js", "insideboojs")

                .end();

        Bundle bundle = wr.getGlobals().getSnapshot().get("plug3:wr3");
        PrebakeWebResourceAssemblerFactory assembler = wr.getWebResourceAssemblerFactory();

        Dimensions result = assembler.computeBundleDimensions(bundle);
        Dimensions expected = Dimensions.empty()
                .andExactly("locale", "en", "fr")
                .andExactly("relative-url", "true") // comes from DefaultStaticTransformers applied to bee.css
                .andAbsent("relative-url") // comes from DefaultStaticTransformers applied to bee.css
                .andExactly("awesome", "on", "off");

        assertEquals(expected, result);
    }
}
