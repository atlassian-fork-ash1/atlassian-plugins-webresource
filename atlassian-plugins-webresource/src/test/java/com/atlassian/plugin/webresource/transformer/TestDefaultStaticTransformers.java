package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.cdn.CdnResourceUrlTransformer;
import com.atlassian.plugin.webresource.impl.UrlBuildingStrategy;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.DimensionAwareTransformerUrlBuilder;
import com.atlassian.webresource.api.prebake.DimensionAwareWebResourceTransformerFactory;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import javax.annotation.Nullable;

import static com.atlassian.plugin.webresource.TestUtils.asContent;
import static com.atlassian.plugin.webresource.TestUtils.emptyQueryParams;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.asDownloadableResource;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultStaticTransformers {

    private static final String CSS_CONTENT = "p { color: red; }";
    private static final String CSS_LOCATION = "/wow.css";
    private static final String CSS_NAME = "wow.css";
    private static final String CSS_PATH = "/test/path/wow.css";

    private static final String DIM1_KEY = "dim1";
    private static final String[] DIM1_VALUES = {"dim1_val1", "dim2_val2"};
    private static final String DIM2_KEY = "dim2";
    private static final String[] DIM2_VALUES = {"dim2_val1", "dim2_val2", "dim2_val3"};
    private static final Dimensions DIMENSIONS = Dimensions.empty()
            .andExactly(DIM1_KEY, DIM1_VALUES)
            .andExactly(DIM2_KEY, DIM2_VALUES);

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void doNothing() {
    }

    @Mock
    StaticTransformersSupplier staticTransformersSupplier;
    @Mock
    CdnResourceUrlTransformer cdnResourceUrlTransformer;

    private StaticTransformers staticTransformers;

    @Before
    public void setup() {
        when(staticTransformersSupplier.computeDimensions()).thenReturn(DIMENSIONS);
        staticTransformers = new DefaultStaticTransformers(staticTransformersSupplier);
    }

    @Test
    public void testComputeDimensions() {
        Dimensions dims = staticTransformers.computeDimensions();
        assertThat(dims, equalTo(DIMENSIONS));
    }

    @Test
    public void testAddToUrl() {
        DimensionAwareTransformerUrlBuilder transformerUrlBuilder = mock(DimensionAwareTransformerUrlBuilder.class);
        mockTransformerSuppliers(
                trans("css", transformerUrlBuilder, mock(UrlReadingWebResourceTransformer.class))
        );

        UrlBuilder urlBuilder = mock(UrlBuilder.class);
        staticTransformers.addToUrl("css", new TransformerParameters("com.test", "my-module", null),
                urlBuilder, UrlBuildingStrategy.normal());

        verify(transformerUrlBuilder).addToUrl(urlBuilder);
    }

    @Test
    public void testAddToUrlOrder() {
        DimensionAwareTransformerUrlBuilder transformerUrlBuilder1 = mock(DimensionAwareTransformerUrlBuilder.class);
        DimensionAwareTransformerUrlBuilder transformerUrlBuilder2 = mock(DimensionAwareTransformerUrlBuilder.class);
        mockTransformerSuppliers(
                trans("css", transformerUrlBuilder1, mock(UrlReadingWebResourceTransformer.class)),
                trans("css", transformerUrlBuilder2, mock(UrlReadingWebResourceTransformer.class))
        );

        UrlBuilder urlBuilder = mock(UrlBuilder.class);
        staticTransformers.addToUrl("css", new TransformerParameters("com.test", "my-module", null), urlBuilder, UrlBuildingStrategy.normal());

        InOrder inOrder = inOrder(transformerUrlBuilder1, transformerUrlBuilder2);

        inOrder.verify(transformerUrlBuilder1).addToUrl(urlBuilder);
        inOrder.verify(transformerUrlBuilder2).addToUrl(urlBuilder);
    }

    @Test
    public void testAddToUrlFilter() {
        DimensionAwareTransformerUrlBuilder transformerUrlBuilder1 = mock(DimensionAwareTransformerUrlBuilder.class);
        DimensionAwareTransformerUrlBuilder transformerUrlBuilder2 = mock(DimensionAwareTransformerUrlBuilder.class);
        mockTransformerSuppliers(
                trans("css", transformerUrlBuilder1, mock(UrlReadingWebResourceTransformer.class)),
                trans("js", transformerUrlBuilder2, mock(UrlReadingWebResourceTransformer.class))
        );

        UrlBuilder urlBuilder = mock(UrlBuilder.class);
        staticTransformers.addToUrl("css", new TransformerParameters("com.test", "my-module", null), urlBuilder, UrlBuildingStrategy.normal());

        verify(transformerUrlBuilder1).addToUrl(urlBuilder);
        verify(transformerUrlBuilder2, never()).addToUrl(urlBuilder);
    }

    @Test
    public void testTransform() {
        UrlReadingWebResourceTransformer transformer = mockTransformer();
        mockTransformerSuppliers(
                trans("css", mock(DimensionAwareTransformerUrlBuilder.class), transformer)
        );

        Content content = asContent(CSS_CONTENT);
        ResourceLocation resourceLocation = mockResourceLocation(CSS_LOCATION, CSS_NAME);
        QueryParams queryParams = emptyQueryParams();

        TransformerParameters transformerParameters = new TransformerParameters("com.test", "my-module", null);
        staticTransformers.transform(content, transformerParameters, resourceLocation, CSS_PATH, queryParams, null);

        ArgumentCaptor<TransformableResource> input = ArgumentCaptor.forClass(TransformableResource.class);
        verify(transformer).transform(input.capture(), eq(queryParams));
        assertThat(TestUtils.toString(input.getValue().nextResource()), equalTo(CSS_CONTENT));
    }

    @Test
    public void testTransformOrder() {
        UrlReadingWebResourceTransformer transformer1 = mockTransformer();
        UrlReadingWebResourceTransformer transformer2 = mockTransformer();
        mockTransformerSuppliers(
                trans("css", mock(DimensionAwareTransformerUrlBuilder.class), transformer1),
                trans("css", mock(DimensionAwareTransformerUrlBuilder.class), transformer2)
        );

        Content content = asContent(CSS_CONTENT);
        String cssTransformed = CSS_CONTENT + " .class { padding: 0px;} ";
        DownloadableResource transformed = asDownloadableResource(asContent(cssTransformed));
        ResourceLocation resourceLocation = mockResourceLocation("/wow.css", "wow.css");
        QueryParams queryParams = emptyQueryParams();

        when(transformer1.transform(anyObject(), eq(queryParams))).thenReturn(transformed);
        staticTransformers.transform(content, new TransformerParameters("com.test", "my-module", null), resourceLocation, "",
                queryParams, null);

        InOrder inOrder = inOrder(transformer1, transformer2);

        ArgumentCaptor<TransformableResource> input1 = ArgumentCaptor.forClass(TransformableResource.class);
        inOrder.verify(transformer1).transform(input1.capture(), eq(queryParams));
        assertThat(TestUtils.toString(input1.getValue().nextResource()), equalTo(CSS_CONTENT));

        ArgumentCaptor<TransformableResource> input2 = ArgumentCaptor.forClass(TransformableResource.class);
        inOrder.verify(transformer2).transform(input2.capture(), eq(queryParams));
        assertThat(TestUtils.toString(input2.getValue().nextResource()), equalTo(cssTransformed));
    }

    @Test
    public void testTransformFilter() {
        UrlReadingWebResourceTransformer transformer1 = mockTransformer();
        UrlReadingWebResourceTransformer transformer2 = mockTransformer();
        mockTransformerSuppliers(
                trans("css", mock(DimensionAwareTransformerUrlBuilder.class), transformer1),
                trans("js", mock(DimensionAwareTransformerUrlBuilder.class), transformer2)
        );

        Content content = asContent("");
        ResourceLocation resourceLocation = mockResourceLocation("/wow.css", "wow.css");
        QueryParams queryParams = emptyQueryParams();

        staticTransformers.transform(content, new TransformerParameters("com.test", "my-module", null), resourceLocation, "",
                queryParams, null);

        verify(transformer1).transform(anyObject(), eq(queryParams));
        verify(transformer2, never()).transform(anyObject(), anyObject());
    }

    private static ResourceLocation mockResourceLocation(String location, String name) {
        ResourceLocation resourceLocation = mock(ResourceLocation.class);
        when(resourceLocation.getLocation()).thenReturn(location);
        when(resourceLocation.getName()).thenReturn(name);
        return resourceLocation;
    }

    private static TransformerForType trans(
            String type,
            DimensionAwareTransformerUrlBuilder urlBuilder,
            UrlReadingWebResourceTransformer transformer) {
        DimensionAwareWebResourceTransformerFactory mock = mock(DimensionAwareWebResourceTransformerFactory.class);
        when(mock.makeUrlBuilder(any(TransformerParameters.class))).thenReturn(urlBuilder);
        when(mock.makeResourceTransformer(any(TransformerParameters.class))).thenReturn(transformer);
        return new TransformerForType(type, mock);
    }

    private void mockTransformerSuppliers(final TransformerForType... transformerSpecs) {
        when(staticTransformersSupplier.get(any(String.class))).thenAnswer(new Answer<Iterable<DimensionAwareWebResourceTransformerFactory>>() {
            @Override
            public Iterable<DimensionAwareWebResourceTransformerFactory> answer(InvocationOnMock invocation) throws Throwable {
                final String type = (String) invocation.getArguments()[0];
                return toTransformerFactories(asList(transformerSpecs), new Predicate<TransformerForType>() {
                    @Override
                    public boolean apply(@Nullable TransformerForType transformerSpec) {
                        return type.equals(transformerSpec.type);
                    }
                });
            }
        });
        when(staticTransformersSupplier.get(any(ResourceLocation.class))).thenAnswer(new Answer<Iterable<DimensionAwareWebResourceTransformerFactory>>() {
            @Override
            public Iterable<DimensionAwareWebResourceTransformerFactory> answer(InvocationOnMock invocation) throws Throwable {
                final ResourceLocation resourceLocation = (ResourceLocation) invocation.getArguments()[0];
                return toTransformerFactories(asList(transformerSpecs), new Predicate<TransformerForType>() {
                    @Override
                    public boolean apply(@Nullable TransformerForType transformerSpec) {
                        return resourceLocation.getName().endsWith("." + transformerSpec.type);
                    }
                });
            }
        });
    }

    private static Iterable<DimensionAwareWebResourceTransformerFactory> toTransformerFactories(
            Iterable<TransformerForType> transformerSpecs,
            Predicate<TransformerForType> predicate) {
        return Iterables.transform(Iterables.filter(transformerSpecs, predicate), new Function<TransformerForType, DimensionAwareWebResourceTransformerFactory>() {
            @Override
            public DimensionAwareWebResourceTransformerFactory apply(@Nullable TransformerForType transformerSpec) {
                return transformerSpec.transformerFactory;
            }
        });
    }

    private static final class TransformerForType {
        final String type;
        final DimensionAwareWebResourceTransformerFactory transformerFactory;

        private TransformerForType(String type, DimensionAwareWebResourceTransformerFactory transformerFactory) {
            this.type = type;
            this.transformerFactory = transformerFactory;
        }
    }

    private static UrlReadingWebResourceTransformer mockTransformer() {
        UrlReadingWebResourceTransformer transformer = mock(UrlReadingWebResourceTransformer.class);
        when(transformer.transform(any(TransformableResource.class), any(QueryParams.class)))
                .thenReturn(asDownloadableResource(asContent("")));
        return transformer;
    }
}
