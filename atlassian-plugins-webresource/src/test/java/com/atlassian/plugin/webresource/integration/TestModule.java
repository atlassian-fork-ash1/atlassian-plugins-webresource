package com.atlassian.plugin.webresource.integration;

import com.atlassian.plugin.webresource.AlwaysTrueCondition;
import com.atlassian.plugin.webresource.condition.AlwaysTrueLegacyCondition;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;
import com.atlassian.webresource.spi.TransformationDto;
import com.atlassian.webresource.spi.TransformerDto;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.printShortPaths;
import static com.atlassian.plugin.webresource.impl.http.Router.escapeSlashes;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextResourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.superBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;

public class TestModule extends TestCase
{
    public void prepareSmokeTest(WebResource.ConfigCallback configCallback)
    {
        wr.configure(configCallback)
            .plugin("plugin")
                .transformer("AddLocation", AddLocation.class)
                .modules("js")
                    .transformation("js", "AddLocation")
                    .condition(AlwaysTrueCondition.class)
                    .context("dashboard", "general")
                    .module("vendor/backbone-1.2.0.js")
                    .module("lib.js")
                        .dependency("./vendor/backbone-1.2.0")
                    .module("dashboard.js")
                        .dependency("./lib")
                        .dependency("css!./lib")
                .modules("css")
                    .transformation("css", "AddLocation")
                    .condition(AlwaysTrueCondition.class)
                    .module("lib.css")

        .end();

        wr.requireContext("general");
    }

    @Test
    public void smokeTestWithContextBatching()
    {
        prepareSmokeTest(config -> {});

        List<String> paths = wr.paths();
        String batchCssUrl = contextBatchUrl("general", "css", buildMap("always-true", "true", "location", "true"));
        String batchJsUrl = contextBatchUrl("general", "js", buildMap("always-true", "true", "location", "true"));
        assertThat(paths, matches(
            batchCssUrl,
            batchJsUrl
        ));

        assertThat(wr.getContent(batchCssUrl), matches(
            "/* module = 'lib.css' */",
            "location=plugin/lib.css (transformer)",
            "content of module lib.css"
        ));

        assertThat(wr.getContent(batchJsUrl), matches(
            "location=plugin/vendor/backbone-1.2.0.js (transformer)",
            "content of module vendor/backbone-1.2.0",

            "location=plugin/lib.js (transformer)",
            "content of module lib",

            "location=plugin/dashboard.js (transformer)",
            "content of module dashboard",

            "require([\"dashboard\"], function(){})"
        ));
    }

    @Test
    public void smokeTestWithoutContextBatching()
    {
        prepareSmokeTest(config -> doReturn(false).when(config).isContextBatchingEnabled());

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            webResourceBatchUrl("lib.css", "css", buildMap("always-true", "true", "location", "true")),
            webResourceBatchUrl(escapeSlashes("vendor/backbone-1.2.0"), "js", buildMap("always-true", "true", "location", "true")),
            webResourceBatchUrl("lib", "js", buildMap("always-true", "true", "location", "true")),
            webResourceBatchUrl("dashboard", "js", buildMap("always-true", "true", "location", "true")),
            contextUrl("general")
        ));

        String content = wr.getContent(paths);
        assertThat(content, matches(
            "/* module = 'lib.css' */",
            "location=plugin/lib.css (transformer)",
            "content of module lib.css",

            "location=plugin/vendor/backbone-1.2.0.js (transformer)",
            "content of module vendor/backbone-1.2.0",

            "location=plugin/lib.js (transformer)",
            "content of module lib",

            "location=plugin/dashboard.js (transformer)",
            "content of module dashboard",

            "require([\"dashboard\"], function(){})"
        ));
    }

    @Test
    public void smokeTestWithoutWebResourceBatching()
    {
        prepareSmokeTest(config -> {
            doReturn(false).when(config).isContextBatchingEnabled();
            doReturn(false).when(config).isWebResourceBatchingEnabled();
        });

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            resourceUrl("lib.css", "lib.css", buildMap("always-true", "true", "location", "true")),
            resourceUrl(escapeSlashes("vendor/backbone-1.2.0"), "vendor/backbone-1.2.0.js", buildMap("always-true", "true", "location", "true")),
            resourceUrl("lib", "lib.js", buildMap("always-true", "true", "location", "true")),
            resourceUrl("dashboard", "dashboard.js", buildMap("always-true", "true", "location", "true")),
            contextResourceUrl("general")
        ));

        String content = wr.getContent(paths);
        assertThat(content, matches(
            "location=plugin/lib.css (transformer)",
            "content of module lib.css",

            "location=plugin/vendor/backbone-1.2.0.js (transformer)",
            "content of module vendor/backbone-1.2.0",

            "location=plugin/lib.js (transformer)",
            "content of module lib",

            "location=plugin/dashboard.js (transformer)",
            "content of module dashboard",

            "require([\"dashboard\"], function(){})"
        ));
    }

    @Test
    public void shouldImmediatelyResolveModulesRequiredExplicitly()
    {
        wr.configure()
            .plugin("plugin")
                .modules("js")
                    .module("dashboard.js")
        .end();

        wr.requireModule("dashboard");
        assertThat(wr.paths(), matches(
            webResourceBatchUrl("dashboard", "js")
        ));

        assertThat(wr.getContent(), matches(
            "content of module dashboard",
            "require([\"dashboard\"], function(){})"
        ));
    }

    @Test
    public void shouldImmediatelyResolveModulesInjectedInContext()
    {
        wr.configure()
            .plugin("plugin")
                .modules("js")
                    .context("dashboard", "general")
                    .module("dashboard.js")
        .end();

        wr.requireContext("general");

        assertThat(wr.paths(), matches(
            contextBatchUrl("general", "js"))
        );

        assertThat(wr.getContent(), matches(
            "content of module dashboard",

            "require([\"dashboard\"], function(){})"
        ));
    }

    void prepareModulesWithWebResourceDependency(WebResource.ConfigCallback cc)
    {
        wr.configure(cc)
            .plugin("plugin")
                .webResource("legacy")
                    .resource("legacy.js")
                .modules("js")
                    .context("dashboard", "general")
                    .module("dashboard.js")
                        .dependency("wr!plugin:legacy")
        .end();

        wr.requireContext("general");
    }

    @Test
    public void shouldUseWebResourcesFromModules()
    {
        prepareModulesWithWebResourceDependency(config -> {});
        assertThat(wr.paths(), matches(contextBatchUrl("general", "js")));

        assertThat(wr.getContent(), matches(
            "content of legacy.js",
            "content of module dashboard"
        ));
    }

    @Test
    public void shouldUseWebResourcesFromModulesWithContextBatchingDisabled()
    {
        prepareModulesWithWebResourceDependency(config -> {
            doReturn(false).when(config).isContextBatchingEnabled();
        });

        assertThat(wr.paths(), matches(
            webResourceBatchUrl("plugin:legacy", "js"),
            webResourceBatchUrl("dashboard", "js"),
            contextUrl("general")
        ));
        assertThat(wr.getContent(), matches(
            "content of legacy.js",
            "content of module dashboard",
            "require([\"dashboard\"], function(){});"
        ));
    }

    @Test
    public void shouldUseWebResourcesFromModulesWithWebResourceBatchingDisabled()
    {
        prepareModulesWithWebResourceDependency(config -> {
            doReturn(false).when(config).isContextBatchingEnabled();
            doReturn(false).when(config).isWebResourceBatchingEnabled();
        });

        assertThat(wr.paths(), matches(
            resourceUrl("plugin:legacy", "legacy.js"),
            resourceUrl("dashboard", "dashboard.js"),
            contextResourceUrl("general")
        ));
        assertThat(wr.getContent(), matches(
            "content of legacy.js",
            "content of module dashboard"
        ));
    }

    @Test
    public void shouldUseModulesFromWebResources()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("legacy")
                    .context("general")
                    .moduleDependency("dashboard")
                    .resource("legacy.js")
                .modules("js")
                    .module("dashboard.js")
        .end();

        assertThat(wr.getContent(contextBatchUrl("general", "js")), matches(
            "content of module dashboard",
            "content of legacy.js"
        ));
    }

        @Test
    public void shouldNotAllowToUseWebResourcesFromModulesWithoutPrefix()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("legacy")
                    .resource("legacy.js")
                .modules("js")
                    .context("dashboard", "general")
                    .module("dashboard.js")
                        // Referencing web resource without prefix, it's not allowed.
                        .dependency("plugin:legacy")
        .end();

        String content = wr.getContent(contextBatchUrl("general", "js"));
        assertThat(content, not(matches("content of legacy.js")));
        assertThat(content, matches(
            "content of module dashboard"
        ));
    }

    @Test
    public void shouldExposeModulesToOtherPlugins()
    {
        wr.configure()
            .plugin("p1")
                .modules("js")
                    .module("a.js")
            .plugin("p2")
                .modules("js")
                    .context("b", "general")
                    .module("b.js")
                        .dependency("a")
        .end();

        wr.requireContext("general");
        assertThat(wr.paths(), matches(
            contextBatchUrl("general", "js")
        ));

        assertThat(wr.getContent(), matches(
            "content of module a", "content of module b"
        ));
    }

    @Test
    public void shouldNotAllowToUseWebResourceDependencyAsModuleDependency()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("legacy")
                    .context("general")
                    // not allowed, moduleDependnecy should be used.
                    .dependency("dashboard")
                    .resource("legacy.js")
                .modules("js")
                    .module("dashboard.js")
        .end();

        assertThat(wr.getContent(contextBatchUrl("general", "js")), matches(
            "content of legacy.js"
        ));
        assertThat(wr.getContent(contextBatchUrl("general", "js")), not(matches(
            "content of module dashboard"
        )));
    }

    @Test
    public void shouldNotAllowToUseModuleAsWebResourceDependency()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("legacy")
                    .context("general")
                    // not allowed, dependency should be used.
                    .moduleDependency("plugin:a")
                    .resource("legacy.js")
                .webResource("a")
                    .resource("a1.js")
        .end();

        assertThat(wr.getContent(contextBatchUrl("general", "js")), matches("content of legacy.js"));
        assertThat(wr.getContent(contextBatchUrl("general", "js")), not(matches("content of a1")));
    }

    @Test
    public void shouldNotAllowToRequireModuleAsWebResource()
    {
        wr.configure()
            .plugin("plugin")
                .modules("js")
                    .module("dashboard.js")
        .end();

        wr.requireResource("dashboard");

        assertEquals(wr.paths().size(), 0);
    }

    @Test
    public void shouldNotAllowToRequireWebResourceAsModule()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js")
        .end();

        wr.requireModule("plugin:a");

        assertEquals(wr.paths().size(), 0);
    }

    @Test
    public void shouldOutputModuleUrlsForLegacyCaseWhithContextBatchDegeneratedToWebResourceBatches()
    {
        shouldOutputModuleUrlsForLegacyCaseWhithContextBatchDegeneratedToWebResourceBatches(config -> {});

        assertThat(wr.paths(), matches(
            webResourceBatchUrl("lib", "js", buildMap("always-true", "true", "location", "true")),
            webResourceBatchUrl("dashboard", "js", buildMap("always-true", "true", "location", "true")),
            contextResourceUrl("general")
        ));
    }

    @Test
    public void shouldOutputModuleUrlsForLegacyCaseWhithContextBatchDegeneratedToWebResourceBatchesAndWebResourceBatchesDisabled()
    {
        shouldOutputModuleUrlsForLegacyCaseWhithContextBatchDegeneratedToWebResourceBatches(config -> {
            doReturn(false).when(config).isWebResourceBatchingEnabled();
        });

        assertThat(wr.paths(), matches(
            resourceUrl("lib", "lib.js", buildMap("always-true", "true", "location", "true")),
            resourceUrl("dashboard", "dashboard.js", buildMap("always-true", "true", "location", "true")),
            contextResourceUrl("general")
        ));
    }

    @Test
    public void shouldWorkWithNonExistingContextInLegacyUnbatchedCaseWithoutThrowingError()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .context("general")
                    // Triggering legacy behavior.
                    .condition(AlwaysTrueLegacyCondition.class)
                    .resource("a1.js")
        .end();

        wr.requireContext("general");
        wr.requireContext("non-existing-context");

        assertThat(wr.paths(), matches(
            webResourceBatchUrl("plugin:a", "js")
        ));
    }

    private void shouldOutputModuleUrlsForLegacyCaseWhithContextBatchDegeneratedToWebResourceBatches(WebResource.ConfigCallback configCallback)
    {
        wr.configure(configCallback)
            .plugin("plugin")
                .transformer("AddLocation", AddLocation.class)
                .webResource("a")
                    .context("general")
                    .resource("a1.js")
                .modules("js")
                    .transformation("js", "AddLocation")
                    .condition(AlwaysTrueCondition.class)
                    .context("dashboard", "general")
                    .module("lib.js")
                    .module("dashboard.js")
                        .dependency("./lib")
        .end();

        // Triggering legacy behaviour.
        wr.exclude("plugin:a");

        wr.requireContext("general");
    }

    @Test
    public void shouldInjectCssModulesInContext()
    {
        wr.configure()
            .plugin("plugin")
                .modules("css")
                    .context("dashboard.css", "general")
                    .module("dashboard.css")
        .end();

        wr.requireContext("general");

        assertThat(wr.paths(), matches(
            contextBatchUrl("general", "css")
        ));

        assertThat(wr.getContent(), matches(
            "/* module = 'dashboard.css' */",
            "content of module dashboard.css"
        ));
    }

    @Test
    public void shouldRequireCssModuleExplicitly()
    {
        wr.configure()
            .plugin("plugin")
                .modules("css")
                    .module("dashboard.css")
        .end();

        wr.requireModule("css!dashboard");

        assertThat(wr.paths(), matches(
            webResourceBatchUrl("dashboard.css", "css")
        ));

        assertThat(wr.getContent(), matches(
            "/* module = 'dashboard.css' */",
            "content of module dashboard.css"
        ));
    }

    @Test
    public void shouldServerLessWithBatchingDisabled()
    {
        wr.configure(config -> {
            doReturn(false).when(config).isContextBatchingEnabled();
            doReturn(false).when(config).isWebResourceBatchingEnabled();
        })
            .plugin("plugin")
                .modules("css")
                    .module("dashboard.less")
        .end();

        wr.requireModule("less!dashboard");

        assertThat(wr.paths(), matches(
            resourceUrl("dashboard.less", "dashboard.less")
        ));
        assertThat(wr.getContent(), matches(
            "content of module dashboard.less"
        ));
    }

    @Test
    public void shouldServeSoy()
    {
        wr.configure()
            .plugin("plugin")
                .modules("templates")
                    .module("dashboard.js")
                    .module("dashboard.soy")
                        .soyNamespace("Templates.dashboard")
        .end();

        assertThat(wr.getContent(webResourceBatchUrl("dashboard.soy", "js")), matches(
            "define(\"dashboard.soy\", function(){ return Templates.dashboard; });",
            "content of module dashboard.soy"
        ));
    }

    @Test
    public void shouldServerSoyWithBatchingDisabled()
    {
        wr.configure(config -> {
            doReturn(false).when(config).isContextBatchingEnabled();
            doReturn(false).when(config).isWebResourceBatchingEnabled();
        })
            .plugin("plugin")
                .modules("templates")
                    .module("dashboard.soy")
        .end();

        wr.requireModule("soy!dashboard");

        assertThat(wr.paths(), matches(
            resourceUrl("dashboard.soy", "dashboard.soy")
        ));
        assertThat(wr.getContent(), matches(
            "content of module dashboard.soy"
        ));
        assertThat(wr.get(resourceUrl("dashboard.soy", "dashboard.soy")).getHeaders().get("Content-Type"), equalTo("application/javascript"));
    }

    @Test
    public void shouldResolveSoyDependencies()
    {
        wr.configure()
            .plugin("plugin")
                .modules("templates")
                    .context("dashboard.soy", "general")
                    .module("lib.soy")
                        .soyTemplate("Templates.renderButton")
                    .module("dashboard.soy")
                        .soyDependency("Templates.renderButton")
        .end();

        wr.requireContext("general");

        assertThat(wr.getContent(wr.paths()), matches(
            "content of module lib.soy",
            "content of module dashboard.soy"
        ));
    }

    @Test
    public void shouldUseDefaultTransformers()
    {
        List<TransformationDto> defaultTransformations = new ArrayList<>();
        List<TransformerDto> jsTransformers = new ArrayList<>();
        jsTransformers.add(new TransformerDto("AddLocation"));
        defaultTransformations.add(new TransformationDto("js", jsTransformers));

        Map<String, Class> defaultTransformerClasses = new HashMap<>();
        defaultTransformerClasses.put("AddLocation", AddLocation.class);

        wr.configure()
            .setDefaultAmdModuleTransformers(defaultTransformations, defaultTransformerClasses)
            .plugin("plugin")
                .modules("js")
                    .context("dashboard", "general")
                    .module("dashboard.js")
        .end();

        wr.requireContext("general");

        assertThat(wr.paths(), matches(
            contextBatchUrl("general", "js", buildMap("location", "true"))
        ));

        assertThat(wr.getContent(), matches(
            "location=plugin/dashboard.js (transformer)",
            "content of module dashboard"
        ));
    }

    @Test
    public void shouldResolveSoyWithSameNameAsJs()
    {
        wr.configure()
            .plugin("plugin")
                .modules("js")
                    .module("widget.js")
                        .dependency("soy!widget")
                    .module("widget.soy")
        .end();

        wr.requireModule("widget");

        assertThat(wr.paths(), matches(
            webResourceBatchUrl("widget.soy", "js"),
            webResourceBatchUrl("widget", "js")
        ));
    }

    @Test
    public void shouldAllowToUseModuleInSuperbatchDeclaration()
    {
        wr.configure()
            .addToSuperbatch("widget")
            .plugin("plugin")
                .modules("js")
                    .module("widget.js")
                        .dependency("soy!widget")
                    .module("widget.soy")
                    .module("app.js")
        .end();

        wr.requireModule("app");

        assertThat(wr.paths(), matches(
            superBatchUrl("js"),
            webResourceBatchUrl("app", "js")
        ));
        assertThat(wr.getContent(superBatchUrl("js")), matches(
            "content of module widget.soy",
            "content of module widget.js"
        ));
    }

    @Test
    public void shouldResolveModuleDependencyIfRequiredAsWebResource()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .moduleDependency("b")
                .modules("js")
                    .module("b.js")
        .end();

        wr.requireResource("plugin:a");

        List<String> paths = wr.paths();
        System.out.println(paths);
    }
}