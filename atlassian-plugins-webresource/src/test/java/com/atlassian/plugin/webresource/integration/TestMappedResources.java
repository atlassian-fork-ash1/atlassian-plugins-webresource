package com.atlassian.plugin.webresource.integration;

import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.plugin.webresource.prebake.PrebakeConfig;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.junit.Assert.assertThat;

public class TestMappedResources extends TestCase {
    @Rule public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule public TemporaryFolder tmp = new TemporaryFolder();

    @Test
    public void shouldReturnMappedResourceInsteadOfOriginal() throws IOException {
        String originalResource = "/s/d41d8cd98f00b204e9800998ecf8427e-CDN/stat&/system-build-number/system-build-counter/d678cc3cdcf41ef6e31020ede28783eb/_/download" + contextBatchUrl("general", "js");
        String mappedResource = "hash-mapped-resource.js";
        final File mappingFile = tmp.newFile();
        try (OutputStream out = new FileOutputStream(mappingFile)) {
            out.write(("{ \"" + originalResource + "\" : [\"" + mappedResource + "\"] }").getBytes(StandardCharsets.UTF_8));
        }
        wr.configure()
                .setIsCtCdnMappingEnabled(true)
                .setCdnStrategy(createCdnStrategy(originalResource, mappedResource))
                .setCtCdnBaseUrl("/my.cdn.com/")
                .plugin("plugin")
                .webResource("a")
                .context("general")
                .resource("a1.js")
                .end();

        wr.requireContext("general");

        List<String> paths = wr.paths();

        assertThat(paths, matches(mappedResource));
    }

    private CDNStrategy createCdnStrategy(final String originalUrl, final String mappedUrl) throws IOException {
        final File mappingFile = tmp.newFile();
        try (OutputStream out = new FileOutputStream(mappingFile)) {
            out.write(("{ \"" + originalUrl + "\" : [\"" + mappedUrl + "\"] }").getBytes(StandardCharsets.UTF_8));
        }
        return new CDNStrategy() {
            @Override
            public boolean supportsCdn() {
                return true;
            }

            @Override
            public String transformRelativeUrl(String url) {
                return "/prefix" + url;
            }

            @Override
            public Optional<PrebakeConfig> getPrebakeConfig() {
                return Optional.of(PrebakeConfig.forPattern(mappingFile.getAbsolutePath()));
            }
        };
    }
}
