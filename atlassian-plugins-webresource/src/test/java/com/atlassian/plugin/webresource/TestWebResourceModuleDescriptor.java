package com.atlassian.plugin.webresource;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestWebResourceModuleDescriptor {
    private static final String TEST_PLUGIN_KEY = "atlassian.test.plugin";

    private WebResourceModuleDescriptor descriptor;
    private Plugin mockPlugin;

    @Before
    public void setUp() throws Exception {
        descriptor = new WebResourceModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, new DefaultHostContainer());
        mockPlugin = mock(Plugin.class);
        when(mockPlugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
    }

    @Test
    public void testInitWithDependencies() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "<dependency>atlassian.test.plugin:jquery</dependency>\n" +
                "<dependency>atlassian.test.plugin:ajs</dependency>\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        List<String> dependencies = descriptor.getDependencies();
        assertEquals(2, dependencies.size());
        assertEquals("atlassian.test.plugin:jquery", dependencies.get(0));
        assertEquals("atlassian.test.plugin:ajs", dependencies.get(1));
    }

    @Test
    public void testInitWithContexts() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "<context>C1</context>\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        Set<String> contexts = descriptor.getContexts();
        assertEquals(2, contexts.size());
        assertTrue(contexts.contains("atlassian.test.plugin:test-resources")); // implicit context
        assertTrue(contexts.contains("C1"));

        assertEquals(0, descriptor.getDependencies().size());
    }

        @Test
    public void testInitWithRootPage() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "  <root-page/>\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        assertTrue(descriptor.isRootPage());
    }

    @Test
    public void testInitWithoutRootPage() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        assertFalse(descriptor.isRootPage());
    }

    @Test
    public void testInitWithContextDependencies() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "  <dependencies>\n" +
                "      <context>C1</context>\n" +
                "      <context>C2</context>\n" +
                "  </dependencies>\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        Set<String> expected = new TreeSet<>();
        expected.add("C1");
        expected.add("C2");
        assertEquals(expected, descriptor.getContextDependencies());
        assertTrue(descriptor.getDependencies().isEmpty());
        assertTrue(descriptor.getModuleDependencies().isEmpty());
        assertTrue(descriptor.getAsyncDependencies().isEmpty());
        assertTrue(descriptor.getAsyncContextDependencies().isEmpty());
    }

    @Test
    public void testInitWithWebResourceDependenciesInBlock() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "  <dependencies>\n" +
                "      <web-resource>atlassian.test.plugin:ajs</web-resource>\n" +
                "      <web-resource>atlassian.test.plugin.jquery</web-resource>\n" +
                "  </dependencies>\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        List<String> expected = asList("atlassian.test.plugin:ajs", "atlassian.test.plugin.jquery");
        assertEquals(expected, descriptor.getDependencies());
        assertTrue(descriptor.getModuleDependencies().isEmpty());
        assertTrue(descriptor.getContextDependencies().isEmpty());
        assertTrue(descriptor.getAsyncDependencies().isEmpty());
        assertTrue(descriptor.getAsyncContextDependencies().isEmpty());
    }

    @Test
    public void testInitWithModuleDependenciesInBlock() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "  <dependencies>\n" +
                "      <web-module>test/module</web-module>\n" +
                "      <web-module>another/module</web-module>\n" +
                "  </dependencies>\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        List<String> expected = asList("test/module", "another/module");
        assertEquals(expected, descriptor.getModuleDependencies());
        assertTrue(descriptor.getDependencies().isEmpty());
        assertTrue(descriptor.getContextDependencies().isEmpty());
        assertTrue(descriptor.getAsyncDependencies().isEmpty());
        assertTrue(descriptor.getAsyncContextDependencies().isEmpty());
    }

    @Test
    public void testInitWithAsyncDependencies() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "  <async-dependencies>\n" +
                "      <web-resource>atlassian.test.plugin:ajs</web-resource>\n" +
                "      <web-resource>atlassian.test.plugin:jquery</web-resource>\n" +
                "  </async-dependencies>\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        Set<String> expectedWebResources = new TreeSet<>();
        expectedWebResources.add("atlassian.test.plugin:ajs");
        expectedWebResources.add("atlassian.test.plugin:jquery");

        assertEquals(expectedWebResources, descriptor.getAsyncDependencies());
        assertTrue(descriptor.getDependencies().isEmpty());
        assertTrue(descriptor.getContextDependencies().isEmpty());
        assertTrue(descriptor.getModuleDependencies().isEmpty());
        assertTrue(descriptor.getAsyncContextDependencies().isEmpty());
    }

    @Test
    public void testInitWithAsyncContextDependencies() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "  <async-dependencies>\n" +
                "      <context>test.context</context>\n" +
                "      <context>another.context</context>\n" +
                "  </async-dependencies>\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        Set<String> expectedContexts = new TreeSet<>();
        expectedContexts.add("test.context");
        expectedContexts.add("another.context");

        assertEquals(expectedContexts, descriptor.getAsyncContextDependencies());
        assertTrue(descriptor.getDependencies().isEmpty());
        assertTrue(descriptor.getModuleDependencies().isEmpty());
        assertTrue(descriptor.getContextDependencies().isEmpty());
        assertTrue(descriptor.getAsyncDependencies().isEmpty());
    }

    /**
     * This test can be removed once we remove support for declarations outside the <dependencies> container
     */
    @Test
    public void testInitWithMixedLegacyAndCurrentResourceDependencies() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "  <dependency>legacy.declared.resource</dependency>\n" +
                "  <dependency>another.legacy.resource</dependency>\n" +
                "  <dependencies>\n" +
                "      <web-resource>test.resource</web-resource>\n" +
                "      <web-resource>another.resource</web-resource>\n" +
                "  </dependencies>\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        List<String> expectedResources = asList(
                "legacy.declared.resource",
                "another.legacy.resource",
                "test.resource",
                "another.resource"
        );

        assertEquals(expectedResources, descriptor.getDependencies());
        assertTrue(descriptor.getModuleDependencies().isEmpty());
        assertTrue(descriptor.getContextDependencies().isEmpty());
        assertTrue(descriptor.getAsyncDependencies().isEmpty());
        assertTrue(descriptor.getAsyncContextDependencies().isEmpty());
    }

    /**
     * This test can be removed once we remove support for declarations outside the <dependencies> container
     */
    @Test
    public void testInitWithMixedLegacyAndCurrentModuleDependencies() throws Exception {
        String xml = "<web-resource key=\"test-resources\">\n" +
                "  <module-dependency>legacy.declared.module</module-dependency>\n" +
                "  <module-dependency>another.legacy.module</module-dependency>\n" +
                "  <dependencies>\n" +
                "      <web-module>test.module</web-module>\n" +
                "      <web-module>another.module</web-module>\n" +
                "  </dependencies>\n" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        List<String> expectedResources = asList(
                "legacy.declared.module",
                "another.legacy.module",
                "test.module",
                "another.module"
        );

        assertEquals(expectedResources, descriptor.getModuleDependencies());
        assertTrue(descriptor.getDependencies().isEmpty());
        assertTrue(descriptor.getContextDependencies().isEmpty());
        assertTrue(descriptor.getAsyncDependencies().isEmpty());
        assertTrue(descriptor.getAsyncContextDependencies().isEmpty());
    }

    @Test
    public void testInitWithDeprecationWarning() throws Exception {
        String xml = "<web-resource key=\"test-deprecated-resource\">\n" +
                "  <deprecated />" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        assertThat(descriptor.isDeprecated(), is(true));
    }

    @Test
    public void testInitWithDeprecationVersions() throws Exception {
        String xml = "<web-resource key=\"test-deprecated-resource\">\n" +
                "  <deprecated since=\"5.9.0\" remove=\"8.0.0\" />" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        assertThat(descriptor.isDeprecated(), is(true));
        assertThat(descriptor.getDeprecation().getSinceVersion(), is("5.9.0"));
        assertThat(descriptor.getDeprecation().getRemoveInVersion(), is("8.0.0"));
    }

    @Test
    public void testInitWithDeprecationInfo() throws Exception {
        String xml = "<web-resource key=\"test-deprecated-resource\">\n" +
                "  <deprecated alternative=\"com.some.other.plugin:web-resource-key\">" +
                "    This is only a test resource." +
                "    Why    are you depending on it?" +
                "  </deprecated>" +
                "</web-resource>";

        initDescriptorFromXml(xml);

        assertThat(descriptor.isDeprecated(), is(true));
        assertThat(descriptor.getDeprecation().getAlternative(), is("com.some.other.plugin:web-resource-key"));
        assertThat(descriptor.getDeprecation().getExtraInfo(), is("This is only a test resource. Why are you depending on it?"));
    }

    private void initDescriptorFromXml(String xml) throws DocumentException {
        descriptor.init(mockPlugin, DocumentHelper.parseText(xml).getRootElement());
    }
}
