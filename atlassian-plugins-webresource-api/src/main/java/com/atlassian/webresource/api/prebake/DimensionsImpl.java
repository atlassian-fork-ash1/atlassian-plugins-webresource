package com.atlassian.webresource.api.prebake;

import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Arrays.asList;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static java.util.Collections.unmodifiableSet;

/**
 */
class DimensionsImpl implements Dimensions {
    /**
     * sorted map for easier debugging
     */
    private final SortedMap<String, Set<Optional<String>>> queryParams = new TreeMap<>();

    private DimensionsImpl() {
    }

    private DimensionsImpl(SortedMap<String, Set<Optional<String>>> a, SortedMap<String, Set<Optional<String>>> b) {
        queryParams.putAll(a);
        for (Map.Entry<String, Set<Optional<String>>> entry : b.entrySet()) {
            String key = entry.getKey();
            if (!queryParams.containsKey(key)) {
                this.queryParams.put(key, new LinkedHashSet<>());
            }
            this.queryParams.get(key).addAll(entry.getValue());
        }
    }

    private DimensionsImpl(SortedMap<String, Set<Optional<String>>> orig, String key, Collection<Optional<String>> more) {
        queryParams.putAll(orig);
        if (!queryParams.containsKey(key)) {
            this.queryParams.put(key, new LinkedHashSet<>());
        }
        this.queryParams.get(key).addAll(more);
    }

    public static Dimensions empty() {
        return new DimensionsImpl();
    }

    @Override
    public Dimensions andExactly(String key, String... values) {
        return andExactly(key, asList(values));
    }

    @Override
    public Dimensions andExactly(String key, Collection<String> values) {
        List<Optional<String>> optionalValues = values.stream().map(Optional::ofNullable).collect(Collectors.toList());
        return new DimensionsImpl(queryParams, key, optionalValues);
    }

    @Override
    public Dimensions andAbsent(String key) {
        return new DimensionsImpl(queryParams, key, singleton(Optional.empty()));
    }

    @Override
    public Dimensions product(Dimensions rhs) {
        return new DimensionsImpl(queryParams, ((DimensionsImpl) rhs).queryParams);
    }

    @Override
    @Nonnull
    public Dimensions whitelistValues(@Nonnull Dimensions whitelist) {
        checkNotNull(whitelist, "whitelist cannot be null");
        return filterValues(whitelist, true);
    }

    @Override
    @Nonnull
    public Dimensions blacklistValues(@Nonnull Dimensions blacklist) {
        checkNotNull(blacklist, "blacklist cannot be null");
        return filterValues(blacklist, false);
    }

    private Dimensions filterValues(Dimensions filter, boolean allow) {
        DimensionsImpl filterImpl = (DimensionsImpl) filter;
        if (filterImpl.queryParams.isEmpty()) {
            return this;
        }

        DimensionsImpl dims = new DimensionsImpl();

        for (Map.Entry<String, Set<Optional<String>>> e : queryParams.entrySet()) {
            String key = e.getKey();
            Set<Optional<String>> values = e.getValue();

            if (filterImpl.contains(key)) {
                Set<Optional<String>> filterValues = filterImpl.getValues(key);
                values = values.stream()
                        .filter(v -> filterValues.contains(v) == allow)
                        .collect(Collectors.toSet());
            }
            dims = dims.addAll(key, values);
        }

        return dims;
    }

    private DimensionsImpl addAll(String key, Set<Optional<String>> values) {
        return new DimensionsImpl(queryParams, key, values);
    }

    private boolean contains(String key) {
        return queryParams.containsKey(key);
    }

    private Set<Optional<String>> getValues(String key) {
        if (!queryParams.containsKey(key)) {
            return emptySet();
        }

        return unmodifiableSet(queryParams.get(key));
    }

    @Override
    public Stream<Coordinate> cartesianProduct() {
        List<Set<QueryParam>> cartesianInput = new ArrayList<>();
        for (Map.Entry<String, Set<Optional<String>>> entry : queryParams.entrySet()) {
            Set<QueryParam> axis = new LinkedHashSet<>();
            for (Optional<String> value : entry.getValue()) {
                axis.add(new QueryParam(entry.getKey(), value));
            }
            cartesianInput.add(axis);
        }

        Set<List<QueryParam>> cartesianProduct = Sets.cartesianProduct(cartesianInput);
        return cartesianProduct.stream().map(CoordinateImpl::new);
    }

    @Override
    public long cartesianProductSize() {
        if (queryParams.isEmpty()) {
            return 0;
        }

        return queryParams.values().stream()
                .map(Collection::size)
                .reduce(1, (a, b) -> a * b);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DimensionsImpl that = (DimensionsImpl) o;

        return queryParams.equals(that.queryParams);

    }

    @Override
    public int hashCode() {
        return queryParams.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("Dimensions: {\n");
        for (Map.Entry<String, Set<Optional<String>>> e : queryParams.entrySet()) {
            builder.append(e.getKey());
            builder.append(" = ");
            builder.append(e.getValue());
            builder.append(",\n");

        }
        builder.append("}");

        return builder.toString();
    }


}
