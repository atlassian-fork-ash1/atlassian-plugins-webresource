package com.atlassian.webresource.api.prebake;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represents all the possible "dimensions" or set of possible combinations of query parameters
 * that a transformer or condition can support. For example, an i18n transformer may support a
 * "locale" query param that can have values "en", "fr", etc. A condition may support a query
 * param "isadmin" that may be "true" or absent. Both those Dimensions can be combined
 * via {@link #product(Dimensions)} to produce all possible valid combinations of "locale" and "isadmin".
 * <p>
 * For example:
 * </p>
 * <pre>{@code
 * Dimensions locale = Dimensions.empty().andExactly("locale", "en", "fr);
 * Dimensions isadmin = Dimensions.empty().andExactly("isadmin", "true").andAbsent("isadmin");
 * Dimensions dims = locale.product(isadmin);
 * }</pre>
 * <p>
 * This Dimensions concept does not support a query param having multiple values simultaneously. For example,
 * you could not construct a Dimensions that represented <code>?locale=en&amp;locale=fr</code>
 * </p>
 * <p>
 * Instances of this class are immutable, and all methods return results and do not modify the current instance.
 * </p>
 * <p>
 * Implementors of this interface will define equals/hashCode.
 * </p>
 */
public interface Dimensions {

    /**
     * Factory for an empty dimension object.
     */
    static Dimensions empty() {
        return DimensionsImpl.empty();
    }

    /**
     * Initialises a new {@link Dimensions} instance from a {@link Map}. Keys correspond to the names of the
     * dimension, and values to the list of values that the dimension can assume. A {@code null} value will be
     * interpreted as an absent dimension value (see {@link Dimensions#andAbsent(String)}).
     *
     * @param dimensionMap A {@link Map} containing the dimension values
     * @return {@link Dimensions} object populated with the data in the map passed as parameter
     */
    @Nonnull
    static Dimensions fromMap(@Nonnull Map<String, List<String>> dimensionMap) {
        checkNotNull(dimensionMap);

        Dimensions dims = Dimensions.empty();
        for (Map.Entry<String, List<String>> e : dimensionMap.entrySet()) {
            String key = e.getKey();
            List<String> values = e.getValue();

            if (values == null || values.isEmpty()) {
                continue;
            }

            dims = dims.andExactly(key, values);
        }

        return dims;
    }

    /**
     * Adds dimensions for query param {@code key} over all the given {@code values}.
     * Does not include the absence of {@code key}.
     */
    Dimensions andExactly(String key, String... values);

    /**
     * Adds dimensions for query param {@code key} over all the given {@code values}.
     * Does not include the absence of {@code key}.
     */
    Dimensions andExactly(String key, Collection<String> values);

    /**
     * Adds a dimension that represents the absence of {@code key} in the query string.
     */
    Dimensions andAbsent(String key);

    /**
     * Combines two dimensions, representing all possible combinations of both dimensions.
     */
    Dimensions product(Dimensions rhs);

    /**
     * <p>
     * Filters the dimension values using the whitelist passed as parameter.
     * </p>
     * <p> This method produces a new {@link Dimensions} object that includes, for the dimension keys contained in the
     * whitelist, only the values explicitly included in the whitelist. All dimensions that don't have a
     * corresponding entry in the whitelist object will be kept as-is.</p>
     */
    @Nonnull
    Dimensions whitelistValues(@Nonnull Dimensions whitelist);

    /**
     * <p>
     * Filters the dimension values using the blacklist passed as parameter.
     * </p>
     * <p> This method produces a new {@link Dimensions} object that includes, for the dimension keys contained in the
     * blacklist, only the values that are not contained in the blacklist. All dimensions that don't have a
     * corresponding entry in the blacklist object will be kept as-is.</p>
     */
    @Nonnull
    Dimensions blacklistValues(@Nonnull Dimensions blacklist);

    /**
     * Computes the cartesian product as an unordered stream of Coordinates.
     * A Coordinate represents a single possible query-string combination.
     */
    Stream<Coordinate> cartesianProduct();

    /**
     * Computes the size of the cartesian product.
     *
     * <p> Use this method instead of iterating over the list of coordinates, or collecting the coordinates in a
     * list, as both these solutions might take a considerable amount of time and memory (their time and memory
     * footprint is more than 2^n, where n is the number of dimensions).</p>
     */
    long cartesianProductSize();

}
