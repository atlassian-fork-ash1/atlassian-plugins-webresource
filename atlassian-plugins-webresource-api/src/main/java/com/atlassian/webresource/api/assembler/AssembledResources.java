package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;

import java.util.concurrent.CompletionStage;

/**
 * Interface for clients to include web resources that have been required via {@link RequiredResources}.
 * <p>
 * Note, {@link WebResourceAssembler} is currently an experimental API and may break between versions of web resources.
 * It is advised that you use WebResourceManager in the mean time if you require a stable API.
 *
 * @see WebResourceAssembler
 * @see PageBuilderService
 * @since v3.0
 */
@ExperimentalApi
public interface AssembledResources {
    /**
     * Calculates dependencies, returns requested resources, then clears the list of webresources + contexts that have
     * been included since the last call to {@link #drainIncludedResources()}.
     * <p>
     * This method ensures that no individual webresource will be emitted twice, either as part of a superbatch,
     * context batch or individual include.
     * <p>
     * The order of resources in the returned list is as follows:
     * <ol>
     * <li>
     * If this is the first call and superbatch resources were requested in the factory that constructed this
     * request, superbatch resources are included
     * </li>
     * <li>
     * Then all required contexts are included
     * </li>
     * <li>
     * Then required webresources are included
     * </li>
     * </ol>.
     * This method *does not block* on any data promises added via {@link RequiredData#requireData(String, CompletionStage)}
     * but it will return any promises that are already complete.
     * The {@link WebResourceSet#isComplete()} property on the returned object can be inspected to see if
     * (at the time <code>pollIncludedResources()</code> was called) there are any incomplete promises.
     *
     * @return list of plugin resources to emit.
     */
    public WebResourceSet drainIncludedResources();

    /**
     * Similar to {@link #drainIncludedResources()}, but will block until there is at least one complete promise to
     * return. If no promises have been added, or they are all complete, then this method returns without blocking.
     * <p>
     * The following code demonstrates the expected pattern to drain all remaining resources including waiting
     * for promises. This pattern ensures that complete promises are output as soon as possible.
     * </p>
     * <pre>{@code
     * WebResourceSet resources;
     * do {
     *     resources = assembler.assembled().pollIncludedResources();
     *     resources.writeHtmlTags(out, UrlMode.AUTO);
     *     out.flush();
     * } while (!resources.isComplete());
     * }</pre>
     */
    public WebResourceSet pollIncludedResources();

    /**
     * Returns the currently requested resources as per {@link #drainIncludedResources()}, however does not clear
     * the internal list. Intended to be used to debugging or introspecting the current state.
     * This does not block on any promises, and importantly does not return any undrained complete promises.
     */
    public WebResourceSet peek();
}
