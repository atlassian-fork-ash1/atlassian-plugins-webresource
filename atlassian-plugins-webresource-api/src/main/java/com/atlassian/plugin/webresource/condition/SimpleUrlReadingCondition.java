package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.DimensionAwareUrlReadingCondition;
import com.atlassian.webresource.api.prebake.Dimensions;

import java.util.Map;

/**
 * Simple implementation for {@link com.atlassian.webresource.api.prebake.DimensionAwareUrlReadingCondition} implementations.
 * <p>
 * Handles encoding a single boolean into the url. The convention here is that if the condition is true, it is added
 * the querystring; if false it is not added to the querystring.
 *
 * @since v3.1.0
 */
public abstract class SimpleUrlReadingCondition implements DimensionAwareUrlReadingCondition {
    private static final String TRUE = String.valueOf(true);

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
        // Default implementation is a no-op
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder) {
        if (isConditionTrue()) {
            urlBuilder.addToQueryString(queryKey(), TRUE);
        }
    }

    @Override
    public Dimensions computeDimensions() {
        String key = queryKey();
        return Dimensions.empty()
                .andExactly(key, TRUE)
                .andAbsent(key);

    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder, Coordinate coord) {
        coord.copyTo(urlBuilder, queryKey());
    }

    @Override
    public boolean shouldDisplay(QueryParams params) {
        return Boolean.valueOf(params.get(queryKey()));
    }

    /**
     * @return true if this condition passes. This is evaluated at url generation time.
     */
    protected abstract boolean isConditionTrue();

    /**
     * @return the key to append to the query string
     */
    protected abstract String queryKey();
}
