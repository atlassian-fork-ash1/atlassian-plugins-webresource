package com.atlassian.plugin.webresource;

import java.util.Map;

/**
 * Provides access to querystring params.
 *
 * @since v3.0
 */
public class QueryParams {
    private final Map<String, String> map;

    public static QueryParams of(Map<String, String> map) {
        return new QueryParams(map);
    }

    private QueryParams(Map<String, String> map) {
        this.map = map;
    }

    public String get(String key) {
        return map.get(key);
    }

    @Override
    public String toString() {
        return "QueryParams{" +
                "map=" + map +
                '}';
    }
}
