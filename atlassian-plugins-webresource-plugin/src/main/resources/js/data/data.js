;(function() {
    function log(msg) {
        console && console.log && console.log(msg); // eslint-disable-line no-console
    }

    /**
     * A constructor for the internal implementation of WRM.data. Can be instantiated
     * many times for testing, but the global instance is at WRM.data
     * @param globalHolder the object in which to find _unparsedData, etc. (typically window.WRM)
     * @return the data api object
     */
    WRM._createDataImpl = function(globalHolder) {
        globalHolder._unparsedData = globalHolder._unparsedData || {};
        globalHolder._unparsedErrors = globalHolder._unparsedErrors || {};
        globalHolder._claimedData = globalHolder._claimedData || {};

        function parseJsonData(unparsedData) {
            if (!unparsedData) {
                return unparsedData;
            }
            return JSON.parse(unparsedData);
        }

        // This is extremely defensive, but we use Object.prototype.hasOwnProperty as developers could include
        // a piece of data with key "hasOwnProperty"
        function _hasOwnProperty(o, key) {
            return Object.prototype.hasOwnProperty.call(o, key);
        }

        var successCallbacks = {};
        var failureCallbacks = {};

        function _addCallback(key, cb, callbacks) {
            if (!cb) {
                return; // they didn't supply a callback
            }
            if (_hasOwnProperty(callbacks, key)) {
                return; // ignore duplicate claims
            }
            callbacks[key] = cb;
        }

        function _extractData(key, allUnparsed, allClaimed) {
            var unparsedData = allUnparsed[key];
            try {
                var data = parseJsonData(unparsedData);
                delete allUnparsed[key]; // purge from unparsed data map
                allClaimed[key] = true; // mark data as claimed
                return data;
            }
            catch (e) {
                log("JSON Error parsing data with key " + key + ": " + e);
                return undefined;
            }
        }

        function _callbackLater(callback, data, key) {
            var f = function() {
                try {
                    callback(data);
                } catch (e) {
                    log("Excpetion calling bigpipe callback for " + key + ": " + e);
                }
            };
            setTimeout(f, 0);
        }

        /**
         * Tries to find any callbacks for data in `unparsed`. If we find a callback for a key,
         * but the value cannot be parsed, then we try and call a callback in `fallbackCallbacks`.
         *
         * @param unparsed Object that contains unparsed key-value pairs
         * @param callbacks map of key->callback
         * @param fallbackCallbacks map of key->callback
         * @private
         */
        function _findAndCallCallbacks(unparsed, claimed, callbacks, fallbackCallbacks) {
            for (var k in unparsed) {
                if (_hasOwnProperty(unparsed, k)) {
                    if (_hasOwnProperty(callbacks, k)) {
                        var callback = callbacks[k];
                        var data = _extractData(k, unparsed, claimed);
                        if (typeof data === "undefined") { // parse error? send it as an error
                            callback = fallbackCallbacks[k]
                        }
                        delete callbacks[k]; // you only get it once
                        delete fallbackCallbacks[k]; // you only get it once
                        if (callback) {
                            _callbackLater(callback, data, k);
                        }
                    }
                }
            }
        }

        /**
         * internal method, exposed so can be called from script tags output by the WRM
         */
        globalHolder._dataArrived = function() {
            _findAndCallCallbacks(globalHolder._unparsedData, globalHolder._claimedData, successCallbacks, failureCallbacks);
            _findAndCallCallbacks(globalHolder._unparsedData, globalHolder._claimedData, failureCallbacks, failureCallbacks);
        };


        var api = {};
        /**
         * Called when the promise completed normally
         * @callback successCallback
         * @param {*} JSON data
         */
        /**
         * Called when the promise completed excpetionally or timed out. This callback takes no arguments.
         * @callback failureCallback
         */

        /**
         * Claims the data returned from the server under the given key.
         *
         * Data for a specific key is retrieved only once. Subsequent requests for the same key will return no data.
         * If the data is required by multiple callers, the suggested pattern here is to wrap an API eg
         * WRM.contextPath around the WRM.data.claim method and cache inside that external API - see context-path.js
         * in this directory for an example.
         *
         * This method has two forms: claim(key) that is synchronous, and claim(key, successcb, failurecb) that is
         * asynchronous.
         *
         * For synchronous callers:
         * Callers must ensure that any JS requesting data appears *after* the data is inserted into the page - this
         * is achieved via the caller expressing a web-resource <dependency> on the resource serving the data.
         *
         * For asynchronous callers:
         * Either the successcb or failurecb callbacks will be called. If the data is already known (either success,
         * or failure) then a callback will be invoked presently (on a timeout(0)).
         *
         * @param {string} key data key.
         * @param {successCallback} [successcb]
         * @param {failureCallback} [failurecb]
         * @returns {*|undefined} JSON data in the synchronous version, undefined in the async version.
         */
        api.claim = function(key, successcb, failurecb) {
            if (!successcb && !failurecb) {
                if (_hasOwnProperty(globalHolder._claimedData, key)) {
                    console && console.error && console.error("Data with key " + key + " has already been claimed");
                }
                // Lazily parses JSON data from WRM._unparsedData
                if (globalHolder._unparsedData && _hasOwnProperty(globalHolder._unparsedData, key)) {
                    return _extractData(key, globalHolder._unparsedData, globalHolder._claimedData); // synchronous
                }
            } else { // async version
                _addCallback(key, successcb, successCallbacks);
                _addCallback(key, failurecb, failureCallbacks);
                globalHolder._dataArrived(); // we might already have that data
            }
        };

        return api;

    };

    // create the global WRM.data instance
    WRM.data = WRM._createDataImpl(WRM);

    if (typeof define === 'function') {
        define('wrm/data', [], function() {
            return WRM.data;
        });
    }

}());
