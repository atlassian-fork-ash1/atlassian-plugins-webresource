;(function() {
    var format = WRM.format;

    var I18n = {
        getText: function(key) {
            console.warn(
                'Call to "getText" function was not replaced with either raw translation or a call to the "format" function. Have you included the "jsI18n" transformation in your webresource configuration?'
            );

            if (arguments.length > 1) {
                return I18n.format.apply(null, arguments);
            }

            return key;
        },

        format: format
    };

    if (typeof define === 'function') {
        define('wrm/i18n', [], function() {
            return I18n;
        });
    }

    // Export to globals.
    WRM.I18n = I18n;
    // This used to be part of AUI, so preserve the legacy.
    AJS.I18n = I18n;
}());
