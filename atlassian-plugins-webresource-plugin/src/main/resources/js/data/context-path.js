;(function() {
    var contextPath = null;

    function getContextPath() {
        if (contextPath === null) {
            contextPath = WRM.data.claim('com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path.context-path');
        }
        return contextPath;
    }

    // TODO: hack, this only exists for unit testing. Should refactor into a factory here
    getContextPath._reset = function() {
        contextPath = null;
    };

    if (typeof define === 'function') {
        define('wrm/context-path', function() {
            return getContextPath;
        });
    }

    // Export a global variable
    WRM.contextPath = getContextPath;
    // This used to be part of AUI, so preserve the legacy.
    AJS.contextPath = getContextPath;
}());
