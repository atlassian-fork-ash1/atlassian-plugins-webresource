;(function() {
    WRM._InOrderLoaderFactory = function (document) {
        var RESOLVED_ATTR_NAME = 'data-resolved';

        // internals needed for triggering allResolvedCallbacks
        var allResolved = false;
        var callbackList = [];

        // The content of file modules are wrapped in a function. These are those functions. Executing them will execute
        // the content of the file.
        var definitions = {}; // string -> function

        return {
            define: function(definition) {
                var name = getScriptUri();
                definitions[name] = definition;
                resolve();
            },
            registerAllResolvedCallback: registerAllResolvedCallback
        };

        function polyfillGetScriptUri() {
            try {
                throw new Error();
            } catch (e) {
                var uri = WRM.InOrderLoader.getScriptUriFromStack(e.stack || "");
                return uri;
            }
        }

        function getScriptUri() {
            var uri = ("currentScript" in document)
                ? document.currentScript.src
                : polyfillGetScriptUri();
            var normalizedUri = WRM.InOrderLoader.normalizeUri(uri);
            return normalizedUri;
        }

        function resolve() {
            // This assumes that <script> tags are not removed from the DOM, which is already an assumption that WRM.require
            // makes, so we're not adding any more assumptions than exist today.
            var scripts = document.head.querySelectorAll("script[data-initially-rendered]");

            for (var i = 0; i < scripts.length; i++) {
                var script = scripts[i];
                if (script.hasAttribute(RESOLVED_ATTR_NAME)) {
                    continue;
                }

                var name = WRM.InOrderLoader.normalizeUri(script.src);
                var definition = definitions[name];

                if (typeof definition == "undefined") {
                    // As soon as we reach a module that's not defined yet, we can't proceed and have to wait for another
                    // module to be defined (which might be this one).
                    return;
                }

                // We're eager with claiming that a definition has been executed, and that's to guard against scripts
                // that have errors in them. Today's behaviour for errors in script tags is to continue executing
                // subsequent scripts. By claiming the definition has been executed _before_ we try executing it, we
                // maintain this behaviour.
                script.setAttribute(RESOLVED_ATTR_NAME, "");
                definition();
            }

            allResolved = true;
            invokeAllResolvedCallbacks();
        }

        function registerAllResolvedCallback(callback) {
            if (allResolved) {
                callback();
            } else {
                callbackList.push(callback);
            }
        }

        function invokeAllResolvedCallbacks() {
            callbackList && callbackList.forEach(function(callback) {
                callback();
            });
            callbackList = null;
        }
    };
    WRM.InOrderLoader = WRM._InOrderLoaderFactory(document);
    WRM.InOrderLoader.getScriptUriFromStack = function(stack) {
        // IE10, IE11 https://msdn.microsoft.com/en-us/library/windows/apps/hh699850.aspx
        var uriMatch = /https?:(\/\/.*?)(?::[0-9]+){0,2}\)?$/;
        var len = stack.length;
        // "len-2" to ignore empty line at the end (if any)
        var lastFrame = stack.substring(stack.lastIndexOf("\n", len - 2) + 1).trim();
        var matches = lastFrame.match(uriMatch);
        var uri = matches && matches[1];
        return uri || "";
    };
    WRM.InOrderLoader.normalizeUri = function(uri) {
        // get rid of protocol
        return uri.substring(uri.indexOf('/'));
    };
}());
