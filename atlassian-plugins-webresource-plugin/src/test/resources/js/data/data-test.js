module("data", {
    setup: function() {
        // local instantiation of WRM and WRM.data
        this.WRM = {};
        this.WRM.data = window.WRM._createDataImpl(this.WRM, this.WRM.data);
        this.clock = sinon.useFakeTimers();
        this.errorSpy = sinon.spy(console, "error");
        this.logSpy = sinon.spy(console, "log");
    },
    teardown: function() {
        this.clock.restore();
        this.errorSpy.restore();
        this.logSpy.restore();
    },
    addData: function(key, jsonValue) {
        if (!this.WRM._unparsedData) {
            this.WRM._unparsedData = {};
        }
        this.WRM._unparsedData[key] = jsonValue;
        this.WRM._dataArrived();
        this.clock.tick(1);
    },
    addError: function(key) {
        if (!this.WRM._unparsedData) {
            this.WRM._unparsedData = {};
        }
        this.WRM._unparsedData[key] = "";
        this.WRM._dataArrived();
        this.clock.tick(1);
    }
});

test("Does not fail when WRM._unparsedData does not exist", function() {
    ok(!this.WRM.data.claim("On a steel horse I ride"));
});

test("Returns basic data", function() {
    this.addData("bon", JSON.stringify("jovi"));

    equal(this.WRM.data.claim("bon"), "jovi");
});

test("Multiple data claims on same key throw an error", function() {
    this.addData("bon", JSON.stringify("doge-vi"));
    equal(this.WRM.data.claim("bon"), "doge-vi");
    equal(this.WRM.data.claim("bon"), null);
    ok(this.errorSpy.calledOnce);
    equal(this.errorSpy.getCall(0).args[0], "Data with key bon has already been claimed");
});

test("Returns JSON data", function() {
    this.addData("bon-jovi", JSON.stringify({ shot: "through the heart" }));

    deepEqual(this.WRM.data.claim("bon-jovi"), {
        shot: "through the heart"
    });
});

test("Returns data with pathologically bad keys", function() {
    this.addData("hasOwnProperty", JSON.stringify("mwahhahahah"));

    equal(this.WRM.data.claim("hasOwnProperty"), "mwahhahahah");
});

test("Handles malformed data", function() {
    this.addData("malformed", "{");

    ok(!this.WRM.data.claim("malformed"));
    // Test that an error message is logged to the console
    ok(this.logSpy.calledOnce);

    // Test that error message is logged every time
    this.WRM.data.claim("malformed");
    ok(this.logSpy.calledTwice);
});

test("Data arrives after async handler", function() {
    var val = false;
    this.WRM.data.claim("bon", function(data) {
        val = data;
    });
    equal(val, false);
    this.addData("bon", JSON.stringify("jovi"));
    equal(val, "jovi");
});

test("Error arrives after async handler", function() {
    var called = false;
    this.WRM.data.claim("bon", null, function() {
        called = true;
    });
    equal(called, false);
    this.addError("bon");
    equal(called, true);
});

test("Data arrives before async handler", function() {
    var val = false;
    this.addData("bon", JSON.stringify("jovi"));
    this.WRM.data.claim("bon", function(data) {
        val = data;
    });
    this.clock.tick(1);
    equal(val, "jovi");
});

test("Error arrives before async handler", function() {
    var called = false;
    this.addError("bon");
    this.WRM.data.claim("bon", null, function() {
        called = true;
    });
    this.clock.tick(1);
    equal(called, true);
});

test("invalid json is recieved as an failure async callback", function() {
    var val = "nothing";
    this.WRM.data.claim("bon",
        function(data) { val = "gotdata"; },
        function() { val = "goterror"; }
    );
    this.addData("bon", "{");
    equal(val, "goterror");
});

test("all callbacks get called even if they throw exceptions", function() {
    var called1 = false;
    var called2 = false;
    this.WRM.data.claim("a",
        function () {
            called1 = true;
            throw "a";
        }
    );
    this.WRM.data.claim("b",
        function () {
            called2 = true;
            throw "b";
        }
    );
    this.WRM._unparsedData["a"] = JSON.stringify("jovi");
    this.WRM._unparsedData["b"] = JSON.stringify("jovi");
    this.WRM._dataArrived();
    this.clock.tick(1);
    equal(called1, true);
    equal(called2, true);
});
