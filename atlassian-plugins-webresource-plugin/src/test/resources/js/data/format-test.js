module('wrm/format', {
    setup: function() {
        this.format = WRM.format;
    }
});

test('with 1 parameter', function () {
    var testFormat = this.format('hello {0}', 'world');
    equal(testFormat, 'hello world');
});

test('with 2 parameters', function () {
    var testFormat = this.format('hello {0} {1}', 'world', 'again');
    equal(testFormat, 'hello world again');
});

test('with 3 parameters', function () {
    var testFormat = this.format('hello {0} {1} {2}', 'world', 'again', '!');
    equal(testFormat, 'hello world again !');
});

test('with 4 parameters', function () {
    var testFormat = this.format('hello {0} {1} {2} {3}', 'world', 'again', '!', 'test');
    equal(testFormat, 'hello world again ! test');
});

test('with symbols', function () {
    var testFormat = this.format('hello {0}', '!@#$%^&*()');
    equal(testFormat, 'hello !@#$%^&*()');
});

test('with curly braces', function () {
    var testFormat = this.format('hello {0}', '{}');
    equal(testFormat, 'hello {}');
});

test('with repeated parameters', function () {
    var testFormat = this.format('hello {0}, {0}, {0}', 'world');
    equal(testFormat, 'hello world, world, world');
});

test('with apostrophe', function () {
    var testFormat = this.format('hello \'{0}\' {0} {0}', 'world');
    equal(testFormat, 'hello {0} world world');
});

test('with very long parameters', function () {
    var testFormat = this.format('hello {0}', new Array(25).join('this parameter is very long ')); // we're joining 25 empty values together, which means we'll get our join string 24 times.
    equal(testFormat, 'hello this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long '); // eslint-disable-line
});

// positional equivalence
test('with prefix', function () {
    var testFormat = this.format('{0} two three', 'one');
    equal(testFormat, 'one two three');
});

test('with infix', function () {
    var testFormat = this.format('one {0} three', 'two');
    equal(testFormat, 'one two three');
});

test('with postfix', function () {
    var testFormat = this.format('one two {0}', 'three');
    equal(testFormat, 'one two three');
});

test('with differing order', function () {
    var testFormat = this.format('{2} {0} {1}', 'two', 'three', 'one');
    equal(testFormat, 'one two three');
});

// choices
test('with a choice value missing parameter', function () {
    var testFormat = this.format('We got {0,choice,0#|1#1 issue|1<{1,number} issues}');
    equal(testFormat, 'We got ');
});

test('with a choice value with parameter lower first option', function () {
    var testFormat = this.format('We got {0,choice,0#0 issues|1#1|1<{1,number} issues}', -1, -1);
    equal(testFormat, 'We got 0 issues');
});

test('with a choice value first option', function () {
    var testFormat = this.format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 0);
    equal(testFormat, 'We got 0 issues');
});

test('with a choice value middle option', function () {
    var testFormat = this.format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 1);
    equal(testFormat, 'We got 1 issue');
});

test('with a choice value last option', function () {
    var testFormat = this.format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 2);
    equal(testFormat, 'We got 2 issues');
});

test('with a choice value with missing number parameter option', function () {
    var testFormat = this.format('We got {0,choice,0# |1#1 issue|1<{1,number} issues}', 2);
    equal(testFormat, 'We got  issues');
});

test('with a choice value with valid second option', function () {
    var testFormat = this.format('We got {0,choice,0# |1#1 issue|1<{1,number} issues}', 10, 10);
    equal(testFormat, 'We got 10 issues');
});

// number
test('with a number value', function () {
    var testFormat = this.format('Give me {0,number}!', 5);
    equal(testFormat, 'Give me 5!');
});

test('with a floating point value', function () {
    var testFormat = this.format('Give me {0,number}!', Math.PI);
    equal(testFormat, 'Give me 3.141592653589793!');
});

test('with a number value and missing parameter', function () {
    var testFormat = this.format('Give me {0,number}!');
    equal(testFormat, 'Give me !');
});

/** @note Not implemented according to jsdoc */
// test('with a number value and non-numeric parameter', function () {
//     var testFormat = this.format('Give me {0,number}!', 'everything');
//     equal(testFormat, 'Give me !');
// });

test('with a bit of everything', function() {
    var issueChoice = '{0,choice,0#0 zgłoszeń|1#1 zgłoszenia|4<{0,number} zgłoszenie|5<{0,number} zgłoszeń}';
    var timeChoice = '{1,choice,0#brak czasu|1#1 minuty|4<{1,number} minut|5<{1,number} minut}';
    var goodLuck = 'powodzenia';
    // reads: try to solve x issues within y minutes or less. good luck!
    var testFormat = this.format('spróbuj rozwiązać ' + issueChoice + ' w ciągu ' + timeChoice+ ' lub szybciej. {2}!', 4, 12, goodLuck);
    equal(testFormat, 'spróbuj rozwiązać 4 zgłoszenie w ciągu 12 minut lub szybciej. powodzenia!');
});

(function () {
    module('wrm/format - with invalid choicePart formats', {
        setup: function () {
            this.errorStub = sinon.stub(console, 'error');
        },
        teardown: function () {
            this.errorStub.restore();
        }
    });

    var invalidFormatToErrorMap = {
        'approval approvals: {0} {0,choice,1#|1<}': 'The format "0,choice,1#|1<" from message "approval approvals: {0} {0,choice,1#|1<}" is invalid.',
        'approval approvals: {0} {0,choice,1|1<}': 'The format "0,choice,1|1<" from message "approval approvals: {0} {0,choice,1|1<}" is invalid.',
        'approval approvals: {0} {0,choice,1}': 'The format "0,choice,1" from message "approval approvals: {0} {0,choice,1}" is invalid.',
    };

    function formatCall(invalidFormat) {
        return WRM.format(invalidFormat, 1, 5, 7, 9, 11);
    }

    Object.keys(invalidFormatToErrorMap).forEach(function (invalidFormat) {
        test('should not throw exception for invalid format "' + invalidFormat + '"', function () {
            try {
                formatCall(invalidFormat);
                ok(true, 'no exception as expected');
            } catch(e) {
                ok(false, 'expected no exception, but got ' + e);
            }
        });

        test('should log error for invalid format "' + invalidFormat + '"', function () {
            var error = invalidFormatToErrorMap[invalidFormat];

            formatCall(invalidFormat, 1);

            equal(this.errorStub.callCount, 1);
            equal(this.errorStub.lastCall.args[0], error);
        });
    });

}());
