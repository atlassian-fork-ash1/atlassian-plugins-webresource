module('wrm/data');

asyncTest("should be same as global", function() {
    require(["wrm/data"], function(data){
        equal(data, window.WRM.data);
        QUnit.start();
    });
});