module("in-order-loader", {
    setup: function() {
        this.Loader = window.WRM.InOrderLoader;
    },
    teardown: function() {
    }
});

test("Should match URIs", function() {
    var getScriptUriFromStack = this.Loader.getScriptUriFromStack;
    var data = [
        {text: "at https://example.com/async/async1.js:1:8", uri: "//example.com/async/async1.js"},
        {text: "at Global code (https://example.com/async/async1.js:10:1234)", uri: "//example.com/async/async1.js"},
        {text: "at Global code (http://example.com/async/async1.js:1:1)", uri: "//example.com/async/async1.js"},
        {text: "@https://example.com/async/async1.js:1:1", uri: "//example.com/async/async1.js"},
        {text: "at http://example.com/async/async1.js:1:8", uri: "//example.com/async/async1.js"},
        {text: "@http://example.com/async/async1.js:1:1", uri: "//example.com/async/async1.js"},
        {text: "http://abcd123.cloudfront.net/path/script.js", uri: "//abcd123.cloudfront.net/path/script.js"},
        {text: "https://abcd123.cloudfront.net/path/script.js:1:2", uri: "//abcd123.cloudfront.net/path/script.js"},
        {text: "https://abcd123.cloudfront.net/path/script.js:98", uri: "//abcd123.cloudfront.net/path/script.js"},
        {
            text: "http://localhost:8090/jira/s/b20ac26d125de877fcc08f87bc382fc4-CDN/en_AU-a0gktl/71001/b6b48b2829824b869586ac216d119363/f203a29efa3c75f6d8be812db073ed26/_/download/contextbatch/js/_super/batch.js?async=true&atlassian.aui.raphael.disabled=true&locale=en-AU:20:123",
            uri: "//localhost:8090/jira/s/b20ac26d125de877fcc08f87bc382fc4-CDN/en_AU-a0gktl/71001/b6b48b2829824b869586ac216d119363/f203a29efa3c75f6d8be812db073ed26/_/download/contextbatch/js/_super/batch.js?async=true&atlassian.aui.raphael.disabled=true&locale=en-AU"
        },
        {
            text: "at Global code (https://example.com:8090/jira/s/c888ed4f8cd068cdfe4c3cb29e665955-CDN/en_AU-luo7vw/71001/b6b48b2829824b869586ac216d119363/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js?async=true&locale=en-AU:10:20)",
            uri: "//example.com:8090/jira/s/c888ed4f8cd068cdfe4c3cb29e665955-CDN/en_AU-luo7vw/71001/b6b48b2829824b869586ac216d119363/1.0/_/download/batch/jira.webresources:jira-fields/jira.webresources:jira-fields.js?async=true&locale=en-AU"
        }
    ];
    data.forEach(function(testCase) {
        assertUri(testCase.text, testCase.uri, getScriptUriFromStack);
    });
});

test("Should extract URI from stack", function() {
    var getScriptUriFromStack = this.Loader.getScriptUriFromStack;
    var data = [
        {
            text: "at FunctionName (<Fully-qualified name/URL>:<line number>:<column number>) \n" +
            "at Global code (https://example.com/async/async1.js:1:8)",
            uri: "//example.com/async/async1.js"
        },
        {
            text: "at FunctionName (<Fully-qualified name/URL>:<line number>:<column number>) \n" +
            "at Global code (https://example.com/async/async1.js:1:8)\n",
            uri: "//example.com/async/async1.js"
        },
        {
            text: "at FunctionName (<Fully-qualified name/URL>:<line number>:<column number>) \n" +
            "at FunctionName (<Fully-qualified name/URL>:<line number>:<column number>) \n" +
            "at Global code (https://example.com/async/async1.js:1:8)",
            uri: "//example.com/async/async1.js"
        }
    ];
    data.forEach(function(testCase) {
        assertUri(testCase.text, testCase.uri, getScriptUriFromStack);
    });
});

test("Should normalise URIs correctly", function() {
    var normaliseUri = this.Loader.normalizeUri;
    var data = [
        {
            in: "http://example.com",
            out: "//example.com"
        },
        {
            in: "https://example.com",
            out: "//example.com"
        },
        {
            in: "//example.com",
            out: "//example.com"
        },
        {
            in: "/jira/s",
            out: "/jira/s"
        }
    ];
    data.forEach(function(testCase) {
        strictEqual(normaliseUri(testCase.in), testCase.out, "Incorrect normalisation");
    });
});

module("Should call async scripts in order", function(hooks) {
    var MockScript = function(src, document) {
        var attributes = {};

        return {
            src: src,
            definition: sinon.spy().named(src),
            hasAttribute: function(name) {
                return attributes.hasOwnProperty(name);
            },
            setAttribute: function(name, value) {
                attributes[name] = value;
            },
            mockScriptLoaded: function(loader) {
                document.currentScript.src = src;
                loader.define(this.definition);
            }
        }
    };

    hooks.beforeEach( function() {
        this.document = {
            head: {querySelectorAll: sinon.stub()},
            currentScript: {
                src: null
            }
        };
        this.scripts = [
            MockScript("script1", this.document),
            MockScript("script2", this.document),
            MockScript("script3", this.document)
        ];

        this.document.head.querySelectorAll.withArgs("script[data-initially-rendered]").returns(this.scripts);


        this.loader = window.WRM._InOrderLoaderFactory(this.document);
        this.assertAllScriptsCalledInOrder = function() {
            sinon.assert.callOrder(this.scripts[0].definition, this.scripts[1].definition, this.scripts[2].definition);
            sinon.assert.calledOnce(this.scripts[0].definition);
            sinon.assert.calledOnce(this.scripts[1].definition);
            sinon.assert.calledOnce(this.scripts[2].definition);
        };
    });

    test("when scripts are loaded in order", function() {
        // when
        this.scripts[0].mockScriptLoaded(this.loader);
        this.scripts[1].mockScriptLoaded(this.loader);
        this.scripts[2].mockScriptLoaded(this.loader);

        // then
        this.assertAllScriptsCalledInOrder();
    });
    test("when scripts are loaded out of order: 1, 0, 2", function() {
        // when
        this.scripts[1].mockScriptLoaded(this.loader);
        this.scripts[0].mockScriptLoaded(this.loader);
        this.scripts[2].mockScriptLoaded(this.loader);

        // then
        this.assertAllScriptsCalledInOrder();
    });
    test("when scripts are loaded out of order: 0, 2, 1", function() {
        // when
        this.scripts[0].mockScriptLoaded(this.loader);
        this.scripts[2].mockScriptLoaded(this.loader);
        this.scripts[1].mockScriptLoaded(this.loader);

        // then
        this.assertAllScriptsCalledInOrder();
    });
    test("when scripts are loaded out of order: 2, 1, 0", function() {
        // when
        this.scripts[2].mockScriptLoaded(this.loader);
        this.scripts[1].mockScriptLoaded(this.loader);
        this.scripts[0].mockScriptLoaded(this.loader);

        // then
        this.assertAllScriptsCalledInOrder();
    });
});

// END of tests

function assertUri(text, uri, fn) {
    strictEqual(fn(text), uri, "URI '" + uri + "' should be matched");
}
