(function() {
    // Cache reference to jQuery in case an upstream consumer calls noConflict()
    var jq = window.jQuery;
    WRM.define("wrm/jquery", function() {
        return jq;
    });
}());
