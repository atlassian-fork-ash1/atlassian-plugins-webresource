
WRM.define("wrm/builder", ["wrm/logger"], function(logger) {

    var Builder = function() {
        this.wrmKeyAttribute = "data-wrm-key";
        this.wrmBatchTypeAttribute = "data-wrm-batch-type";
    };

    Builder.prototype = {
        // Builds a list of loaded modules and contexts on the page
        initialize: function(container) {
            var scripts = container.querySelectorAll("script[data-wrm-key][data-wrm-batch-type]"),
                links = container.querySelectorAll("link[data-wrm-key][data-wrm-batch-type]"),
                loadedResources = {
                    modules: [],
                    contexts: []
                };

            this._inspectTagArray(scripts, loadedResources);
            this._inspectTagArray(links, loadedResources);

            return loadedResources;
        },

        _inspectTagArray: function(elements, loadedResources) {
            for (var i = 0; i < elements.length; i++) {
                var element = elements[i];
                this.addResource(element.getAttribute(this.wrmKeyAttribute), element.getAttribute(this.wrmBatchTypeAttribute), loadedResources);
            }
        },

        addResource: function(wrmKey, batchType, loadedResources) {
            var keys = wrmKey.split(',');
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i];

                if (key[0] === "-") {
                    continue;
                }

                if (batchType === "resource") {
                    this._addToLoadedList(loadedResources.modules, key);
                } else if (batchType === "context") {
                    this._addToLoadedList(loadedResources.contexts, key);
                } else {
                    logger.log("Unknown batch type: " + batchType);
                }
            }
        },

        _addToLoadedList: function(loadedList, key) {
            if (loadedList.indexOf(key) !== -1) {
                return;
            }
            loadedList.push(key);
        }
    };

    return Builder;
});

