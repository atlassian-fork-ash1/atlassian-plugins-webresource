WRM.define("wrm/require-handler", [
    "wrm/jquery",
    "wrm/_",
    "wrm/logger",
    "wrm/builder",
    "wrm/context-path"
], function(
    $,
    _,
    logger,
    Builder,
    wrmContextPath
) {
    /**
     * @typedef {Object} Resource
     * @property {string} url - the location of the resource on the server.
     * @property {string} key - the complete module key for the resource's parent web-resource.
     * @property {('JAVASCRIPT'|'CSS')} resourceType - the kind of asset at the given url.
     * @property {('context'|'resource')} batchType - whether this url represents a context batch or a standalone resource.
     * @property {string} [media] - (for CSS resources) the type of media the resource should render to (e.g., print).
     * @property {boolean} [ieOnly=false] - whether the resource should only render in IE < 10.
     * Since the WRM only supports IE 11+, setting this to true will prevent the resource from rendering.
     * @property {string} [conditionalComment] - the specific IE version(s) the resource should render in.
     * Since the WRM only supports IE 11+, setting this to a non-empty value will prevent the resource from rendering.
     */

    var WEBRESOURCE_PATTERN = /^wr!(.*)$/;
    var CONTEXT_PATTERN = /^wrc!(.*)$/;
    var LOADER_PATTERN = /^.+!/;

    /**
     * @enum
     */
    var failcases = {
        AJAX: 1,
        CURL: 2,
    };

    /**
     * Removes resources that have any parameters configured to output them in IE 9 or below.
     * @param {Resource[]} resources
     * @returns {Resource[]}
     */
    function omitLegacyIeResourcesFilter(resources) {
        return _.filter(resources, function(resource) {
            var hasLegacyIEParams = resource.ieOnly || resource.conditionalComment;
            return !hasLegacyIEParams;
        });
    }

    function RequireHandler() {
        this._requireCache = {
            // handle empty resource requests gracefully
            '': $.Deferred().resolveWith(this, [window]).promise()
        };
        this._resetRequestedResources();

        // Contains a promise for the currently executing to the WRM REST API, otherwise false
        this._requestInFlight = false;
        this._willIterate = false;
    }

    RequireHandler.prototype = {
        /**
         * Requires resources on the page.
         * @param {Resource[]} resources list of resources (eg webresources or webresource contexts)
         * @param {Function} [cb] Callback that is executed when the actual JS / CSS resources representing the given
         * resources have been included on the page
         * @return {Promise} a Promise that is resolved when all JS / CSS resources have been included on the page. Can be used
         * as an alternative to callback.
         */
        require: function(resources, cb) {
            if (!_.isArray(resources)) {
                resources = [resources];
            }
            resources = _.filter(resources, function(res) {
                return typeof res === 'string';
            });

            // Requests are cached in this._requireCache so that if a client makes multiple requests for the same
            // set of resources, we can piggyback off the promise for the first request.
            // In other words, if a client calls require([a, b]), then does some work, then calls require([a, b])
            // again, the second call should resolve immediately (or after the first call has resolved).
            if (!this._requireCache.hasOwnProperty(resources)) {
                var deferred = $.Deferred();
                var that = this;

                function resolveAsync() {
                    that._resolveAsync(resources).done(deferred.resolve).fail(deferred.reject);
                }

                var InOrderLoader = WRM.InOrderLoader;
                if (typeof InOrderLoader !== 'undefined') {
                    InOrderLoader.registerAllResolvedCallback(resolveAsync);
                } else {
                    resolveAsync();
                }

                this._requireCache[resources] = deferred.promise();
            }

            var promise = this._requireCache[resources];
            if (cb) {
                promise.done(function() {
                    cb.apply(this, arguments);
                });
            }
            return promise;
        },
        /**
         * Given a list of resources, translates those resources to actual CSS / JS files and includes them on the page
         * @param resources a list of webresources and webresource contexts to include
         * @return a Promise that is resolved only after all resources have been included on the page
         * @private
         */
        _resolveAsync: function(resources) {
            this._addToRequestedResources(resources);

            var deferred = $.Deferred();
            var that = this;

            function onRequestFail(failcase, args) {
                if (failcase === failcases.AJAX) {
                    delete that._requireCache[resources];
                }
                deferred.rejectWith(that, args);
            }

            function onRequestDone() {
                var callbackArgs = _.map(resources, function() { return window; });
                deferred.resolveWith(that, callbackArgs);
            }

            this._enqueueRequest()
                .done(onRequestDone)
                .fail(onRequestFail);

            return deferred.promise();
        },
        /**
         * Reduces the number of requests made to the server, making them only when there are
         * resources to be loaded.
         *
         * Think of it like a bus stop.
         * - If you get to the stop while the bus is there, you travel immediately.
         * - Otherwise, you wait a while until the next bus arrives. Others might show up while you wait.
         *   You may not be the first person on to the bus when it arrives.
         *
         * @returns a Promise that resolves when a response comes back from the server with
         * the resources relevant to the request.
         * @private
         */
        _enqueueRequest: function () {
            var deferred = $.Deferred();

            var iterate = function() {
                this._willIterate = false;
                if (this._shouldRequestResources()) {
                    this._requestInFlight = this._getScriptsForResources();
                }
                if (this._requestInFlight) {
                    this._requestInFlight.done(deferred.resolve).fail(deferred.reject);
                }
            }.bind(this);

            if (this._willIterate) {
                this._requestInFlight.done(iterate).fail(iterate);
            }
            else if (this._requestInFlight && this._requestInFlight.state() === "pending") {
                // wait until the previous request is done before making another one.
                this._requestInFlight.done(iterate).fail(iterate);
                this._willIterate = true;
            } else {
                // kick off the request right now
                iterate();
            }

            return deferred.promise();
        },
        /**
         * Processes a response from the WRM REST endpoint.
         * This needs to have a 1:1 call relationship between _getScriptsForResources' use of $.ajax
         * @param {Object} resourceResponse
         * @param {Resource[]} resourceResponse.resources
         * @private
         */
        _processResourceResponse: function(resourceResponse) {
            var deferred = new $.Deferred();
            var that = this;

            if (resourceResponse.unparsedData) {
                WRM._unparsedData || (WRM._unparsedData = {});
                _.each(resourceResponse.unparsedData, function(val, key) {
                    WRM._unparsedData[key] = val;
                });
                WRM._dataArrived();
            }
            if (resourceResponse.unparsedErrors) {
                WRM._unparsedErrors || (WRM._unparsedErrors = {});
                _.each(resourceResponse.unparsedErrors, function(val, key) {
                    WRM._unparsedErrors[key] = val;
                });
                WRM._dataArrived();
            }
            var curlResources = [];
            var cssMediaResources = [];
            var filteredResourceUrlInfos = this._filter(resourceResponse.resources);
            for (var i = 0; i < filteredResourceUrlInfos.length; ++i) {
                var resource = filteredResourceUrlInfos[i];
                var url = resource.url;

                this._builder.addResource(resource.key, resource.batchType, this._loadedResources);

                if (resource.resourceType === "JAVASCRIPT") {
                    if (!this._isJSInInitLoad(url)) {
                        curlResources.push("js!" + url + "!order");
                    }
                }
                else if (resource.resourceType === "CSS") {
                    if (!this._isCSSInInitLoad(url)) {
                        if (resource.media && "all" !== resource.media) {
                            // HACK: this can't be loaded by curl.js. The solution is to the DOM immediately
                            // using a <link> tag. This means that the callback may be called before the CSS
                            // has been loaded, resulting in a flash of unstyled content.
                            cssMediaResources.push(resource);
                        }
                        else {
                            curlResources.push("css!" + url);
                        }
                    }
                }
                else {
                    logger.log("Unknown resource type required: " + url);
                }
            }
            logger.log("Downloading resources:\n" + curlResources.join("\n"));
            WRM.curl(curlResources, function() {
                // Add all css media resources. This is done after curl resources to ensure ordering is consistent
                // with the way resources are delivered on the server.
                _.each(cssMediaResources, function(resource) {
                    that._loadCssImmediate(resource);
                });
                deferred.resolveWith(that, arguments);
            }, function() {
                deferred.rejectWith(that, arguments);
            });
            return deferred.promise();
        },
        /**
         * Loads
         * @param resource
         * @private
         */
        _loadCssImmediate: function(resource) {
            logger.log('WARN: asynchronously loading a CSS resource containing a media query: ' + resource.url);
            var tag = document.createElement("link");
            tag.setAttribute("rel", "stylesheet");
            tag.setAttribute("type", "text/css");
            tag.setAttribute("href", resource.url);
            tag.setAttribute("media", resource.media);
            document.head.appendChild(tag);
        },
        /**
         * Makes an AJAX request to retrieve the actual JS and CSS files required to represent a resource
         * @return a Promise for the AJAX request
         * @private
         */
        _getScriptsForResources: function() {
            if (!this._builder) {
                this._builder = new Builder();
                this._loadedResources = this._builder.initialize(document);
            } else {
                this._updateLoadedResourcesFromDom();
            }

            var deferred = $.Deferred();
            $.ajax({
                url: wrmContextPath() + "/rest/webResources/1.0/resources",
                type: "POST",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    r: this._requestedResources.webResources,
                    c: this._requestedResources.contexts,
                    xc: this._loadedResources.contexts,
                    xr: this._loadedResources.modules
                }),
                success: function (resourceResponse) {
                    this._processResourceResponse(resourceResponse)
                        .done(deferred.resolve)
                        .fail(function() { deferred.rejectWith(this, [failcases.CURL].concat(arguments)); })
                }.bind(this),
                error: function() { deferred.rejectWith(this, [failcases.AJAX].concat(arguments)); }
            });

            // Clear out what we've just requested so subsequent ones won't double up.
            this._resetRequestedResources();

            return deferred.promise();
        },
        /**
         * PLUGWEB-399: Inspect the DOM to see if any <script> or <link> tags have already been added, in which case
         * they should not be loaded again.
         *
         * PLUGWEB-402: Only <script> or <link> tags in the DOM with the "data-wrm-key" and "data-wrm-batch-type" are
         * inspected to count which resources have already been loaded. HOWEVER, these attributes are only added if the
         * <script> or <link> tags were rendered on the server-side. Resources loaded asynchronously do not add these
         * attributes and are instead counted when the URLs are returned from "/rest/webResources/1.0/resources" and
         * processed.
         */
        _updateLoadedResourcesFromDom: function() {
            function addMissingItems(sourceList, destList) {
                _.each(sourceList, function(item) {
                    if (destList.indexOf(item) === -1) {
                        destList.push(item);
                    }
                });
            }

            var resourcesLoadedInDom = this._builder.initialize(document);
            addMissingItems(resourcesLoadedInDom.modules, this._loadedResources.modules);
            addMissingItems(resourcesLoadedInDom.contexts, this._loadedResources.contexts);
        },
        /**
         *
         * @param resources list of resources (e.g. context or web resource)
         * @private
         */
        _addToRequestedResources: function(resources) {
            var that = this;

            _.each(resources, function(resource) {
                var match;
                if (match = resource.match(WEBRESOURCE_PATTERN)) {
                    that._requestedResources.webResources.push(match[1]);
                }
                else if (match = resource.match(CONTEXT_PATTERN)) {
                    that._requestedResources.contexts.push(match[1]);
                }
                // If it's neither context nor web-resource it should be AMD module.
                else {
                    // Handling modules as if it's the web-resource.
                    that._requestedResources.webResources.push(resource.replace(LOADER_PATTERN, ''));
                }
            });
        },
        _shouldRequestResources: function() {
            return (this._requestedResources.webResources.length || this._requestedResources.contexts.length);
        },
        _resetRequestedResources: function() {
            this._requestedResources = {
                contexts: [],
                webResources: []
            };
        },
        /**
         * Checks if a script element whose src is the given url exists on the page
         * @param url url
         * @return {boolean} True if the script is on the page, otherwise false
         * @private
         */
        _isJSInInitLoad: function(url) {
            return document.querySelectorAll("script[src='" + url + "']").length > 0;
        },
        /**
         * Checks if a link element whose href is the given url exists on the page
         * @param url url
         * @return {boolean} True if the link is on the page, otherwise false
         * @private
         */
        _isCSSInInitLoad: function(url) {
            return document.querySelectorAll("link[href='" + url + "']").length > 0;
        },
        /**
         * Filters resources that should not be included from the given list
         * @param resourceUrlInfos list
         * @return list of filters resourceUrlInfo objects.
         * @private
         */
        _filter: function(resourceUrlInfos) {
            if (!this._filters) {
                this._filters = [
                    omitLegacyIeResourcesFilter
                ];
            }
            var filteredResourceUrlInfos = resourceUrlInfos;
            _.each(this._filters, function(filter) {
                filteredResourceUrlInfos = filter(filteredResourceUrlInfos);
            });
            return filteredResourceUrlInfos;
        }
    };

    return RequireHandler;
});
