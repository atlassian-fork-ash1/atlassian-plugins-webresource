package com.atlassian.webresource.plugin.prebake.util;

public final class StopWatch {

    private long start;

    public StopWatch() {
        start = System.currentTimeMillis();
    }

    public long getElapsedSecondsAndReset() {
        long now = System.currentTimeMillis();
        long elapsed = now - start;
        start = now;
        return elapsed / 1000;
    }

}