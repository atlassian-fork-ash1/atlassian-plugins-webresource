package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.plugin.webresource.impl.PrebakeErrorFactory;
import io.atlassian.util.concurrent.ThreadFactories;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;
import com.atlassian.webresource.api.assembler.resource.PrebakeWarning;
import com.atlassian.webresource.plugin.prebake.exception.PreBakeException;
import com.atlassian.webresource.plugin.prebake.exception.PreBakeIOException;
import com.atlassian.webresource.plugin.prebake.resources.GenericUrlResource;
import com.atlassian.webresource.plugin.prebake.resources.Resource;
import com.atlassian.webresource.plugin.prebake.resources.ResourceCollector;
import com.atlassian.webresource.plugin.prebake.resources.ResourceContent;
import com.atlassian.webresource.plugin.prebake.util.StopWatch;
import com.google.common.base.Preconditions;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Responsible for pre bake processing.
 *
 * @since v3.5.0
 */
public final class DiscoveryTask {

    private static final Logger log = LoggerFactory.getLogger(DiscoveryTask.class);

    private static final int MAX_RESOURCE_QUEUE_LENGTH = 2048;
    private static final int MAX_THREADS = 2;
    private static final ExecutorService pool = Executors.newFixedThreadPool(
            MAX_THREADS,
            ThreadFactories.namedThreadFactory("prebaker-collection", ThreadFactories.Type.DAEMON)
    );

    private final List<WebResourceCrawler> crawlers;
    private final ResourceCollector resourceCollector;
    private final Bundler bundler;
    private final String contextPath;
    private final String baseUrl;
    private final Pattern cssUrlPattern;

    private final boolean ignorePrebakeWarnings;
    private final boolean isCSSPrebakingEnabled;

    public DiscoveryTask(
            boolean ignorePrebakeWarnings,
            boolean isCSSPrebakingEnabled,
            String contextPath,
            String baseUrl,
            List<WebResourceCrawler> crawlers,
            ResourceCollector resourceCollector,
            Bundler bundler) {
        Preconditions.checkArgument(!CollectionUtils.isEmpty(crawlers));

        this.ignorePrebakeWarnings = ignorePrebakeWarnings;
        this.isCSSPrebakingEnabled = isCSSPrebakingEnabled;
        this.contextPath = contextPath;
        this.baseUrl = baseUrl;
        this.crawlers = crawlers;
        this.resourceCollector = resourceCollector;
        this.bundler = bundler;
        cssUrlPattern = Pattern.compile(
                "url[\\s]*+\\([\\s]*+[\"']?[\\s]*+(?!https?://|data:)" +
                Pattern.quote(contextPath) +
                "(?<url>[^\\)\"']*)[\\s]*+[\"']?[\\s]*+\\)"
        );
    }

    public void doRun() throws PreBakeException {
        AtomicBoolean urlFetchingComplete = new AtomicBoolean(false);
        BlockingQueue<Resource> queue = new ArrayBlockingQueue<>(MAX_RESOURCE_QUEUE_LENGTH);

        StopWatch stopwatch = new StopWatch();
        final Future<Void> resourceFetcher = pool.submit(() -> {
            // Gather resources
            while (!queue.isEmpty() || !urlFetchingComplete.get()) {
                Resource res = queue.poll(2, TimeUnit.SECONDS);
                if (res == null) {
                    continue;
                }
                collect(res);
            }
            return null;
        });

        pool.submit(() -> {
            // Discover resources
            try {
                for (WebResourceCrawler c : crawlers) {
                    c.crawl(queue);
                }
            } catch (InterruptedException e) {
                resourceFetcher.cancel(true);
            } finally {
                urlFetchingComplete.set(true);
            }
        });

        try {
            resourceFetcher.get();
            log.info("Resource collection took " + stopwatch.getElapsedSecondsAndReset() + "s");
        } catch (Exception e) {
            throw new PreBakeException("Unable to complete prebake", e);
        }
    }

    /**
     * Collects and prebakes a single resource.
     *
     * <p> This function is responsible for retrieving the resource, processing it (e.g., prebaking CSS relative url
     * references), creating the hashed filename, and saving the file on disk.
     *
     * @param res              Resource to collect
     * @return filename of the collected resource. Corresponds to the hash of the content, followed by the
     * extension of the resource file. This value can be used to retrieve the file from the {@code resourcesDir}.
     */
    private Optional<String> collect(Resource res) {
        if (!isCSSPrebakingEnabled && res.getExtension().equals(".css")) {
            return Optional.empty();
        }

        String url = res.getUrl();

        // Relative URL, used for the mapping file
        String relativeUrl = toRelativeUrl(url);

        if (bundler.isMapped(relativeUrl)) {
            return bundler.getMapping(relativeUrl);
        } else if (bundler.isTainted(relativeUrl)) {
            return Optional.empty();
        }

        if (!isSafeForPrebaking(res)) {
            log.warn(format("Encountered tainted resource: %s", url));
            bundler.addTaintedResource(relativeUrl, res);
            return Optional.empty();
        }

        try {
            // Complete URI, used to retrieve the resource from the WebResourceManager
            URI uri = toAbsoluteURI(url);
            ResourceContent content = resourceCollector.collect(uri);
            if (res.getExtension().equals(".css")) {
                content = processCSS(content);
            }
            return bundler.addResource(relativeUrl, res, content);
        } catch (Exception e) {
            String msg = format("Error processing resource '%s'", url);
            log.warn(msg);
            bundler.addTaintedResource(relativeUrl, res.getName(), PrebakeErrorFactory.from(msg, e));
        }
        return Optional.empty();
    }

    private boolean isSafeForPrebaking(Resource res) {
        if (!res.isTainted()) {
            return true;
        } else if (!ignorePrebakeWarnings) {
            return false;
        }

        List<PrebakeError> errors = res.getPrebakeErrors();
        if (!containsOnlyWarnings(errors)) {
            return false;
        }

        StringBuilder sb = new StringBuilder()
                .append("Ignoring PrebakeWarning on resource ")
                .append(res.getUrl())
                .append(":");
        for (PrebakeError e : errors) {
            sb.append("\n\t").append(e.toString());
        }
        log.warn(sb.toString());
        return true;
    }

    private boolean containsOnlyWarnings(List<PrebakeError> errors) {
        return !errors.stream()
                .anyMatch(e -> !(e instanceof PrebakeWarning));
    }

    /**
     * Searches the content of a CSS file for product-relative URLs, prebakes those resources, and
     * substitutes their product-relative URL with the corresponding CT-CDN hashed URL.
     *
     * @param originalCSS      Original CSS resource
     * @return Processed CSS resource
     * @throws PreBakeIOException In case a resource referenced from the CSS file cannot be collected.
     */
    private ResourceContent processCSS(ResourceContent originalCSS) throws PreBakeIOException {
        final String input = new String(originalCSS.getContent(), UTF_8);
        final Matcher matcher = cssUrlPattern.matcher(input);
        final StringBuffer output = new StringBuffer();
        while (matcher.find()) {
            String cssResourceUrl = matcher.group("url").trim();
            Optional<String> hash = collect(new GenericUrlResource(cssResourceUrl));
            String replacement = hash
                    .map(s -> "url(" + s + ")")
                            // We're taking the conservative approach here. By throwing this exception, we declare the
                            // entire CSS file tainted if any of the product-relative resource that it references cannot be
                            // collected.
                    .orElseThrow(() -> new PreBakeIOException(format("Cannot prebake CSS file: resource '%s' is missing", cssResourceUrl)));
            matcher.appendReplacement(output, replacement);
        }
        matcher.appendTail(output);
        return new ResourceContent(
                originalCSS.getUri(),
                originalCSS.getContentType(),
                output.toString().getBytes(UTF_8)
        );
    }

    private String toRelativeUrl(String uriString) {
        if (uriString.contains(baseUrl)) {
            return uriString.replace(baseUrl, "");
        } else {
            return uriString;
        }
    }

    private URI toAbsoluteURI(String uriString) throws URISyntaxException {
        String absoluteUrl;

        if (uriString.startsWith(baseUrl)) {
            absoluteUrl = uriString;
        } else if (uriString.startsWith(contextPath)) {
            absoluteUrl = baseUrl + uriString.replace(contextPath, "");
        } else {
            absoluteUrl = baseUrl + uriString;
        }

        return new URI(absoluteUrl);
    }

}
