package com.atlassian.webresource.plugin.prebake.resources;

import org.apache.commons.lang3.StringUtils;

import java.net.URI;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Wrapper class for storing the contents of a resource fetched during prebaking.
 *
 * @since 3.5.9
 */
public final class ResourceContent {

    private final URI uri;
    private final String contentType;
    private final byte[] content;

    public ResourceContent(URI uri, String contentType, byte[] content) {
        checkArgument(StringUtils.isNotBlank(contentType));
        this.uri = checkNotNull(uri);
        this.contentType = contentType;
        this.content = checkNotNull(content);
    }

    public URI getUri() {
        return uri;
    }

    public String getContentType() {
        return contentType;
    }

    public byte[] getContent() {
        return content;
    }

}
