package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.webresource.api.assembler.resource.PrebakeError;

import java.util.Collections;
import java.util.List;

import static java.util.Collections.singletonList;

/**
 * A class representing a non CDN-safe resource, i.e., a web resource for which the
 * pre-baking operation could not be completed due to the presence of dimension-unaware
 * transformers or conditions.
 *
 * @since 3.5.9
 */
public final class TaintedResource {

    private final String url;
    private final String fullName;
    private final List<PrebakeError> prebakeErrors;

    protected TaintedResource(
            String url,
            String fullName,
            List<PrebakeError> prebakeErrors) {
        this.url = url;
        this.fullName = fullName;
        this.prebakeErrors = Collections.unmodifiableList(prebakeErrors);
    }

    protected TaintedResource(
            String url,
            String fullName,
            PrebakeError prebakeError) {
        this(url, fullName, singletonList(prebakeError));
    }

    /**
     * Returns the URL of the tainted resource
     *
     * @return URL of the tanted resource
     * @since 3.5.9
     */
    public String getUrl() {
        return url;
    }

    /**
     * Returns a descriptive name of the tainted resource
     *
     * @return full name of the tainted resource
     * @since 3.5.9
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Returns the list of errors generated while pre-baking the resource
     *
     * @return errors generated while pre-baking the resource
     * @since 3.5.9
     */
    public List<PrebakeError> getPrebakeErrors() {
        return prebakeErrors;
    }

}
