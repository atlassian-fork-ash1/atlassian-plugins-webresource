package com.atlassian.webresource.plugin.async;

import com.atlassian.webresource.api.assembler.resource.PluginUrlResource.BatchType;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Resolves web resources into a format suitable for delivery via REST
 *
 * @since v3.0
 */
public interface AsyncWebResourceLoader {
    public static enum ResourceType {
        JAVASCRIPT,
        CSS
    }

    @XmlRootElement
    public static class Resource {
        @XmlAttribute
        public final String url;
        @XmlAttribute
        public final ResourceType resourceType;
        @XmlAttribute
        @Deprecated
        public final String conditionalComment;
        @XmlAttribute
        @Deprecated
        public final boolean ieOnly;
        @XmlAttribute
        public final String media;
        @XmlAttribute
        public final String key;
        @XmlAttribute
        public final String batchType;

        @Deprecated
        public Resource(String url, ResourceType resourceType, String conditionalComment, boolean ieOnly, String media,
                        String key, BatchType batchType) {
            this.url = url;
            this.resourceType = resourceType;
            this.media = media;
            this.key = key;
            this.batchType = batchType.name().toLowerCase();
            this.ieOnly = ieOnly;
            this.conditionalComment = conditionalComment;
        }

        public Resource(String url, ResourceType resourceType, String media,
                        String key, BatchType batchType) {
            this(url, resourceType, "", false, media, key, batchType);
        }
    }

    @XmlRootElement
    public static class ResourcesAndData {
        @XmlAttribute
        public final Iterable<Resource> resources;

        @XmlAttribute
        public final Map<String, String> unparsedData;

        @XmlAttribute
        public final Map<String, String> unparsedErrors;

        public ResourcesAndData(Iterable<Resource> resources, Map<String, String> data, Map<String, String> unparsedErrors) {
            this.resources = resources;
            this.unparsedData = data;
            this.unparsedErrors = unparsedErrors;
        }
    }

    public ResourcesAndData resolve(Set<String> webResources, Set<String> contexts, Set<String> excludeResources,
                                    Set<String> excludeContexts) throws IOException;
}
