package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.plugin.webresource.impl.snapshot.RootPage;
import com.atlassian.plugin.webresource.impl.snapshot.WebResource;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.atlassian.webresource.plugin.prebake.resources.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * A {@link WebResourceCrawler} that collects all resource urls accessible from a series of {@code <root-page>}s.
 *
 * @since v3.5.9
 */
public final class RootPageCrawler implements WebResourceCrawler {

    private final Logger log = LoggerFactory.getLogger(RootPageCrawler.class);
    private final PrebakeWebResourceAssemblerFactory assemblerFactory;
    private final Iterable<RootPage> rootPages;
    private final Dimensions valueWhitelist;

    public RootPageCrawler(
            PrebakeWebResourceAssemblerFactory assemblerFactory,
            Iterable<RootPage> rootPages,
            Dimensions valueWhitelist) {
        this.rootPages = rootPages;
        this.assemblerFactory = assemblerFactory;
        this.valueWhitelist = valueWhitelist;
    }

    @Override
    public void crawl(BlockingQueue<Resource> queue) throws InterruptedException {
        log.debug("Getting resource URLs from Root Page Crawler");

        for (RootPage rp : rootPages) {
            String rpKey = rp.getWebResource().getKey();
            log.debug("Generating resource URLs for root page {}", rpKey);

            Dimensions dims = assemblerFactory
                    .computeBundleDimensions(rp.getWebResource())
                    .whitelistValues(valueWhitelist);
            log.debug("{} dimensions are:\n {}", rpKey, dims);
            log.debug("The size of the cartesian product is: {}", dims.cartesianProductSize());

            Iterator<Coordinate> coords = dims.cartesianProduct().iterator();
            while (coords.hasNext()) {
                WebResourceAssembler assembler = createAssembler(rp, coords.next(), assemblerFactory);
                crawlAssembledResource(assembler.assembled(), queue);
            }
        }
    }

    @Override
    public List<String> getTargets() {
        return StreamSupport.stream(rootPages.spliterator(), false)
                .map(RootPage::getWebResource)
                .map(WebResource::getKey)
                .collect(Collectors.toList());
    }

    private WebResourceAssembler createAssembler(
            RootPage page,
            Coordinate coord,
            PrebakeWebResourceAssemblerFactory fct) {
        WebResourceAssembler assembler = fct
                .create()
                .withCoordinate(coord)
                .build();
        assembler.resources().requirePage(page.getWebResource().getKey());
        return assembler;
    }

}
