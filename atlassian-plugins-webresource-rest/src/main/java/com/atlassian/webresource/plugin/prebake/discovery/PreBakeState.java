package com.atlassian.webresource.plugin.prebake.discovery;

/**
 * @since v3.5.0
 */
public enum PreBakeState {
    NOTSTARTED, // pre-baking wasn't started yet
    RUNNING, // pre-baking is in progress
    DONE, // pre-baking is done
    CANCELLED, // pre-baking was cancelled
    ;
}