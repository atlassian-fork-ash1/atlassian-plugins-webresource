package com.atlassian.webresource.plugin.prebake.resources;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.assembler.resource.PrebakeError;

import javax.annotation.Nonnull;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link Resource} that wraps a {@link PluginUrlResource}.
 *
 * @since 3.5.13
 */
public final class PluginResource extends Resource {

    private final PluginUrlResource<?> resource;

    /**
     * Creates a new {@link Resource} using an existing {@link PluginCssResource} or {@link PluginJsResource}.
     *
     * @param resource resource to wrap
     * @throws IllegalArgumentException if the resource passed as parameter is not a
     *                                  {@link PluginJsResource} or a {@link PluginCssResource}
     */
    public PluginResource(@Nonnull PluginUrlResource<?> resource) {
        checkNotNull(resource);
        checkArgument(resource instanceof PluginJsResource || resource instanceof PluginCssResource);
        this.resource = resource;
    }

    @Override
    public String getUrl() {
        // The additional regex replacement ensures that we return a correct relative URL.
        // This is necessary since getStaticUrl(UrlMode.RELATIVE) might not return a relative URL when we
        // are in the prebaker thread.
        return resource
                .getStaticUrl(UrlMode.RELATIVE)
                .replaceFirst(".*?(?=/s/.*?/_/)", "");
    }

    @Override
    public boolean isTainted() {
        return resource.isTainted();
    }

    @Override
    public List<PrebakeError> getPrebakeErrors() {
        return resource.getPrebakeErrors();
    }

    @Override
    public String getName() {
        return resource.toString();
    }

    @Override
    public String getExtension() {
        if (resource instanceof PluginJsResource) {
            return ".js";
        } else if (resource instanceof PluginCssResource) {
            return ".css";
        } else {
            throw new RuntimeException("Unknown plugin resource class: '" +
                    resource.getClass().getSimpleName() + "'");
        }
    }

}
