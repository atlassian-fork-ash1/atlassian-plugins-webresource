package com.atlassian.webresource.plugin.prebake.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Helper with concurrency.
 *
 * @since v3.5.0
 */
public final class ConcurrentUtil {

    private static final Logger log = LoggerFactory.getLogger(ConcurrentUtil.class);
    public static final long TIMEOUT = 3000;

    private ConcurrentUtil() {
        // statics only
    }

    public static void stopExecutor(ExecutorService pool) {
        stopExecutor(pool, TIMEOUT);
    }

    /**
     * Notes on advanced pool closer: see {@link ExecutorService}'s javadoc on shutdownAndAwaitTermination method impl
     *
     * @param pool    - executor to shutdown.
     * @param timeout - timeout to wait for.
     */
    public static void stopExecutor(ExecutorService pool, long timeout) {
        if (pool != null) {
            pool.shutdown(); // Disable new tasks from being submitted
            try {
                // Wait a while for existing tasks to terminate
                if (!pool.awaitTermination(timeout, TimeUnit.MILLISECONDS)) {
                    pool.shutdownNow(); // Cancel currently executing tasks
                    // Wait a while for tasks to respond to being cancelled
                    if (!pool.awaitTermination(timeout, TimeUnit.MILLISECONDS)) {
                        log.error("Pool did not terminate");
                    }
                }
            } catch (InterruptedException ie) {
                // (Re-)Cancel if current thread also interrupted
                pool.shutdownNow();
                // Preserve interrupt status
                ExceptionHandler.interruption(ie);
            }
        }
    }

}
