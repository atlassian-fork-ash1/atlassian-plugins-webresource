package com.atlassian.webresource.plugin.prebake.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Exception handling helper.
 *
 * @since v3.5.0
 */
public final class ExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(ExceptionHandler.class);

    private static final String DEFAULT_ERROR = "Something went wrong -> ";
    private static final String DEFAULT_INTERRUPTION_ERROR = "Was interrupted";

    private ExceptionHandler() {
        // statics only
    }

    /**
     * Log exception in to STD_ERR.
     *
     * @param message - message to write within exception.
     * @param t       - exception to log.
     */
    public static void handle(String message, Throwable t) {
        log.error(message, t);
    }

    /**
     * Handles InterruptedException in special way - preserves interrupt status.
     *
     * @param message - error message to log.
     * @param ie      - instance of interrupted exception to handle.
     * @see java.lang.InterruptedException
     */
    public static void interruption(String message, InterruptedException ie) {
        handle(message, ie);
        Thread.currentThread().interrupt();
    }

    /**
     * Handles InterruptedException in special way - preserves interrupt status.
     *
     * @param ie - instance of interrupted exception to handle.
     * @see java.lang.InterruptedException
     */
    public static void interruption(InterruptedException ie) {
        interruption(DEFAULT_INTERRUPTION_ERROR, ie);
    }

    /**
     * Handles incoming exception.
     * Checks if this exception is instanceof InterruptedException and applies special conditions,
     * otherwise default behavior.
     *
     * @param message - error message to log.
     * @param t       - exception to handle.
     * @see java.lang.InterruptedException
     */
    public static void ex(String message, Throwable t) {
        if (t instanceof InterruptedException) {
            interruption(message, (InterruptedException) t);
        } else {
            handle(message, t);
        }
    }

    /**
     * Handles incoming exception.
     * Checks if this exception is instanceof InterruptedException and applies special conditions,
     * otherwise default behavior.
     *
     * @param t - exception to handle.
     * @see java.lang.InterruptedException
     */
    public static void ex(Throwable t) {
        if (t instanceof InterruptedException) {
            interruption((InterruptedException) t);
        } else {
            handle(DEFAULT_ERROR, t);
        }
    }

}
