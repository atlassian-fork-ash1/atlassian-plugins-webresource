package com.atlassian.webresource.plugin.prebake.util;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import java.nio.file.Paths;

/**
 * Pre-bake helpers.
 *
 * @since v3.5.0
 */
public final class PreBakeUtil {

    public static final String UTF8 = "UTF-8";

    public static final String SEPRTR = Paths.get(".").getFileSystem().getSeparator();
    public static final String RELATIVE_ZIP = "bundle.zip";
    public static final String RELATIVE_STATE = "state.txt";
    public static final String RELATIVE_MAPPINGS = "mappings.json";
    public static final String RELATIVE_RESOURCES = "resources" + PreBakeUtil.SEPRTR;
    public static final String RELATIVE_REPORT = "report.txt";

    public static final String BUNDLE_ZIP_DIR = "bundle" + PreBakeUtil.SEPRTR;
    public static final String BUNDLE_ZIP_FILE = "bundle.zip";

    // part of the URI path to bundle ZIP file inside JSON response entity, concatenates with base URI.
    public static final String BUNDLE_ZIP_URI_PATH = "/prebake/" + BUNDLE_ZIP_FILE;

    private static final HashFunction HF = Hashing.sha1();

    private PreBakeUtil() {
        // statics only
    }

    public static String hash(byte[] bytes) {
        return HF.hashBytes(bytes).toString();
    }
}
