package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssembler;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.webresource.api.prebake.Coordinate;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.atlassian.webresource.plugin.prebake.resources.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import static java.util.Collections.singletonList;

/**
 * A {@link WebResourceCrawler} that collects the resource urls associated with the superbatch.
 *
 * @since v3.5.0
 */
public final class SuperBatchCrawler implements WebResourceCrawler {

    private final Logger log = LoggerFactory.getLogger(RootPageCrawler.class);
    private final PrebakeWebResourceAssemblerFactory assemblerFactory;
    private final Dimensions valueWhitelist;

    public SuperBatchCrawler(
            PrebakeWebResourceAssemblerFactory assemblerFactory,
            Dimensions valueWhitelist) {
        this.assemblerFactory = assemblerFactory;
        this.valueWhitelist = valueWhitelist;
    }

    @Override
    public void crawl(BlockingQueue<Resource> queue) throws InterruptedException {
        log.debug("Getting resource URLS from Super Batch Crawler");

        Dimensions dims = assemblerFactory
                .computeDimensions()
                .whitelistValues(valueWhitelist);
        log.debug("Current dimensions are:\n {}", dims.toString());
        log.debug("The size of the cartesian product is: {}", dims.cartesianProductSize());

        Iterator<Coordinate> coords = dims.cartesianProduct().iterator();
        while (coords.hasNext()) {
            PrebakeWebResourceAssembler assembler = assemblerFactory
                    .create()
                    .withCoordinate(coords.next())
                    .build();
            crawlAssembledResource(assembler.assembled(), queue);
        }
    }

    @Override
    public List<String> getTargets() {
        return singletonList("superbatch");
    }

}
