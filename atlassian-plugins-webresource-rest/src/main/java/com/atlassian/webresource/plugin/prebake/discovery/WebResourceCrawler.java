package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.plugin.webresource.impl.snapshot.RootPage;
import com.atlassian.webresource.api.assembler.AssembledResources;
import com.atlassian.webresource.api.assembler.WebResource;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.plugin.prebake.resources.PluginResource;
import com.atlassian.webresource.plugin.prebake.resources.Resource;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Generates resources URLs to prebake.
 *
 * @since v3.5.0
 */
@ExperimentalApi
public interface WebResourceCrawler {

    /**
     * Generates resource URLs to prebake.
     *
     * @param queue the queue where newly generated resource urls have to be added.
     */
    void crawl(BlockingQueue<Resource> queue) throws InterruptedException;

    /**
     * Gets the names of targets that this crawler will crawl, for example a list of {@link RootPage} keys.
     *
     * @return {@link List} of targets that will be crawled.
     */
    List<String> getTargets();

    /**
     * Crawls the contents of an {@link AssembledResources} object and adds its content to the {@link Collection}
     * passed as a parameter.
     *
     * @param source      {@link AssembledResources} object to crawl.
     * @param destination destination {@link Collection} for the resources discovered by the crawler.
     */
    default void crawlAssembledResource(
            AssembledResources source,
            BlockingQueue<Resource> destination) throws InterruptedException {
        WebResourceSet webResourceSet;
        do {
            webResourceSet = source.pollIncludedResources();
            for (WebResource resource : webResourceSet.getResources()) {
                if (!(resource instanceof PluginJsResource) && !(resource instanceof PluginCssResource)) {
                    continue;
                }
                PluginResource pr = new PluginResource((PluginUrlResource<?>) resource);
                destination.put(pr);
            }
        } while (!webResourceSet.isComplete());
    }

}
