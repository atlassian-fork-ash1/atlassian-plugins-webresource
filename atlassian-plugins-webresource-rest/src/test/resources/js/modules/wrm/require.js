module('wrm/require');

asyncTest("should be same as global", function() {
    require(["wrm/require"], function(wrmRequre){
        equal(wrmRequre, window.WRM.require);
        QUnit.start();
    });
});