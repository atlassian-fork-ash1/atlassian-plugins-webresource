module('wrm');

asyncTest("should be same as global", function() {
    require(["wrm"], function(wrm){
        equal(wrm, window.WRM);
        QUnit.start();
    });
});