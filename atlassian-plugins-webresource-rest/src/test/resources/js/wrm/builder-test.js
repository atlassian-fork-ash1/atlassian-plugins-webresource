WRM.curl(["wrm/_", "wrm/builder"], function(_, Builder) {
    module("Web Resource Manager Builder", {
        setup: function() {
            this.el = document.createElement("div");
            this.wrmKeyAttribute = "data-wrm-key";
            this.wrmBatchTypeAttribute = "data-wrm-batch-type";
        },
        addScript: function(scripts) {
            _.each(scripts.keys, _.bind(function(key) {
                var script = document.createElement("script");
                script.setAttribute(this.wrmKeyAttribute, key);
                script.setAttribute(this.wrmBatchTypeAttribute, scripts.type);
                this.el.appendChild(script);
            }, this));
        },
        addStylesheet: function(scripts) {
            _.each(scripts.keys, _.bind(function(key) {
                var link = document.createElement("link");
                link.rel = "stylesheet";
                link.setAttribute(this.wrmKeyAttribute, key);
                link.setAttribute(this.wrmBatchTypeAttribute, scripts.type);
                this.el.appendChild(link);
            }, this));
        },
        getContextBatchJs: function() {
            return {
                keys: ["jira.general"],
                type: "context"
            };
        },
        getContextBatchCss: function() {
            return {
                keys: ["jira.general"],
                type: "context"
            };
        },
        getContextBatchMultiJs: function() {
            return {
                keys: ["greenhopper-rapid-non-gadget,atl.general"],
                type: "context"
            };
        },
        getContextBatchMultiCss: function() {
            return {
                keys: ["greenhopper-rapid-non-gadget,atl.general"],
                type: "context"
            };
        },
        getResourceBatchJs: function() {
            return {
                keys: ["com.atlassian.jira.jira-header-plugin:jira-header"],
                type: "resource"
            };
        },
        getResourceBatchCss: function() {
            return {
                keys: ["com.atlassian.jira.jira-header-plugin:jira-header"],
                type: "resource"
            };
        },
        getNoBatchJs: function() {
            return {
                keys: ["com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources",
                       "com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources"],
                type: "resource"
            };
        },
        getNoBatchCss: function() {
            return {
                keys: ["com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources",
                       "com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources"],
                type: "resource"
            };
        }
    });

    test("JS loading", function() {
        this.addScript(this.getContextBatchJs());
        this.addScript(this.getContextBatchMultiJs());
        this.addScript(this.getResourceBatchJs());
        this.addScript(this.getNoBatchJs());

        var loaded = new Builder().initialize(this.el);

        equal(loaded.contexts.length, 3, "Expected 1 loaded contexts");
        equal(loaded.contexts[0], "jira.general", "Expected context batch");
        equal(loaded.contexts[1], "greenhopper-rapid-non-gadget", "Expected context batch");
        equal(loaded.contexts[2], "atl.general", "Expected context batch");
        equal(loaded.modules.length, 2, "Expected 2 loaded contexts");
        equal(loaded.modules[0], "com.atlassian.jira.jira-header-plugin:jira-header", "Expected web resource batch");
        equal(loaded.modules[1], "com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources", "Expected unbatched");
    });

    test("CSS loading", function() {
        this.addStylesheet(this.getContextBatchCss());
        this.addStylesheet(this.getContextBatchMultiCss());
        this.addStylesheet(this.getResourceBatchCss());
        this.addStylesheet(this.getNoBatchCss());

        var loaded = new Builder().initialize(this.el);

        equal(loaded.contexts.length, 3, "Expected 1 loaded contexts");
        equal(loaded.contexts[0], "jira.general", "Expected context batch");
        equal(loaded.contexts[1], "greenhopper-rapid-non-gadget", "Expected context batch");
        equal(loaded.contexts[2], "atl.general", "Expected context batch");
        equal(loaded.modules.length, 2, "Expected 2 loaded contexts");
        equal(loaded.modules[0], "com.atlassian.jira.jira-header-plugin:jira-header", "Expected web resource batch");
        equal(loaded.modules[1], "com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources", "Expected unbatched");
    });

    test("JS + CSS loading", function() {
        this.addStylesheet(this.getContextBatchCss());
        this.addStylesheet(this.getContextBatchMultiCss());
        this.addStylesheet(this.getResourceBatchCss());
        this.addStylesheet(this.getNoBatchCss());
        this.addScript(this.getContextBatchJs());
        this.addScript(this.getContextBatchMultiJs());
        this.addScript(this.getResourceBatchJs());
        this.addScript(this.getNoBatchJs());

        var loaded = new Builder().initialize(this.el);

        equal(loaded.contexts.length, 3, "Expected 1 loaded contexts");
        equal(loaded.contexts[0], "jira.general", "Expected context batch");
        equal(loaded.contexts[1], "greenhopper-rapid-non-gadget", "Expected context batch");
        equal(loaded.contexts[2], "atl.general", "Expected context batch");
        equal(loaded.modules.length, 2, "Expected 2 loaded contexts");
        equal(loaded.modules[0], "com.atlassian.jira.jira-header-plugin:jira-header", "Expected web resource batch");
        equal(loaded.modules[1], "com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources", "Expected unbatched");
    });

    test("JS + CSS loading with contextPath", function() {
        this.addStylesheet(this.getContextBatchCss());
        this.addStylesheet(this.getContextBatchMultiCss());
        this.addStylesheet(this.getResourceBatchCss());
        this.addStylesheet(this.getNoBatchCss());
        this.addScript(this.getContextBatchJs());
        this.addScript(this.getContextBatchMultiJs());
        this.addScript(this.getResourceBatchJs());
        this.addScript(this.getNoBatchJs());

        var loaded = new Builder().initialize(this.el);

        equal(loaded.contexts.length, 3, "Expected 1 loaded contexts");
        equal(loaded.contexts[0], "jira.general", "Expected context batch");
        equal(loaded.contexts[1], "greenhopper-rapid-non-gadget", "Expected context batch");
        equal(loaded.contexts[2], "atl.general", "Expected context batch");
        equal(loaded.modules.length, 2, "Expected 2 loaded contexts");
        equal(loaded.modules[0], "com.atlassian.jira.jira-header-plugin:jira-header", "Expected web resource batch");
        equal(loaded.modules[1], "com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources", "Expected unbatched");
    });
});
