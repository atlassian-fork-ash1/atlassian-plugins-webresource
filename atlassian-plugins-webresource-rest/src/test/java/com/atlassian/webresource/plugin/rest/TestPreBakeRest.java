package com.atlassian.webresource.plugin.rest;

import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.webresource.plugin.TestPreBake;
import com.atlassian.webresource.plugin.prebake.discovery.PreBakeState;
import com.atlassian.webresource.plugin.rest.PreBakeResource.PreBakeResult;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.atlassian.webresource.plugin.prebake.util.PreBakeUtil.BUNDLE_ZIP_URI_PATH;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Tests class responsible for exposing REST API.
 *
 * @since v3.5.0
 */
@SuppressWarnings("deprecation")
public class TestPreBakeRest extends TestPreBake {

    private PreBakeResource resource;

    @Mock
    private UriInfo info;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private PluginResourceLocator pluginResourceLocator;
    @Mock
    Config config;
    @Mock
    Globals globals;
    @Mock
    Snapshot snapshot;
    String baseUrl;

    @Before
    public void setUp() {
        baseUrl = "http://localhost:8090/jira/";
        when(info.getBaseUri()).thenReturn(URI.create(baseUrl));
        when(config.computeGlobalStateHash()).thenReturn("1234");
    }

    @Test
    public void testBundleURIPath() {
        String bundleUrl = "" + URI.create(info.getBaseUri() + BUNDLE_ZIP_URI_PATH).normalize();
        assertThat("URL path is wrong!", bundleUrl, containsString(baseUrl));
    }

    @Test
    public void testBundleURIDoubleSlash() {
        String bundleUrl = "" + URI.create(info.getBaseUri() + BUNDLE_ZIP_URI_PATH).normalize();
        List<String> urlParts = Lists.newArrayList(Splitter.on("//").split(bundleUrl));
        assertThat("There are more then two \"//\" in URL!", urlParts, hasSize(2));
    }

    @Test
    public void unitTestPreBakeSuperbatch() throws InterruptedException, IOException {
        when(config.isPreBakeEnabled()).thenReturn(true);

        PrebakeWebResourceAssemblerFactory assemblerFactory = wr.getWebResourceAssemblerFactory();
        resource = new PreBakeResource(config, globals, webResourceIntegration, assemblerFactory);
        resource.info = info;

        resource.put(new PreBakeResource.PreBakeRequest());
        PreBakeResult result;
        do {
            result = (PreBakeResult) resource.get().getEntity();
            TimeUnit.MILLISECONDS.sleep(15);
        } while (PreBakeState.RUNNING == result.state);

        Response bundle = resource.bundle();
        Object entity = bundle.getEntity();
        assertTrue("response is null!", entity != null && entity instanceof File && ((File) entity).exists());
    }

    @Test
    public void unitTestPreBakeRootPages() throws InterruptedException, IOException {
        when(config.isPreBakeEnabled()).thenReturn(true);
        when(globals.getSnapshot()).thenReturn(snapshot);
        when(snapshot.getAllRootPages()).thenReturn(emptyList());

        PrebakeWebResourceAssemblerFactory assemblerFactory = wr.getWebResourceAssemblerFactory();
        resource = new PreBakeResource(config, globals, webResourceIntegration, assemblerFactory);
        resource.info = info;

        resource.prebakeRootPages(new PreBakeResource.PreBakeRequest());
        PreBakeResult result;
        do {
            result = (PreBakeResult) resource.get().getEntity();
            TimeUnit.MILLISECONDS.sleep(15);
        } while (PreBakeState.RUNNING == result.state);

        Response bundle = resource.bundle();
        Object entity = bundle.getEntity();
        assertTrue("response is null!", entity != null && entity instanceof File && ((File) entity).exists());
    }

    @Test
    public void testPreBakeDisabled() {
        when(config.isPreBakeEnabled()).thenReturn(false);
        wr.configure().end();
        resource = new PreBakeResource(config, globals, webResourceIntegration, wr.getWebResourceAssemblerFactory());
        resource.info = info;
        assertThat(resource.get().getStatus(), is(Response.Status.FORBIDDEN.getStatusCode()));
    }

}
