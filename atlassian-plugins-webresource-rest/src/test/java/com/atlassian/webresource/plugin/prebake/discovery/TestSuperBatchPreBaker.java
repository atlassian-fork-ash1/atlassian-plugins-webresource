package com.atlassian.webresource.plugin.prebake.discovery;

import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import com.atlassian.plugin.webresource.prebake.DimensionAwareMockI18nTransformer;
import com.atlassian.plugin.webresource.prebake.TestPrebakeIdeas;
import com.atlassian.webresource.api.prebake.Dimensions;
import com.atlassian.webresource.plugin.prebake.resources.Resource;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @since v3.5.0
 */
public class TestSuperBatchPreBaker {

    @Test
    public void testSuperbatchCrawler() throws Exception {
        WebResourceImpl wr = new WebResourceImpl();

        wr.configure()
                .plugin("plug1")
                .transformer("I18N", DimensionAwareMockI18nTransformer.class)
                .webResource("wr1")
                .transformation("js", "I18N")
                .resource("bar.js", "insidebarjs REPLACE")
                .addToSuperbatch("plug1:wr1")

                .plugin("plug2")
                .webResource("wr2")
                .dependency("plug1:wr1")
                .condition(TestPrebakeIdeas.FeatureFlagCondition.class, ImmutableMap.of("flag", "awesome"))
                .condition(TestPrebakeIdeas.IsAdminCondition.class)
                .resource("foo.js", "insidefoojs")
                .addToSuperbatch("plug2:wr2")

                .end();

        SuperBatchCrawler sbc = new SuperBatchCrawler(
                wr.getWebResourceAssemblerFactory(),
                Dimensions.empty()
        );
        BlockingQueue<Resource> queue = new LinkedBlockingQueue<>();
        sbc.crawl(queue);

        Set<Resource> resources = new HashSet<>();
        queue.drainTo(resources);
        assertThat(resources.size(), equalTo(8));
        // Only JS resources for the moment. Making sure we're not slipping anything else in.
        for (Resource res : resources) {
            assertTrue(res.getPrebakeErrors().isEmpty());
            String url = res.getUrl();
            assertTrue(url.contains("_super/batch.js"));
        }
    }

}
