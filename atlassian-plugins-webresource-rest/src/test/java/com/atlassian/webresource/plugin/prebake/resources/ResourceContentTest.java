package com.atlassian.webresource.plugin.prebake.resources;

import org.junit.Test;

import java.net.URI;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ResourceContentTest {

    @Test
    public void testNormalResult() throws Exception {
        URI uri = new URI("http://localhost:8080/test");
        String contentType = "application/javascript";
        byte[] content = {12, 32, 45};

        ResourceContent res = new ResourceContent(uri, contentType, content);
        assertThat(res.getUri(), equalTo(uri));
        assertThat(res.getContentType(), equalTo(contentType));
        assertThat(res.getContent(), equalTo(content));
    }

}
