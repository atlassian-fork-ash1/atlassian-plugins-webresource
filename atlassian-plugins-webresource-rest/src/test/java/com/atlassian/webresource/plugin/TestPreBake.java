package com.atlassian.webresource.plugin;

import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.prebake.DimensionAwareMockI18nTransformer;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.prebake.TestPrebakeIdeas;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.io.IOException;

import static com.google.common.collect.ImmutableMap.of;
import static org.mockito.Mockito.when;

/**
 * @since v3.5.0
 */
public class TestPreBake extends TestCase {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    protected File tmpDir;

    @Mock
    protected WebResourceIntegration webResourceIntegration;
    protected PrebakeWebResourceAssemblerFactory assemblerFactory;

    @Before
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void setUpTestPreBake() throws IOException {
        tmpDir = temporaryFolder.newFolder("tmpBundle");

        when(webResourceIntegration.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn("http://localhost:8090/jira");
        when(webResourceIntegration.getBaseUrl(UrlMode.RELATIVE)).thenReturn("/jira");
        when(webResourceIntegration.getBaseUrl()).thenReturn("http://localhost:8090/jira");
        when(webResourceIntegration.getTemporaryDirectory()).thenReturn(tmpDir);

        wr.configure()
                .setStaticTransformers(TestUtils.emptyStaticTransformers())
                .plugin("plug1")
                .transformer("I18N", DimensionAwareMockI18nTransformer.class)
                .webResource("wr1")
                .transformation("js", "I18N")
                .resource("bar.js", "insidebarjs REPLACE")
                .addToSuperbatch("plug1:wr1")

                .plugin("plug2")
                .webResource("wr2")
                .dependency("plug1:wr1")
                .condition(TestPrebakeIdeas.FeatureFlagCondition.class, of("flag", "awesome"))
                .condition(TestPrebakeIdeas.IsAdminCondition.class)
                .resource("foo.js", "insidefoojs")
                .addToSuperbatch("plug2:wr2")

                .end();

        assemblerFactory = wr.getWebResourceAssemblerFactory();
    }
}
