package com.atlassian.webresource.plugin.prebake.resources;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class GenericUrlResourceTest {

    @Test
    public void testGenericUrlResourceCreation() {
        String url = "/test/file.svg";
        GenericUrlResource gu = new GenericUrlResource(url);

        assertThat(gu.getUrl(), equalTo(url));
        assertThat(gu.getName(), equalTo(url));
        assertThat(gu.getExtension(), equalTo(".svg"));
        assertFalse(gu.isTainted());
        assertThat(gu.getPrebakeErrors().size(), equalTo(0));
    }

    @Test
    public void testExtensionRetrieval() {
        String url = "/test/file.svg#asdf";
        GenericUrlResource gu = new GenericUrlResource(url);
        assertThat(gu.getExtension(), equalTo(".svg"));

        url = "/test/file.png?query1=test";
        gu = new GenericUrlResource(url);
        assertThat(gu.getExtension(), equalTo(".png"));

        url = "/test/file.jpg?query1=23&query2=test#hash";
        gu = new GenericUrlResource(url);
        assertThat(gu.getExtension(), equalTo(".jpg"));
    }

}
