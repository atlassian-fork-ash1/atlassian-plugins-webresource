package com.atlassian.webresource.plugin.rest;

import com.atlassian.plugin.webresource.impl.snapshot.RootPage;
import com.atlassian.plugin.webresource.impl.snapshot.WebResource;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPreBakeResource {

    private List<RootPage> rootPages;
    private List<String> rootPageNames;

    @Before
    public void setUp() {
        rootPageNames = asList("jira.root.pages:issue-view-page",
                "jira.root.pages:issues-nav-list",
                "jira.root.pages:agile-work",
                "jira.root.pages:issues-nav-detail",
                "jira.root.pages:dashboard-page");

        rootPages = new ArrayList<>();

        for (String name : rootPageNames) {
            WebResource mockWebResource = mock(WebResource.class);
            when(mockWebResource.getKey()).thenReturn(name);
            rootPages.add(new RootPage(mockWebResource));
        }
    }

    @Test
    public void testGetValidatedRootPages_returnsValidAndInvalidCorrectly() {
        List<String> validPagesNames = asList("jira.root.pages:issue-view-page",
                "jira.root.pages:issues-nav-list");
        List<String> invalidPagesNames = asList("jira.root.pages:sharks",
                "jira.root.pages:fish");

        List<RootPage> includedRootPages = rootPages.stream().filter(
                rootPage -> {
                    String name = rootPage.getWebResource().getKey();
                    return validPagesNames.contains(name);
                }).collect(Collectors.toList());

        List<String> allPageNames = new ArrayList<>();
        allPageNames.addAll(validPagesNames);
        allPageNames.addAll(invalidPagesNames);

        PreBakeResource.ValidatedRootPages validatedRootPages =
                PreBakeResource.getValidatedRootPages(allPageNames, rootPages);

        assertTrue(CollectionUtils.isEqualCollection(validPagesNames, validatedRootPages.validRootPageNames));
        assertTrue(CollectionUtils.isEqualCollection(invalidPagesNames, validatedRootPages.invalidRootPageNames));
        assertTrue(CollectionUtils.isEqualCollection(includedRootPages, validatedRootPages.includedRootPages));
    }

    @Test
    public void testGetValidatedRootPages_returnsAllWhenNull() {
        PreBakeResource.ValidatedRootPages validatedRootPages = PreBakeResource.getValidatedRootPages(null, rootPages);
        assertTrue(CollectionUtils.isEqualCollection(rootPages, validatedRootPages.includedRootPages));
        assertTrue(CollectionUtils.isEqualCollection(rootPageNames, validatedRootPages.validRootPageNames));
        assertTrue(CollectionUtils.isEmpty(validatedRootPages.invalidRootPageNames));
    }

    @Test
    public void testGetValidatedRootPages_returnsAllWhenEmpty() {
        PreBakeResource.ValidatedRootPages validatedRootPages =
                PreBakeResource.getValidatedRootPages(new ArrayList<>(), rootPages);
        assertTrue(CollectionUtils.isEqualCollection(rootPages, validatedRootPages.includedRootPages));
        assertTrue(CollectionUtils.isEmpty(validatedRootPages.invalidRootPageNames));
    }
}
