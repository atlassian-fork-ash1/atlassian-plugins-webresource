package it.com.atlassian.webresource;

import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class LegacyInternetExplorerResourcesOmittedTest {
    private static final Logger log = LoggerFactory.getLogger(LegacyInternetExplorerResourcesOmittedTest.class);

    private static final RefappTestedProduct refapp = TestedProductFactory.create(RefappTestedProduct.class);
    private static final String IT_TEST_SERVLET_PATH = "/plugins/servlet/it-legacy-ie-resources";
    private static final String PLUGIN_KEY = "com.atlassian.plugins.refapp-integration-tests";

    private AtlassianWebDriver driver;

    @Before
    public void setup() {
        driver = refapp.getTester().getDriver();
    }

    @Test
    public void shouldOmitIeOnlyResourcesWhenExplicitWebResourceRequestedStatically() {
        runTestAtPath("/staticViaExplicit");
    }

    @Test
    public void shouldOmitIeOnlyResourcesWhenContextRequestedStatically() {
        runTestAtPath("/staticViaContext");
    }

    @Test
    public void shouldOmitIeOnlyResourcesWhenExplicitWebResourceRequestedAsynchronously() {
        runTestAtPath("/asyncViaExplicit");
    }

    @Test
    public void shouldOmitIeOnlyResourcesWhenContextRequestedAsynchronously() {
        runTestAtPath("/asyncViaContext");
    }

    private void runTestAtPath(final String path) {
        // when
        driver.navigate().to(refapp.getProductInstance().getBaseUrl() + IT_TEST_SERVLET_PATH + path);
        driver.waitUntilElementIsLocated(By.id("resources-loaded"));

        // then
        final List<WebElement> scripts = driver.findElements(By.cssSelector("script[src]"));
        final List<WebElement> ourPluginScripts = scripts.stream()
                .filter(script -> script.getAttribute("src").contains(PLUGIN_KEY))
                .collect(Collectors.toCollection(ArrayList::new));

        final String headHtml = (String)driver.executeScript("return document.querySelector('head').innerHTML;");
        if (log.isDebugEnabled()) {
            log.debug("<head> source:");
            log.debug(headHtml);
        }

        assertThat(headHtml.toLowerCase(), allOf(
                not(containsString("<!--[if")),
                not(containsString("<![endif]"))
        ));
        assertThat(scripts.get(scripts.size()-1).getAttribute("src"), endsWith("not-ie-only.js"));
        assertThat(ourPluginScripts, hasSize(1));
    }
}
