package it.com.atlassian.webresource;

import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.elements.query.AbstractTimedQuery;
import com.atlassian.pageobjects.elements.query.ExpirationHandler;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.plugins.features.testing.EnableDarkFeaturesRule;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import org.intellij.lang.annotations.Language;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Collection;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class AsyncScriptsLoadedInOrderTest {
    private static final RefappTestedProduct refapp = TestedProductFactory.create(RefappTestedProduct.class);
    private static final String IT_TEST_SERVLET_PATH = "/plugins/servlet/it-test-resources";
    private static final String ENABLE_ASYNC_SCRIPTS = "ENABLE_ASYNC_SCRIPTS";

    private final List<String> expectedAsyncScriptsInOrder = asList(
            "webresource-it-test-1",
            "webresource-it-test-2",
            "webresource-it-test-3");
    @Rule
    public RuleChain enableDarkFeature = EnableDarkFeaturesRule.enableFeatureForAllUsersForProducts(ENABLE_ASYNC_SCRIPTS, refapp);

    @Test
    public void shouldMakeScriptsAsync() {
        // when
        navigate("/justAsyncLoad");

        // then
        @Language("CSS")
        final String asyncScriptSelector = "script[async][data-initially-rendered][src]";
        final List<WebElement> asyncScripts = findElements(By.cssSelector(asyncScriptSelector));

        assertThat(asyncScripts, hasSize(expectedAsyncScriptsInOrder.size()));
    }

    @Test
    public void shouldIncludeCssWithRegardToMediaQueryIntoCssBatch() {
        // when
        navigate("/cssMedia");

        // then
        final String printElementColor = findElement(By.cssSelector(".print_element")).getCssValue("color");
        final String displayElementColor = findElement(By.cssSelector(".display_element")).getCssValue("color");

        assertThat(printElementColor, equalTo("rgba(0, 0, 0, 1)"));
        assertThat(displayElementColor, equalTo("rgba(0, 0, 255, 1)"));
    }

    @Test
    public void shouldLoadScriptsInOrder() {
        // when
        navigate("/justAsyncLoad");

        // then
        final Object loadedScriptsLog = getLoadedScriptsLog(expectedAsyncScriptsInOrder.size());
        assertThat(loadedScriptsLog, equalTo(expectedAsyncScriptsInOrder));
    }

    @Test
    public void shouldLoadWRMRequiredScriptsAfterPageScripts() {
        // when
        navigate("/wrmRequire");

        // then
        final List<String> expectedOrderWithWrmRequire = asList(
                "require-resource-loaded",
                "webresource-it-test-2",
                "webresource-it-test-3",
                "webresource-it-test-1",
                "require-resource-resolved");

        final Object loadedScriptsLog = getLoadedScriptsLog(expectedOrderWithWrmRequire.size());
        assertThat(loadedScriptsLog, equalTo(expectedOrderWithWrmRequire));
    }

    @Test
    public void shouldLoadWRMRequiredScriptsWhenCalledAfterResourcesLoaded() {
        // when
        navigate("/wrmRequireDefered");

        // then
        final List<String> expectedOrderWithWrmRequire = asList(
                "wrm-require-defered",
                "webresource-it-test-1",
                "required-resource-defered-resolved");
        final Object loadedScriptsLog = getLoadedScriptsLog(expectedOrderWithWrmRequire.size());
        assertThat(loadedScriptsLog, equalTo(expectedOrderWithWrmRequire));
    }

    private Object getLoadedScriptsLog(final int numberOfExpectedScripts) {
        final AbstractTimedQuery<?> getLoadedScripts = new AbstractTimedQuery(new DefaultTimeouts().timeoutFor(TimeoutType.PAGE_LOAD), Timeouts.DEFAULT_INTERVAL, ExpirationHandler.RETURN_CURRENT) {
            @Override
            protected boolean shouldReturn(Object currentEval) {
                return currentEval instanceof Collection && ((Collection) currentEval).size() == numberOfExpectedScripts;
            }

            @Override
            protected Object currentValue() {
                @Language("JavaScript")
                final String getLoadOrder = "return scriptLoadOrder = window.scriptLoadOrder || null";
                return refapp.getTester().getDriver().executeScript(getLoadOrder);
            }
        };

        return getLoadedScripts.byDefaultTimeout();
    }

    private final void navigate(String servlet) {
        refapp.getTester().getDriver().navigate().to(refapp.getProductInstance().getBaseUrl() + IT_TEST_SERVLET_PATH + servlet);
    }

    private final List<WebElement> findElements(By selector) {
        return refapp.getTester().getDriver().findElements(selector);
    }

    private final WebElement findElement(By selector) {
        return refapp.getTester().getDriver().findElement(selector);
    }
}
