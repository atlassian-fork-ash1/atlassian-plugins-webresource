(function (aWindow) {
    var scriptName = "wrm-require-defered";

    aWindow.scriptLoadOrder = aWindow.scriptLoadOrder || [];
    aWindow.scriptLoadOrder.push(scriptName);

    console.log(scriptName + " loaded!");

    setTimeout(function () {
        WRM.require("wr!com.atlassian.plugins.refapp-integration-tests:atlassian-plugins-webresource-it-plugin-resources-1", function () {
            var scriptName = "required-resource-defered-resolved";
            aWindow.scriptLoadOrder = aWindow.scriptLoadOrder || [];
            aWindow.scriptLoadOrder.push(scriptName);

            console.log(scriptName + " loaded!");
        });
    }, 10)
})(window);