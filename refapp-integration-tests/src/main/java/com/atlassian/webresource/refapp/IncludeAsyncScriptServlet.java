package com.atlassian.webresource.refapp;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

@Scanned
public class IncludeAsyncScriptServlet extends AbstractWebResourceServlet {
    private static final String DEV_MODE = "atlassian.dev.mode";


    @Inject
    public IncludeAsyncScriptServlet(@ComponentImport SoyTemplateRenderer renderer, @ComponentImport PageBuilderService pageBuilderService) {
        super(renderer, pageBuilderService);

        handlers.put("justAsyncLoad", this::justAsyncLoad);
        handlers.put("wrmRequire", this::wrmRequire);
        handlers.put("wrmRequireDefered", this::wrmRequireDefered);
        handlers.put("cssMedia", this::cssMedia);
    }

    private void justAsyncLoad(HttpServletRequest request) {
        pageBuilderService.assembler().resources().requireWebResource(PLUGIN_ID + ":atlassian-plugins-webresource-it-plugin-resources-3");
    }

    private void wrmRequire(HttpServletRequest request) {
        pageBuilderService.assembler().resources().requireWebResource(PLUGIN_ID + ":test-resources-after-wrm-require");
    }

    private void wrmRequireDefered(HttpServletRequest request) {
        pageBuilderService.assembler().resources().requireWebResource(PLUGIN_ID + ":wrm-require-defered");
    }

    /*
     * we do want batching to be enabled here. so dev mode must be off
     */
    private void cssMedia(HttpServletRequest httpServletRequest) {
        String oldDevModeValue = switchDevModeOff();
        try {
            pageBuilderService.assembler().resources().requireContext("css_batching");
        } finally {
            restoreValue(oldDevModeValue);
        }
    }

    private static final void restoreValue(String value) {
        System.setProperty(DEV_MODE, value);
    }

    private static final String switchDevModeOff() {
        String oldDevModeValue = System.getProperty(DEV_MODE);
        System.setProperty(DEV_MODE, "false");
        return oldDevModeValue;
    }
}
