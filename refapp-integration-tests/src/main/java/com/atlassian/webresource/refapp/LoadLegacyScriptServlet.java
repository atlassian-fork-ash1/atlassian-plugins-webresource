package com.atlassian.webresource.refapp;

import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import javax.servlet.http.HttpServletRequest;

public class LoadLegacyScriptServlet extends AbstractWebResourceServlet {
    public LoadLegacyScriptServlet(SoyTemplateRenderer renderer, PageBuilderService pageBuilderService) {
        super(renderer, pageBuilderService);

        handlers.put("staticViaContext", this::staticViaContext);
        handlers.put("staticViaExplicit", this::staticViaExplicit);
        handlers.put("asyncViaContext", this::asyncViaContext);
        handlers.put("asyncViaExplicit", this::asyncViaExplicit);
    }

    private void staticViaContext(HttpServletRequest request) {
        pageBuilderService.assembler().resources().requireContext("test.ie-resources.static");
    }

    private void staticViaExplicit(HttpServletRequest request) {
        pageBuilderService.assembler().resources().requireWebResource(PLUGIN_ID + ":ie-resources-for-static");
    }

    private void asyncViaContext(HttpServletRequest request) {
        pageBuilderService.assembler().resources().requireWebResource("com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager");
        addToContext("loadString", "wrc!test.ie-resources.async");
    }

    private void asyncViaExplicit(HttpServletRequest request) {
        pageBuilderService.assembler().resources().requireWebResource("com.atlassian.plugins.atlassian-plugins-webresource-rest:web-resource-manager");
        addToContext("loadString", "wr!" + PLUGIN_ID + ":ie-resources-for-async");
    }
}
