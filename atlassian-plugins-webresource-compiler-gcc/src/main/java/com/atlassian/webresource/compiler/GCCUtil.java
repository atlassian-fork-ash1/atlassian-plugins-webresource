package com.atlassian.webresource.compiler;

import com.atlassian.webresource.spi.CompilerEntry;
import com.atlassian.webresource.spi.CompilerUtil;
import com.google.javascript.jscomp.CompilationLevel;
import com.google.javascript.jscomp.CompilerOptions;
import com.google.javascript.jscomp.SourceFile;
import com.google.javascript.jscomp.VariableRenamingPolicy;

import java.util.stream.Stream;

/**
 * @since v3.5.10
 */
public final class GCCUtil {

    private GCCUtil() {
        // statics only
    }

    /**
     * Reference method which helps to figure out what is the default options of GCC.
     */
    @SuppressWarnings("unused")
    public static CompilerOptions initDefaultOptions() {
        CompilerOptions options = new CompilerOptions();
        options.setLanguageIn(CompilerOptions.LanguageMode.ECMASCRIPT_2015);
        options.setStrictModeInput(false);
        options.setLanguageOut(CompilerOptions.LanguageMode.ECMASCRIPT5); // defaults of GCC - ECMASCRIPT3
        options.setEnvironment(CompilerOptions.Environment.BROWSER);
        options.setOutputCharset(CompilerUtil.CHARSET);
        options.setCheckSuspiciousCode(true);
        options.setFoldConstants(true);
        options.setDeadAssignmentElimination(true);
        options.setInlineFunctions(CompilerOptions.Reach.LOCAL_ONLY);
        options.setCoalesceVariableNames(true);
        options.setInlineLocalVariables(true);
        options.setFlowSensitiveInlineVariables(true);
        options.setRemoveDeadCode(true);
        options.setRemoveUnusedVariables(CompilerOptions.Reach.LOCAL_ONLY);
        options.setCollapseVariableDeclarations(true);
        options.setConvertToDottedProperties(true);
        options.setOptimizeArgumentsArray(true);
        options.setVariableRenaming(VariableRenamingPolicy.LOCAL);
        options.setLabelRenaming(true);
        options.setShadowVariables(true);
        options.setCollapseObjectLiterals(true);
        options.setClosurePass(true);
        options.setTrustedStrings(true);
        options.setProtectHiddenSideEffects(true);

        return options;
    }

    public static CompilerOptions initOptionsFromLevel(CompilationLevel compilationLevel) {
        CompilerOptions options = new CompilerOptions();
        compilationLevel.setOptionsForCompilationLevel(options);
        options.setLanguageIn(CompilerOptions.LanguageMode.ECMASCRIPT_2015);
        options.setStrictModeInput(false);
        options.setLanguageOut(CompilerOptions.LanguageMode.ECMASCRIPT5);
        options.setOutputCharset(CompilerUtil.CHARSET);
        options.setLegacyCodeCompile(true); // this option aggressively reduces optimization
        return options;
    }

    /**
     * @since 3.5.14
     */
    public static Stream<SourceFile> entryToSource(CompilerEntry entry) {
        if (entry == null) {
            return Stream.empty();
        }
        return Stream.of(SourceFile.fromCode(entry.key(), (String) entry.value()));
    }

}
