package com.atlassian.webresource.compiler;

import com.google.javascript.jscomp.CommandLineRunner;

import java.io.IOException;

/**
 * Wrapper for {@link CommandLineRunner} to mimic CLI GCC runs.
 *
 * @since v3.5.10
 */
public class WRMCommandLineRunner extends CommandLineRunner {
    public WRMCommandLineRunner(String[] args) {
        super(args);
    }

    public int performRun() throws IOException, FlagUsageException {
        return doRun();
    }
}
