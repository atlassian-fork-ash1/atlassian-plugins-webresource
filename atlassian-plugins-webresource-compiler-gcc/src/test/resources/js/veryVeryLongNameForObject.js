define([
    "./logger"
], function (Logger) {

    var logger = new Logger("DEBUG");

    function VeryVeryLongNameForObject(aPrettyLongNameForVariable) {
        var someNumber = 13 + 117;

        var whatIsIt = function() {
            logger.log(aPrettyLongNameForVariable + " is " + someNumber);
        }

        return whatIsIt;
    }

    return VeryVeryLongNameForObject;
});