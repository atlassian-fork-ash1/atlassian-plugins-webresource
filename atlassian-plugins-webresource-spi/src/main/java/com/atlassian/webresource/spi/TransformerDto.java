package com.atlassian.webresource.spi;

/**
 * Java DTO for `transformer` XML element. Needed to configure transformers from Java code.
 *
 * @since v3.5.13
 */
public class TransformerDto {
    /**
     * The key of transformer.
     */
    public final String key;

    /**
     * @param key the key of transformer.
     */
    public TransformerDto(final String key) {
        this.key = key;
    }
}
