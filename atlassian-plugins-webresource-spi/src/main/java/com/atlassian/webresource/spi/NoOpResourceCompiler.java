package com.atlassian.webresource.spi;

import javax.annotation.Nonnull;
import java.util.stream.Stream;

/**
 * @since v3.5.10
 */
public class NoOpResourceCompiler implements ResourceCompiler {
    @Override
    public void compile(@Nonnull Stream<CompilerEntry> entries) {
        // do nothing
    }

    @Override
    public String content(@Nonnull String key) {
        return null;
    }
}
