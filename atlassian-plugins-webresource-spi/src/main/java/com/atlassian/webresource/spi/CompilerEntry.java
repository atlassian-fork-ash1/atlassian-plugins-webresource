package com.atlassian.webresource.spi;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Input of {@link ResourceCompiler}.
 * Typical use case is to gather {@link java.util.stream.Stream} of this inputs and put them into
 * {@link ResourceCompiler#compile(Stream)}. After that you can receive compiled/minified content
 * providing {@link #key()} to {@link ResourceCompiler#content(String)} as an argument.
 *
 * @param <T> - type of the compiler/minifier input.
 * @since v3.5.16
 */
public class CompilerEntry<T> {
    private final String key;
    private final T value;

    private CompilerEntry(String key, T value) {
        if (key == null) {
            throw new IllegalArgumentException("key is null");
        }
        if (value == null) {
            throw new IllegalArgumentException("value is null");
        }
        this.key = key;
        this.value = value;
    }

    public static <T> CompilerEntry<T> ofKeyValue(@Nonnull String key, @Nonnull T value) {
        return new CompilerEntry<>(key, value);
    }

    public String key() {
        return key;
    }

    public T value() {
        return value;
    }

    @Override
    public String toString() {
        return "CompilerEntry{key='" + key + "'}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompilerEntry<?> that = (CompilerEntry<?>) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
